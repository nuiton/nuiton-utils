# Nuiton-Utils

Ensemble de projets utilitaires

Site : http://nuiton.page.nuiton.org/nuiton-utils/

## Librairie Util

  Librairie regroupant les utilitaires classiques sur les fichiers, les tableaux,
  les collections, les maps, les chaînes de caractères, ...

  * [ArrayUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/ArrayUtil.html)

  * [ClassLoaderUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/ClassLoaderUtil.html)

  * [CollectionUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/CollectionUtil.html)

  * [DateUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/DateUtil.html)

  * [ExceptionUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/ExceptionUtil.html)

  * [FileUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/FileUtil.html)

  * [GZUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/GZUtil.html)

  * [ObjectUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/ObjectUtil.html)

  * [ReflectUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/ReflectUtil.html)

  * [StringUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/StringUtil.html)

  * [ZipUtil](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/ZipUtil.html)

## Autres Collection

  * [BoundedList](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/BoundedList.html) :
    permet de définir des bornes min et max aux éléments contenu dans la liste.

  * [CategorisedListenerSet](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/CategorisedListenerSet.html) :
    permet de ranger des listeners en fonction d'une clé (catégorie). Les
    catégories sont hiérarchiques, les évènements seront donc transmis en
    cascade.

  * [ListenerSet](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/ListenerSet.html) :
    permet d'enregistrer des listeners sans doublon et de facilement lancer des
    évènements sur l'ensemble de ces listeners.

  * [HashList](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/HashList.html) :
    une liste indexé sans doublon.

  * [RecursiveProperties](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/RecursiveProperties.html) :
    permet d'injecter des valeurs de propriétés dans une autre.

  * [SortedProperties](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/SortedProperties.html) :
    Properties itérant lexicographiquement sur les clés.

  * [TransformedList](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/TransformedList.html) :
    permet d'avoir un état de stockage différent de l'état de l'objet retourné
    ou ajouté. (Ex : stockage du null sous forme de String)

## Profiling

  * [CallAnalyze](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/CallAnalyze.html) :
    trace les appels de méthodes par Thread (temps + mémoire utilisée).

  * [TimeLog](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/TimeLog.html) :
    affiche un message suivant un certain seuil de temps d'exécution. Elle
    s'appuie sur commons-logging pour afficher ces messages.

## Autres

  * [Resource](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/Resource.html) :
    permet de retrouver des fichiers dans le classpath de l'application ou en
    cas d'échec, sur le système de fichier.

  * Checksum : deux classes sont disponibles pour faire du hash MD5 sur des flux
    (MD5InputStream et MD5OutputStream), pour des chaînes vous pouvez utiliser
    directement les méthodes dans StringUtil (encodeMD5 et encodeSHA1).

  * [Version](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/version/Version.html) :
    représente un numéro de version d'une application, on peut y extraire ses
    constituantes ainsi que comparer différentes versions.

  * [PeriodDates](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/org/nuiton/util/PeriodDates.html) :
    représente une période entre deux dates. Il contient plusieurs méthodes
    utiles pour retrouver les mois constituant la période ou pour faire des
    comparaison.

