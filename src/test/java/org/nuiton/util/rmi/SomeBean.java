/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.rmi;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class SomeBean implements Serializable {

    protected SomeBean father;
    protected String message;
    protected int createdBy;
    protected transient Date createdOn;
    protected transient long number;

    public SomeBean(String message, Object creator, long number) {
        this.message = message;
        this.createdBy = System.identityHashCode(creator);
        this.createdOn = new Date();
        this.number = number;
    }

    public SomeBean getFather() {
        return father;
    }

    public void setFather(SomeBean father) {
        this.father = father;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

}
