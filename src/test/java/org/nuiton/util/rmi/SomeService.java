/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.rmi;

/**
 * Any service interface which will be used to test the RMI proxy.
 *
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public interface SomeService {

    /**
     * This method will return a String that can only be known by itself
     *
     * @return an instance identifier
     */
    int getInstanceId();

    /**
     * This method will return a complex object to validate that this is not
     * working only with primitive types
     *
     * @return a newly created SomeBean
     */
    SomeBean createComplexTypeObject();

    /**
     * Will throw an AnyException to test exception propagation
     *
     * @param bool true : will not fail, false will fail
     * @throws AnyException if the given boolean is false
     */
    void throwAnExceptionIfFalse(Boolean bool) throws AnyException;

}
