/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.rmi;

import java.io.InvalidObjectException;

/**
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class SomeServiceImpl implements SomeService {

    @Override
    public int getInstanceId() {
        return System.identityHashCode(this);
    }

    @Override
    public SomeBean createComplexTypeObject() {
        SomeBean father = new SomeBean("I'm your father !", this, 123);
        SomeBean result = new SomeBean("Son", this, 456);
        result.setFather(father);
        return result;
    }

    @Override
    public void throwAnExceptionIfFalse(Boolean bool) throws AnyException {
        if (!bool) {
            InvalidObjectException ioe = new InvalidObjectException("Wrong parameter !");
            throw new AnyException("Please provide 'true'", ioe);
        }
    }

}
