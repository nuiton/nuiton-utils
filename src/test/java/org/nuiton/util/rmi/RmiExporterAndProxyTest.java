/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.rmi;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.InvalidObjectException;
import java.rmi.NotBoundException;

/**
 * @author Arnaud Thimel - thimel@codelutin.com
 */
@Ignore
public class RmiExporterAndProxyTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RmiExporterAndProxyTest.class);

    @Test(expected = NullPointerException.class)
    public void testExportNullService() throws Exception {

        // It is mandatory to provide a service instance
        ServiceExporter.registerService(SomeService.class, null);
    }

    @Test(expected = NotBoundException.class)
    public void testProxyWithoutService() throws Exception {

        // Register and unregister (to make sure RMI registry is created)
        SomeServiceImpl impl = new SomeServiceImpl();
        ServiceExporter.registerService(SomeService.class, impl);
        ServiceExporter.unregisterService(SomeService.class);

        // This will throw an exception because service is not registered
        RemoteProxyFactory.createProxy(SomeService.class);
    }

    @Test
    public void testObjectIdentity() throws Exception {

        // Create and bind the service
        SomeServiceImpl impl = new SomeServiceImpl();
        int realServiceIdentity = System.identityHashCode(impl);
        ServiceExporter.registerService(SomeService.class, impl);

        // Get a proxy on this service and make sure this is not the same object
        SomeService clientSide = RemoteProxyFactory.createProxy(SomeService.class);
        int proxyIdentity = System.identityHashCode(clientSide);
        Assert.assertNotSame(proxyIdentity, realServiceIdentity);

        // Get the remote identifier and compare
        int serviceIdentityFromProxy = clientSide.getInstanceId();
        Assert.assertEquals(realServiceIdentity, serviceIdentityFromProxy);

        ServiceExporter.unregisterService(SomeService.class);
    }

    @Test
    public void testComplexType() throws Exception {

        // Create and bind the service
        SomeServiceImpl impl = new SomeServiceImpl();
        int realServiceIdentity = System.identityHashCode(impl);
        ServiceExporter.registerService(SomeService.class, impl);

        // Get a proxy on this service and do the call to createComplexTypeObject
        SomeService clientSide = RemoteProxyFactory.createProxy(SomeService.class);
        SomeBean bean = clientSide.createComplexTypeObject();

        // Now check that the bean is exactly as expected
        SomeBean father = bean.getFather();
        Assert.assertNotNull(father);
        Assert.assertEquals("I'm your father !", father.getMessage());
        Assert.assertEquals(realServiceIdentity, father.getCreatedBy());
        Assert.assertNull(father.getFather());
        Assert.assertNull(father.getCreatedOn()); // transient value
        Assert.assertEquals(0L, father.getNumber()); // transient value

        Assert.assertEquals("Son", bean.getMessage());
        Assert.assertEquals(realServiceIdentity, bean.getCreatedBy());
        Assert.assertNull(bean.getCreatedOn()); // transient value
        Assert.assertEquals(0L, bean.getNumber()); // transient value

        ServiceExporter.unregisterService(SomeService.class);
    }

    @Test
    public void testExceptionPropagation() throws Exception {

        // Create and bind the service
        SomeServiceImpl impl = new SomeServiceImpl();
        ServiceExporter.registerService(SomeService.class, impl);

        // Get a proxy on this service and do the call to createComplexTypeObject
        SomeService clientSide = RemoteProxyFactory.createProxy(SomeService.class);

        // Does not fail
        clientSide.throwAnExceptionIfFalse(true);

        try {
            clientSide.throwAnExceptionIfFalse(false);
            Assert.fail("An axeption should have been thrown");
        } catch (AnyException ae) {

            Assert.assertEquals("Please provide 'true'", ae.getMessage());

            // Exception is the good one. Now check its cause
            InvalidObjectException cause = (InvalidObjectException)ae.getCause();
            Assert.assertEquals("Wrong parameter !", cause.getMessage());
        }

        ServiceExporter.unregisterService(SomeService.class);
    }

}
