package org.nuiton.util.beans;
/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * To test http://nuiton.org/issues/2178.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.1
 */
public class BeanC {

    public static final String PROPERTY_A = "a";

    public static final String PROPERTY_AA = "aa";

    public static final String PROPERTY_B = "b";

    protected BeanA a;

    protected BeanAA aa;

    protected BeanB b;

    protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);


    public BeanA getA() {
        return a;
    }

    public void setA(BeanA a) {
        Object oldValue = this.a;
        this.a = a;
        firePropertyChange(PROPERTY_A, oldValue, a);
    }

    public BeanAA getAa() {
        return aa;
    }

    public void setAa(BeanAA aa) {
        Object oldValue = this.aa;
        this.aa = aa;
        firePropertyChange(PROPERTY_AA, oldValue, aa);
    }

    public BeanB getB() {
        return b;
    }

    public void setB(BeanB b) {
        Object oldValue = this.b;
        this.b = b;
        firePropertyChange(PROPERTY_B, oldValue, b);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue,
                                      Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected PropertyChangeListener[] getPropertyChangeListeners() {
        return pcs.getPropertyChangeListeners();
    }
}
