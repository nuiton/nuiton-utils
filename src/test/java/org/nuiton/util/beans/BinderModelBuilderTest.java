/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util.beans;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Tests the {@link BinderModelBuilder}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5.3
 */
public class BinderModelBuilderTest {

    protected static final String PROPERTY_CLASS = "class";

    BinderModelBuilder<BeanA, BeanA> builderAA;

    BinderModelBuilder<BeanA, BeanB> builderAB;

    BinderModelBuilder<BeanB, BeanB> builderBB;

    BinderModelBuilder<BeanB, BeanA> builderBA;

    @Test
    public void newEmptyBuilder() throws Exception {

        // Limit cases

        try {
            BinderModelBuilder.newEmptyBuilder(null);
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }
        try {
            BinderModelBuilder.newEmptyBuilder(null, null);
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }
        try {
            BinderModelBuilder.newEmptyBuilder(BeanA.class, null);
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }
        try {
            BinderModelBuilder.newEmptyBuilder(null, BeanA.class);
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        // A → A

        builderAA = BinderModelBuilder.newEmptyBuilder(BeanA.class);
        assertBuilder(builderAA, BeanA.class, BeanA.class);

        assertDescriptor(builderAA.sourceDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanA.PROPERTY_A,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        assertDescriptor(builderAA.targetDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanA.PROPERTY_A,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        builderAA = BinderModelBuilder.newEmptyBuilder(BeanA.class, BeanA.class);
        assertBuilder(builderAA, BeanA.class, BeanA.class);

        assertDescriptor(builderAA.sourceDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanA.PROPERTY_A,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        assertDescriptor(builderAA.targetDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanA.PROPERTY_A,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        // A → B

        builderAB = BinderModelBuilder.newEmptyBuilder(BeanA.class, BeanB.class);
        assertBuilder(builderAB, BeanA.class, BeanB.class);

        assertDescriptor(builderAB.sourceDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanA.PROPERTY_A,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        assertDescriptor(builderAB.targetDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F,
                         BeanB.PROPERTY_BB,
                         BeanB.PROPERTY_A2,
                         BeanB.PROPERTY_B2,
                         BeanB.PROPERTY_C2,
                         BeanB.PROPERTY_D2,
                         BeanB.PROPERTY_E2,
                         BeanB.PROPERTY_F2
        );

        // B → B

        builderBB = BinderModelBuilder.newEmptyBuilder(BeanB.class);
        assertBuilder(builderBB, BeanB.class, BeanB.class);

        assertDescriptor(builderBB.sourceDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F,
                         BeanB.PROPERTY_BB,
                         BeanB.PROPERTY_A2,
                         BeanB.PROPERTY_B2,
                         BeanB.PROPERTY_C2,
                         BeanB.PROPERTY_D2,
                         BeanB.PROPERTY_E2,
                         BeanB.PROPERTY_F2
        );
        assertDescriptor(builderBB.targetDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F,
                         BeanB.PROPERTY_BB,
                         BeanB.PROPERTY_A2,
                         BeanB.PROPERTY_B2,
                         BeanB.PROPERTY_C2,
                         BeanB.PROPERTY_D2,
                         BeanB.PROPERTY_E2,
                         BeanB.PROPERTY_F2
        );

        builderBB = BinderModelBuilder.newEmptyBuilder(BeanB.class, BeanB.class);
        assertBuilder(builderBB, BeanB.class, BeanB.class);

        assertDescriptor(builderBB.sourceDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F,
                         BeanB.PROPERTY_BB,
                         BeanB.PROPERTY_A2,
                         BeanB.PROPERTY_B2,
                         BeanB.PROPERTY_C2,
                         BeanB.PROPERTY_D2,
                         BeanB.PROPERTY_E2,
                         BeanB.PROPERTY_F2
        );
        assertDescriptor(builderBB.targetDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F,
                         BeanB.PROPERTY_BB,
                         BeanB.PROPERTY_A2,
                         BeanB.PROPERTY_B2,
                         BeanB.PROPERTY_C2,
                         BeanB.PROPERTY_D2,
                         BeanB.PROPERTY_E2,
                         BeanB.PROPERTY_F2
        );

        // B → A

        builderBA = BinderModelBuilder.newEmptyBuilder(BeanB.class, BeanA.class);
        assertBuilder(builderBA, BeanB.class, BeanA.class);

        assertDescriptor(builderBA.sourceDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F,
                         BeanB.PROPERTY_BB,
                         BeanB.PROPERTY_A2,
                         BeanB.PROPERTY_B2,
                         BeanB.PROPERTY_C2,
                         BeanB.PROPERTY_D2,
                         BeanB.PROPERTY_E2,
                         BeanB.PROPERTY_F2
        );
        assertDescriptor(builderBA.targetDescriptors,
                         PROPERTY_CLASS,
                         BeanA.PROPERTY_AA,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F
        );
    }


    @Test
    public void newDefaultBuilder() throws Exception {

        // Limit cases

        try {
            BinderModelBuilder.newDefaultBuilder(null);
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }
        try {
            BinderModelBuilder.newDefaultBuilder(null, null);
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }
        try {
            BinderModelBuilder.newDefaultBuilder(BeanA.class, null);
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }
        try {
            BinderModelBuilder.newDefaultBuilder(null, BeanA.class);
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        // A → A

        builderAA = BinderModelBuilder.newDefaultBuilder(BeanA.class);
        assertBuilder(builderAA, BeanA.class, BeanA.class);
        assertDescriptor(builderAA.getModel().propertiesMapping,
                         BeanA.PROPERTY_A,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        builderAA = BinderModelBuilder.newDefaultBuilder(BeanA.class, BeanA.class);
        assertBuilder(builderAA, BeanA.class, BeanA.class);
        assertDescriptor(builderAA.getModel().propertiesMapping,
                         BeanA.PROPERTY_A,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        // A → B

        builderAB = BinderModelBuilder.newDefaultBuilder(BeanA.class, BeanB.class);
        assertBuilder(builderAB, BeanA.class, BeanB.class);
        assertDescriptor(builderAB.getModel().propertiesMapping,
                         BeanA.PROPERTY_A,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        // B → B

        builderBB = BinderModelBuilder.newDefaultBuilder(BeanB.class);
        assertBuilder(builderBB, BeanB.class, BeanB.class);

        assertDescriptor(builderBB.getModel().propertiesMapping,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F,
                         BeanB.PROPERTY_A2,
                         BeanB.PROPERTY_B2,
                         BeanB.PROPERTY_C2,
                         BeanB.PROPERTY_D2,
                         BeanB.PROPERTY_E2,
                         BeanB.PROPERTY_F2
        );

        builderBB = BinderModelBuilder.newDefaultBuilder(BeanB.class, BeanB.class);
        assertBuilder(builderBB, BeanB.class, BeanB.class);
        assertDescriptor(builderBB.getModel().propertiesMapping,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F,
                         BeanB.PROPERTY_A2,
                         BeanB.PROPERTY_B2,
                         BeanB.PROPERTY_C2,
                         BeanB.PROPERTY_D2,
                         BeanB.PROPERTY_E2,
                         BeanB.PROPERTY_F2
        );

        // B → A

        builderBA = BinderModelBuilder.newDefaultBuilder(BeanB.class, BeanA.class);
        assertBuilder(builderBA, BeanB.class, BeanA.class);
        assertDescriptor(builderBA.getModel().propertiesMapping,
                         BeanB.PROPERTY_A,
                         BeanB.PROPERTY_B,
                         BeanB.PROPERTY_C,
                         BeanB.PROPERTY_D,
                         BeanB.PROPERTY_E,
                         BeanB.PROPERTY_F
        );

    }

    @Test
    public void testAddSimpleProperties() throws Exception {

        builderAA = BinderModelBuilder.newEmptyBuilder(BeanA.class);

        Binder.BinderModel<BeanA, BeanA> model = builderAA.getModel();

        // limit case
        try {
            builderAA.addSimpleProperties((String) null);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        // not existing source property
        try {
            builderAA.addSimpleProperties(BeanB.PROPERTY_A2);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }

        builderAA.addSimpleProperties(BeanA.PROPERTY_A);
        Assert.assertEquals(1, model.getSourceDescriptors().length);
        Assert.assertEquals(1, model.getTargetDescriptors().length);
        Map<String, String> map = model.getPropertiesMapping();
        assertDescriptor(map, BeanB.PROPERTY_A);
        Assert.assertEquals(BeanA.PROPERTY_A, map.get(BeanA.PROPERTY_A));

        builderAA.addSimpleProperties(BeanA.PROPERTY_A);
        Assert.assertEquals(1, model.getSourceDescriptors().length);
        Assert.assertEquals(1, model.getTargetDescriptors().length);
        assertDescriptor(map, BeanB.PROPERTY_A);
        Assert.assertEquals(BeanA.PROPERTY_A, map.get(BeanA.PROPERTY_A));

        builderAA.addSimpleProperties(BeanA.PROPERTY_B);
        Assert.assertEquals(2, model.getSourceDescriptors().length);
        Assert.assertEquals(2, model.getTargetDescriptors().length);
        assertDescriptor(map, BeanB.PROPERTY_A, BeanB.PROPERTY_B);
        Assert.assertEquals(BeanA.PROPERTY_B, map.get(BeanA.PROPERTY_B));

    }

    @Test
    public void testAddProperty() throws Exception {
        builderAB =
                BinderModelBuilder.newEmptyBuilder(BeanA.class, BeanB.class);

        Binder.BinderModel<BeanA, BeanB> model = builderAB.getModel();

        // limit cases

        try {
            builderAB.addProperty(null, null);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }
        try {
            builderAB.addProperty(BeanA.PROPERTY_A, null);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }
        try {
            builderAB.addProperty(null, BeanA.PROPERTY_A);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        // not existing source property
        try {
            builderAB.addProperty(BeanB.PROPERTY_A2, BeanB.PROPERTY_A);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }

        builderAB.addProperty(BeanA.PROPERTY_A, BeanA.PROPERTY_A);
        Assert.assertEquals(1, model.getSourceDescriptors().length);
        Assert.assertEquals(1, model.getTargetDescriptors().length);
        Map<String, String> map = model.getPropertiesMapping();
        assertDescriptor(map, BeanB.PROPERTY_A);
        Assert.assertEquals(BeanA.PROPERTY_A, map.get(BeanA.PROPERTY_A));

        builderAB.addProperty(BeanB.PROPERTY_A, BeanB.PROPERTY_A2);
        Assert.assertEquals(1, model.getSourceDescriptors().length);
        Assert.assertEquals(1, model.getTargetDescriptors().length);
        assertDescriptor(map, BeanB.PROPERTY_A);
        Assert.assertEquals(BeanB.PROPERTY_A2, map.get(BeanA.PROPERTY_A));

        builderAB.addProperty(BeanA.PROPERTY_B, BeanB.PROPERTY_B2);
        Assert.assertEquals(2, model.getSourceDescriptors().length);
        Assert.assertEquals(2, model.getTargetDescriptors().length);
        assertDescriptor(map, BeanB.PROPERTY_A, BeanB.PROPERTY_B);
        Assert.assertEquals(BeanB.PROPERTY_B2, map.get(BeanB.PROPERTY_B));
    }

    @Test
    public void testAddProperties() throws Exception {
        builderAB =
                BinderModelBuilder.newEmptyBuilder(BeanA.class, BeanB.class);

        try {
            builderAB.addProperties(BeanB.PROPERTY_A, BeanB.PROPERTY_A2,
                                    BeanB.PROPERTY_B);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }

        try {
            builderAB.addProperties(BeanB.PROPERTY_A, null,
                                    BeanB.PROPERTY_B, BeanB.PROPERTY_B);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        builderAB.addProperties(BeanB.PROPERTY_A, BeanB.PROPERTY_A,
                                BeanB.PROPERTY_B, BeanB.PROPERTY_B2);

        Binder.BinderModel<?, ?> model = builderAB.getModel();

        Assert.assertEquals(2, model.getSourceDescriptors().length);
        Assert.assertEquals(2, model.getTargetDescriptors().length);
        Map<String, String> map = model.getPropertiesMapping();
        assertDescriptor(map, BeanB.PROPERTY_A, BeanB.PROPERTY_B);
        Assert.assertEquals(BeanA.PROPERTY_A, map.get(BeanA.PROPERTY_A));
        Assert.assertEquals(BeanB.PROPERTY_B2, map.get(BeanA.PROPERTY_B));
    }

    protected void assertDescriptor(Map<String, ?> descriptors,
                                    String... propertyNames) {
        Assert.assertNotNull(descriptors);
        Assert.assertEquals(propertyNames.length, descriptors.size());
        for (String propertyName : propertyNames) {
            Assert.assertTrue(descriptors.containsKey(propertyName));
        }
    }

    protected <S, T> void assertBuilder(BinderModelBuilder<S, T> builder,
                                        Class<S> sourceType,
                                        Class<T> targetType) {
        Assert.assertNotNull(builder);
        Assert.assertNotNull(builder.getModel());
        Assert.assertEquals(sourceType, builder.getModel().getSourceType());
        Assert.assertEquals(targetType, builder.getModel().getTargetType());
    }

    @Test
    public void testBug1713() throws Exception {

        // This is test for http://nuiton.org/issues/1713

        BinderModelBuilder<Source, Destination> builder =
                BinderModelBuilder.newDefaultBuilder(Source.class, Destination.class);
        builder.addProperty("salutationCode", "gender");

        Binder<Source, Destination> binder = builder.toBinder();

        String salutationCode = "youhou";

        Source source = new Source(salutationCode);
        Destination destination = new Destination();
        binder.copy(source, destination);

        Assert.assertEquals(salutationCode, destination.getGender());
    }

    @Test
    public void testEvol1913() throws Exception {

        // This is test for http://nuiton.org/issues/1913

        // A → AA

        BinderModelBuilder<BeanA, BeanAA> builderAAA;

        builderAAA = BinderModelBuilder.newDefaultBuilder(BeanA.class, BeanAA.class);
        assertBuilder(builderAAA, BeanA.class, BeanAA.class);
        assertDescriptor(builderAAA.getModel().propertiesMapping,
                         BeanA.PROPERTY_B,
                         BeanA.PROPERTY_C,
                         BeanA.PROPERTY_D,
                         BeanA.PROPERTY_E,
                         BeanA.PROPERTY_F
        );

        try {
            // property a is not the same, will fail
            builderAAA = BinderModelBuilder.newDefaultBuilder(BeanA.class, BeanAA.class, false);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertTrue(true);
        }
    }

}
