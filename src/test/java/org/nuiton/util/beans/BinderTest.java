/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util.beans;

import com.google.common.collect.ImmutableMap;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class BinderTest {

    BeanA a;

    BeanB b;

    BeanC c;

    Binder<BeanA, BeanA> binderA;

    Binder<BeanA, BeanB> binderB;

    Binder<BeanC, BeanC> binderC;

    private static final String VALUE_A = "a";

    private static final String VALUE_B = "b";

    private static final String VALUE_C = "c";

    private static final int VALUE_E = 10;

    @BeforeClass
    public static void beforeClass() {

        // remove all previous models from the factory
        BinderFactory.clear();

        // creates a mirrored binder model from A → A with only one property

        BinderModelBuilder<BeanA, BeanA> builder =
                BinderModelBuilder.newEmptyBuilder(BeanA.class).
                        addSimpleProperties(BeanA.PROPERTY_A);

        // register the model into factory
        BinderFactory.registerBinderModel(builder);

        // creates a binder model from A → B with more properties

        BinderModelBuilder<BeanA, BeanB> builder1 =
                BinderModelBuilder.newEmptyBuilder(BeanA.class, BeanB.class).
                        addSimpleProperties(BeanA.PROPERTY_A, BeanA.PROPERTY_B).
                        addProperty(BeanA.PROPERTY_C, BeanB.PROPERTY_C2).
                        addProperty(BeanA.PROPERTY_E, BeanB.PROPERTY_E2);

        // register the model into factory
        BinderFactory.registerBinderModel(builder1);

        // creates a binder model from A → B with more properties

        BinderModelBuilder<BeanC, BeanC> builder2 =
                BinderModelBuilder.newEmptyBuilder(BeanC.class, BeanC.class).
                        addSimpleProperties(BeanC.PROPERTY_A).
                        addBinder(BeanC.PROPERTY_A, builder.toBinder());

        // register the model into factory
        BinderFactory.registerBinderModel(builder2);

    }

    @AfterClass
    public static void tearDown() {

        // remove all models from the factory
        BinderFactory.clear();

    }

    @Before
    public void setUp() {

        // get the binder A → A
        binderA = BinderFactory.newBinder(BeanA.class);

        // get the binder A → B
        binderB = BinderFactory.newBinder(BeanA.class, BeanB.class);

        // get the binder C → C
        binderC = BinderFactory.newBinder(BeanC.class);

        a = new BeanA();
        b = new BeanB();
        c = new BeanC();
    }

    @Test
    public void testObtainProperties() {
        Map<String, Object> map;
        map = binderA.obtainProperties(a);
        Assert.assertEquals(0, map.size());

        map = binderA.obtainProperties(a, true);
        Assert.assertEquals(1, map.size());

        map = binderB.obtainProperties(a);
        Assert.assertEquals(0, map.size());

        a.setA(VALUE_A);
        map = binderA.obtainProperties(a);
        Assert.assertEquals(1, map.size());
        Assert.assertTrue(map.containsKey(BeanA.PROPERTY_A));
        Assert.assertTrue(map.containsValue(VALUE_A));
        Assert.assertEquals(VALUE_A, map.get(BeanA.PROPERTY_A));

        map = binderB.obtainProperties(a);
        Assert.assertEquals(1, map.size());
        Assert.assertTrue(map.containsKey(BeanA.PROPERTY_A));
        Assert.assertTrue(map.containsValue(VALUE_A));
        Assert.assertEquals(VALUE_A, map.get(BeanA.PROPERTY_A));

        a.setB(VALUE_B);
        map = binderA.obtainProperties(a);
        Assert.assertEquals(1, map.size());
        Assert.assertTrue(map.containsKey(BeanA.PROPERTY_A));
        Assert.assertFalse(map.containsKey(BeanA.PROPERTY_B));
        Assert.assertTrue(map.containsValue(VALUE_A));
        Assert.assertFalse(map.containsValue(VALUE_B));
        Assert.assertEquals(VALUE_A, map.get(BeanA.PROPERTY_A));

        map = binderB.obtainProperties(a);
        Assert.assertEquals(2, map.size());
        Assert.assertTrue(map.containsKey(BeanA.PROPERTY_A));
        Assert.assertTrue(map.containsKey(BeanA.PROPERTY_B));
        Assert.assertTrue(map.containsValue(VALUE_A));
        Assert.assertTrue(map.containsValue(VALUE_B));
        Assert.assertEquals(VALUE_A, map.get(BeanA.PROPERTY_A));
        Assert.assertEquals(VALUE_B, map.get(BeanA.PROPERTY_B));
    }

    @Test
    public void testObtainSourceProperty() {

        {
            String value = binderA.obtainSourceProperty(a, BeanA.PROPERTY_A);
            Assert.assertNull(value);
        }
        a.setA(VALUE_A);
        {
            String value = binderA.obtainSourceProperty(a, BeanA.PROPERTY_A);
            Assert.assertEquals(VALUE_A, value);
        }

        // Binder A has no property e declared
        try {
            binderA.obtainSourceProperty(a, BeanA.PROPERTY_E);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertTrue(true);
        }

        {
            int value = binderB.obtainSourceProperty(a, BeanA.PROPERTY_E);
            Assert.assertEquals(0, value);
        }
        a.setE(VALUE_E);
        {
            int value = binderB.obtainSourceProperty(a, BeanA.PROPERTY_E);
            Assert.assertEquals(VALUE_E, value);
        }

    }

    @Test
    public void testObtainTargetProperty() {

        {
            String value = binderA.obtainTargetProperty(a, BeanA.PROPERTY_A);
            Assert.assertNull(value);
        }
        a.setA(VALUE_A);
        {
            String value = binderA.obtainTargetProperty(a, BeanA.PROPERTY_A);
            Assert.assertEquals(VALUE_A, value);
        }

        // Binder A has no property e declared
        try {
            binderA.obtainTargetProperty(a, BeanA.PROPERTY_E);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertTrue(true);
        }

        {
            int value = binderB.obtainTargetProperty(b, BeanB.PROPERTY_E2);
            Assert.assertEquals(0, value);
        }
        b.setE2(VALUE_E);
        {
            int value = binderB.obtainTargetProperty(b, BeanB.PROPERTY_E2);
            Assert.assertEquals(VALUE_E, value);
        }

    }

    @Test
    public void testInjectProperties() {

        ImmutableMap.Builder<String, Object> builder = new ImmutableMap.Builder<String, Object>();
        builder.put(BeanA.PROPERTY_A, "a");

        Assert.assertNull(a.getA());

        {
            ImmutableMap<String, Object> properties = builder.build();
            binderA.injectProperties(properties, a);

            Assert.assertEquals(a.getA(), properties.get(BeanA.PROPERTY_A));

        }

        builder.put(BeanA.PROPERTY_B, "b");

        {

            ImmutableMap<String, Object> properties = builder.build();
            try {
                binderA.injectProperties(properties, a);
                Assert.fail();
            } catch (IllegalStateException e) {
                // Ok normal case binder does not have properties b
            }

        }

        {

            Assert.assertNull(b.getA());
            Assert.assertNull(b.getB());

            ImmutableMap<String, Object> properties = builder.build();
            binderB.injectProperties(properties, b);

            Assert.assertEquals(b.getA(), properties.get(BeanB.PROPERTY_A));
            Assert.assertEquals(b.getB(), properties.get(BeanB.PROPERTY_B));

        }

    }

    @Test
    public void testCopy() {

        a.setA(VALUE_A);
        a.setB(VALUE_B);
        a.setC(VALUE_C);
        a.setE(VALUE_E);

        binderA.copy(a, b);
        Assert.assertEquals(VALUE_A, b.getA());
        Assert.assertNull(b.getB());
        Assert.assertNull(b.getB2());
        Assert.assertNull(b.getC());
        Assert.assertEquals(0, b.getE2());

        binderB.copy(a, b);
        Assert.assertEquals(VALUE_A, b.getA());
        Assert.assertEquals(VALUE_B, b.getB());
        Assert.assertNull(b.getC());
        Assert.assertEquals(VALUE_C, b.getC2());
        Assert.assertEquals(VALUE_E, b.getE2());

        binderB.copy(null, b);
        Assert.assertNull(b.getA());
        Assert.assertNull(b.getB());
        Assert.assertNull(b.getC2());
        Assert.assertEquals(0, b.getE2());

        c.setA(a);
        BeanC cTarget = new BeanC();
        binderC.copy(c, cTarget);
        Assert.assertNotEquals(c.getA(), cTarget.getA());
        Assert.assertEquals(c.getA().getA(), cTarget.getA().getA());
        Assert.assertNull(cTarget.getA().getB());
        Assert.assertNull(cTarget.getA().getC());
        Assert.assertNull(cTarget.getA().getD());

        c.setA(null);
        cTarget = new BeanC();
        binderC.copy(c, cTarget);
        Assert.assertNull(cTarget.getA());

    }

    @Test
    public void testCopyIncluding() {

        a.setA(VALUE_A);
        a.setB(VALUE_B);
        a.setC(VALUE_C);
        a.setE(VALUE_E);

        // copy only the property A
        binderA.copy(a, b, BeanA.PROPERTY_A);
        Assert.assertEquals(VALUE_A, b.getA());
        Assert.assertNull(b.getB());
        Assert.assertNull(b.getB2());
        Assert.assertNull(b.getC());
        Assert.assertEquals(0, b.getE2());
    }

    @Test
    public void testCopyExcluding() {

        a.setA(VALUE_A);
        a.setB(VALUE_B);
        a.setC(VALUE_C);
        a.setE(VALUE_E);

        // #1 : Copy all properties defined by binderB instead of PROPERTY_A
        binderB.copyExcluding(a, b, BeanA.PROPERTY_A);
        Assert.assertNull(b.getA());
        // same result as copy
        Assert.assertEquals(VALUE_B, b.getB());
        Assert.assertNull(b.getC());
        Assert.assertEquals(VALUE_C, b.getC2());
        Assert.assertEquals(VALUE_E, b.getE2());

        // #2 : Copy all properties : same as binderB.copy(a, b);
        binderB.copyExcluding(a, b);
        // same result as copy
        Assert.assertEquals(VALUE_A, b.getA());
        Assert.assertEquals(VALUE_B, b.getB());
        Assert.assertNull(b.getC());
        Assert.assertEquals(VALUE_C, b.getC2());
        Assert.assertEquals(VALUE_E, b.getE2());

        // #3 : Copy all properties, excluding one is not considered from source
        binderB.copyExcluding(a, b, BeanB.PROPERTY_C2);
        // same result as copy
        Assert.assertEquals(VALUE_A, b.getA());
        Assert.assertEquals(VALUE_B, b.getB());
        Assert.assertNull(b.getC());
        Assert.assertEquals(VALUE_C, b.getC2());
        Assert.assertEquals(VALUE_E, b.getE2());

    }

    @Test
    public void testDiff() {
        a.setA(VALUE_A);
        b.setA(VALUE_A);
        a.setB(VALUE_A);
        b.setB(VALUE_B);

        List<PropertyDiff> diff = binderB.diff(a, b);

        // only one property has changed
        Assert.assertEquals(1, diff.size());

        // names of the properties used for comparison
        PropertyDiff propertyDiff = diff.get(0);
        Assert.assertEquals(BeanA.PROPERTY_B, propertyDiff.getSourceProperty());
        Assert.assertEquals(BeanB.PROPERTY_B, propertyDiff.getTargetValue());

        // old and new values
        Assert.assertEquals(VALUE_A, propertyDiff.getSourceValue());
        Assert.assertEquals(VALUE_B, propertyDiff.getTargetValue());
    }
}
