/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util.beans;

public class BeanB extends BeanA {

    public static final String PROPERTY_BB = "bb";

    public static final String PROPERTY_A2 = "a2";

    public static final String PROPERTY_B2 = "b2";

    public static final String PROPERTY_C2 = "c2";

    public static final String PROPERTY_D2 = "d2";

    public static final String PROPERTY_E2 = "e2";

    public static final String PROPERTY_F2 = "f2";

    String bb;

    String a2, b2, c2, d2;

    int e2, f2;

    public String getBb() {
        return bb;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        Object oldValue = this.a2;
        this.a2 = a2;
        firePropertyChange(PROPERTY_A2, oldValue, a2);
    }

    public String getB2() {
        return b2;
    }

    public void setB2(String b2) {
        Object oldValue = this.b2;
        this.b2 = b2;
        firePropertyChange(PROPERTY_B2, oldValue, b2);
    }

    public String getC2() {
        return c2;
    }

    public void setC2(String c2) {
        Object oldValue = this.c2;
        this.c2 = c2;
        firePropertyChange(PROPERTY_C2, oldValue, c2);
    }

    public String getD2() {
        return d2;
    }

    public void setD2(String d2) {
        Object oldValue = this.d2;
        this.d2 = d2;
        firePropertyChange(PROPERTY_D2, oldValue, d2);
    }

    public int getE2() {
        return e2;
    }

    public void setE2(int e2) {
        Object oldValue = this.e2;
        this.e2 = e2;
        firePropertyChange(PROPERTY_E2, oldValue, e2);
    }

    public int getF2() {
        return f2;
    }

    public void setF2(int f2) {
        Object oldValue = this.f2;
        this.f2 = f2;
        firePropertyChange(PROPERTY_F2, oldValue, f2);
    }
}
