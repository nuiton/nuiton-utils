/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util.beans;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class BeanAA {

    public static final String PROPERTY_AA = "aa";
    
    public static final String PROPERTY_A = "a";

    public static final String PROPERTY_B = "b";

    public static final String PROPERTY_C = "c";

    public static final String PROPERTY_D = "d";

    public static final String PROPERTY_E = "e";

    public static final String PROPERTY_F = "f";

    protected String aa, b, c, d;

    protected int a, e, f;

    protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public void setAa(String aa) {
        this.aa = aa;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        Object oldValue = this.a;
        this.a = a;
        firePropertyChange(PROPERTY_A, oldValue, a);
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        Object oldValue = this.b;
        this.b = b;
        firePropertyChange(PROPERTY_B, oldValue, b);
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        Object oldValue = this.c;
        this.c = c;
        firePropertyChange(PROPERTY_C, oldValue, c);
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        Object oldValue = this.d;
        this.d = d;
        firePropertyChange(PROPERTY_D, oldValue, d);
    }

    public int getE() {
        return e;
    }

    public void setE(int e) {
        Object oldValue = this.e;
        this.e = e;
        firePropertyChange(PROPERTY_E, oldValue, e);
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        Object oldValue = this.f;
        this.f = f;
        firePropertyChange(PROPERTY_F, oldValue, f);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue,
                                      Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected PropertyChangeListener[] getPropertyChangeListeners() {
        return pcs.getPropertyChangeListeners();
    }
}
