/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.beans;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the {@link BinderFactory}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.5.3
 */

public class BinderFactoryTest {

    @Before
    public void setUp() {
        BinderFactory.clear();
    }

    @Test
    public void testNewBinder() throws Exception {

        // Limit cases
        try {
            BinderFactory.newBinder(null);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        try {
            BinderFactory.newBinder(null, (Class<?>) null);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        try {
            BinderFactory.newBinder(BeanA.class, (Class<?>) null);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        try {
            BinderFactory.newBinder(null, BeanA.class);
            Assert.fail();
        } catch (NullPointerException e) {
            Assert.assertTrue(true);
        }

        Assert.assertTrue(BinderFactory.binderModels.isEmpty());

        Binder<BeanA, BeanA> binderAA = BinderFactory.newBinder(BeanA.class);

        Assert.assertNotNull(binderAA);
        Assert.assertNotNull(BinderFactory.binderModels);
        Assert.assertEquals(1, BinderFactory.binderModels.size());

    }


    @Test
    public void testRegisterBinderModel() throws Exception {

        Assert.assertNull(BinderFactory.binderModels);

        Binder.BinderModel<BeanA, BeanA> model =
                BinderFactory.registerBinderModel(BinderModelBuilder.newDefaultBuilder(BeanA.class));

        Assert.assertNotNull(BinderFactory.binderModels);
        Assert.assertEquals(1, BinderFactory.binderModels.size());
        Assert.assertEquals(model,
                            BinderFactory.newBinder(BeanA.class).getModel());

        Binder.BinderModel<BeanA, BeanA> model1 =
                BinderFactory.registerBinderModel(BinderModelBuilder.newDefaultBuilder(BeanA.class));

        Assert.assertNotNull(BinderFactory.binderModels);
        Assert.assertEquals(1, BinderFactory.binderModels.size());
        Assert.assertNotSame(model, model1);
        Assert.assertEquals(model1,
                            BinderFactory.newBinder(BeanA.class).getModel());

        BinderFactory.registerBinderModel(model1, "context");

        Assert.assertNotNull(BinderFactory.binderModels);
        Assert.assertEquals(2, BinderFactory.binderModels.size());
        Assert.assertEquals(
                model1,
                BinderFactory.newBinder(BeanA.class, "context").getModel());


        BinderFactory.registerBinderModel(model, "context");


        Assert.assertNotNull(BinderFactory.binderModels);
        Assert.assertEquals(2, BinderFactory.binderModels.size());
        Assert.assertEquals(
                model,
                BinderFactory.newBinder(BeanA.class, "context").getModel());

    }


    @Test
    public void testClear() throws Exception {
        Assert.assertNull(BinderFactory.binderModels);
        BinderFactory.newBinder(BeanA.class);
        Assert.assertNotNull(BinderFactory.binderModels);
        Assert.assertEquals(1, BinderFactory.binderModels.size());

        BinderFactory.clear();

        Assert.assertNull(BinderFactory.binderModels);

    }
}
