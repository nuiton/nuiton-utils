/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.beans;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.beans.beancontext.BeanContextSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

/**
 * To test the {@link BeanUtil} class.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class BeanUtilTest {

    @Test
    public void isJavaBeanCompiliant() {

        boolean javaBeanCompiliant;

        javaBeanCompiliant = BeanUtil.isJavaBeanCompiliant(BeanA.class);
        Assert.assertTrue(javaBeanCompiliant);

        javaBeanCompiliant = BeanUtil.isJavaBeanCompiliant(BeanB.class);
        Assert.assertTrue(javaBeanCompiliant);

        javaBeanCompiliant = BeanUtil.isJavaBeanCompiliant(getClass());
        Assert.assertFalse(javaBeanCompiliant);
    }

    @Test
    public void getReadableProperties() {

        assertFoundReadableProperties(BeanA.class,
                                      BeanA.PROPERTY_A,
                                      BeanA.PROPERTY_B,
                                      BeanA.PROPERTY_C,
                                      BeanA.PROPERTY_D,
                                      BeanA.PROPERTY_E,
                                      BeanA.PROPERTY_F);

        assertFoundReadableProperties(BeanB.class,
                                      BeanA.PROPERTY_A,
                                      BeanA.PROPERTY_B,
                                      BeanA.PROPERTY_C,
                                      BeanA.PROPERTY_D,
                                      BeanA.PROPERTY_E,
                                      BeanA.PROPERTY_F,
                                      BeanB.PROPERTY_BB,
                                      BeanB.PROPERTY_A2,
                                      BeanB.PROPERTY_B2,
                                      BeanB.PROPERTY_C2,
                                      BeanB.PROPERTY_D2,
                                      BeanB.PROPERTY_E2,
                                      BeanB.PROPERTY_F2
        );

        assertFoundReadableProperties(getClass());
    }

    @Test
    public void getDescriptors() {

        assertFoundDescriptors(BeanA.class,
                               BeanUtil.IS_READ_DESCRIPTOR,
                               BeanA.PROPERTY_A,
                               BeanA.PROPERTY_B,
                               BeanA.PROPERTY_C,
                               BeanA.PROPERTY_D,
                               BeanA.PROPERTY_E,
                               BeanA.PROPERTY_F);

        assertFoundDescriptors(BeanB.class,
                               BeanUtil.IS_READ_DESCRIPTOR,
                               BeanA.PROPERTY_A,
                               BeanA.PROPERTY_B,
                               BeanA.PROPERTY_C,
                               BeanA.PROPERTY_D,
                               BeanA.PROPERTY_E,
                               BeanA.PROPERTY_F,
                               BeanB.PROPERTY_BB,
                               BeanB.PROPERTY_A2,
                               BeanB.PROPERTY_B2,
                               BeanB.PROPERTY_C2,
                               BeanB.PROPERTY_D2,
                               BeanB.PROPERTY_E2,
                               BeanB.PROPERTY_F2
        );

        assertFoundDescriptors(getClass(), BeanUtil.IS_READ_DESCRIPTOR);

        assertFoundDescriptors(BeanA.class,
                               BeanUtil.IS_WRITE_DESCRIPTOR,
                               BeanA.PROPERTY_AA,
                               BeanA.PROPERTY_A,
                               BeanA.PROPERTY_B,
                               BeanA.PROPERTY_C,
                               BeanA.PROPERTY_D,
                               BeanA.PROPERTY_E,
                               BeanA.PROPERTY_F);

        assertFoundDescriptors(BeanB.class,
                               BeanUtil.IS_WRITE_DESCRIPTOR,
                               BeanA.PROPERTY_AA,
                               BeanA.PROPERTY_A,
                               BeanA.PROPERTY_B,
                               BeanA.PROPERTY_C,
                               BeanA.PROPERTY_D,
                               BeanA.PROPERTY_E,
                               BeanA.PROPERTY_F,
                               BeanB.PROPERTY_A2,
                               BeanB.PROPERTY_B2,
                               BeanB.PROPERTY_C2,
                               BeanB.PROPERTY_D2,
                               BeanB.PROPERTY_E2,
                               BeanB.PROPERTY_F2
        );

        assertFoundDescriptors(getClass(), BeanUtil.IS_WRITE_DESCRIPTOR);
    }

    @Test
    public void getWriteableProperties() {

        assertFoundWriteableProperties(BeanA.class,
                                       BeanA.PROPERTY_A,
                                       BeanA.PROPERTY_B,
                                       BeanA.PROPERTY_C,
                                       BeanA.PROPERTY_D,
                                       BeanA.PROPERTY_E,
                                       BeanA.PROPERTY_F);

        assertFoundWriteableProperties(BeanB.class,
                                       BeanA.PROPERTY_A,
                                       BeanA.PROPERTY_B,
                                       BeanA.PROPERTY_C,
                                       BeanA.PROPERTY_D,
                                       BeanA.PROPERTY_E,
                                       BeanA.PROPERTY_F,
                                       BeanB.PROPERTY_BB,
                                       BeanB.PROPERTY_A2,
                                       BeanB.PROPERTY_B2,
                                       BeanB.PROPERTY_C2,
                                       BeanB.PROPERTY_D2,
                                       BeanB.PROPERTY_E2,
                                       BeanB.PROPERTY_F2
        );

        assertFoundWriteableProperties(getClass());
    }

    @Test
    public void getNestedReadeableProperty() {

        assertFoundNestedReadableProperties(BeanA.class,
                                            BeanA.PROPERTY_A,
                                            BeanA.PROPERTY_B,
                                            BeanA.PROPERTY_C,
                                            BeanA.PROPERTY_D,
                                            BeanA.PROPERTY_E,
                                            BeanA.PROPERTY_F);

        assertFoundNestedReadableProperties(BeanB.class,
                                            BeanA.PROPERTY_A,
                                            BeanA.PROPERTY_B,
                                            BeanA.PROPERTY_C,
                                            BeanA.PROPERTY_D,
                                            BeanA.PROPERTY_E,
                                            BeanA.PROPERTY_F,
                                            BeanB.PROPERTY_BB,
                                            BeanB.PROPERTY_A2,
                                            BeanB.PROPERTY_B2,
                                            BeanB.PROPERTY_C2,
                                            BeanB.PROPERTY_D2,
                                            BeanB.PROPERTY_E2,
                                            BeanB.PROPERTY_F2
        );

        assertFoundNestedReadableProperties(BeanC.class,
                                            BeanC.PROPERTY_A,
                                            BeanC.PROPERTY_AA,
                                            BeanC.PROPERTY_B,
                                            BeanC.PROPERTY_A + "." + BeanA.PROPERTY_A,
                                            BeanC.PROPERTY_A + "." + BeanA.PROPERTY_A,
                                            BeanC.PROPERTY_A + "." + BeanA.PROPERTY_B,
                                            BeanC.PROPERTY_A + "." + BeanA.PROPERTY_C,
                                            BeanC.PROPERTY_A + "." + BeanA.PROPERTY_D,
                                            BeanC.PROPERTY_A + "." + BeanA.PROPERTY_E,
                                            BeanC.PROPERTY_A + "." + BeanA.PROPERTY_F,
                                            BeanC.PROPERTY_B + "." + BeanB.PROPERTY_BB,
                                            BeanC.PROPERTY_B + "." + BeanB.PROPERTY_A2,
                                            BeanC.PROPERTY_B + "." + BeanB.PROPERTY_B2,
                                            BeanC.PROPERTY_B + "." + BeanB.PROPERTY_C2,
                                            BeanC.PROPERTY_B + "." + BeanB.PROPERTY_D2,
                                            BeanC.PROPERTY_B + "." + BeanB.PROPERTY_E2,
                                            BeanC.PROPERTY_B + "." + BeanB.PROPERTY_F2

        );
    }

    @Test
    public void addPropertyChangeListener() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {

        PropertyChangeListener l = new BeanContextSupport();

        BeanA beanA = new BeanA();

        BeanUtil.addPropertyChangeListener(l, beanA);

        PropertyChangeListener[] listeners;
        listeners = beanA.getPropertyChangeListeners();

        Assert.assertEquals(1, listeners.length);
        Assert.assertEquals(l, listeners[0]);


        BeanUtil.addPropertyChangeListener(l, beanA);

        listeners = beanA.getPropertyChangeListeners();

        Assert.assertEquals(2, listeners.length);
        Assert.assertEquals(l, listeners[0]);
        Assert.assertEquals(l, listeners[1]);
    }

    @Test
    public void removePropertyChangeListener() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {

        PropertyChangeListener[] listeners;

        PropertyChangeListener l = new BeanContextSupport();

        BeanA beanA = new BeanA();
        beanA.addPropertyChangeListener(l);

        listeners = beanA.getPropertyChangeListeners();
        Assert.assertEquals(1, listeners.length);
        Assert.assertEquals(l, listeners[0]);

        BeanUtil.removePropertyChangeListener(l, beanA);

        listeners = beanA.getPropertyChangeListeners();

        Assert.assertEquals(0, listeners.length);
    }

    protected void assertFoundReadableProperties(Class<?> type, String... expectedproperties) {
        Set<String> readableProperties = BeanUtil.getReadableProperties(type);
        Assert.assertEquals(expectedproperties.length, readableProperties.size());
        for (String expectedproperty : expectedproperties) {
            Assert.assertTrue("Did not found property " + expectedproperty, readableProperties.contains(expectedproperty));
        }
    }

    protected void assertFoundWriteableProperties(Class<?> type, String... expectedproperties) {
        Set<String> readableProperties = BeanUtil.getReadableProperties(type);
        Assert.assertEquals(expectedproperties.length, readableProperties.size());
        for (String expectedproperty : expectedproperties) {
            Assert.assertTrue(readableProperties.contains(expectedproperty));
        }
    }

    protected void assertFoundNestedReadableProperties(Class<?> type, String... expectedProperties) {
        for (String expectedProperty : expectedProperties) {
            boolean actual = BeanUtil.isNestedReadableProperty(type, expectedProperty);
            Assert.assertTrue("Did not found nested property " + expectedProperty, actual);
        }
    }

    protected void assertFoundDescriptors(Class<?> type,
                                          Predicate<PropertyDescriptor> predicate,
                                          String... expectedProperties) {
        Set<PropertyDescriptor> actual = BeanUtil.getDescriptors(type, predicate);
        Assert.assertEquals(expectedProperties.length, actual.size());

        ImmutableMap<String, PropertyDescriptor> map = Maps.uniqueIndex(actual, new Function<PropertyDescriptor, String>() {
            @Override
            public String apply(PropertyDescriptor input) {
                return input.getName();
            }
        });
        for (String expectedproperty : expectedProperties) {
            Assert.assertTrue("Did not found property " + expectedproperty,
                              map.containsKey(expectedproperty));
        }
    }
}
