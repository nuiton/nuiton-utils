/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.beans;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Tests {@link BeanMonitor}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @serial
 * @since 1.4.1
 */
public class BeanMonitorTest {

    @Test
    public void testMonitor() throws Exception {

        BeanMonitor monitor = new BeanMonitor(BeanA.PROPERTY_A,
                                              BeanA.PROPERTY_B,
                                              BeanA.PROPERTY_E
        );

        BeanA bean = new BeanA();
        monitor.setBean(bean);

        // nothing is monitored
        assertMonitor(monitor);

        // property A was modified
        bean.setA("A");
        assertMonitor(monitor, BeanA.PROPERTY_A, null);

        // clean monitor
        monitor.clearModified();
        assertMonitor(monitor);

        // property A was not modified
        bean.setA("A");
        assertMonitor(monitor);

        // property C is not monitored
        bean.setC("C");
        assertMonitor(monitor);

        // property A was modified
        bean.setA("AA");
        assertMonitor(monitor, BeanA.PROPERTY_A, "A");

        // property A and B were modified
        bean.setB("B");
        assertMonitor(monitor,
                      BeanA.PROPERTY_A, "A",
                      BeanA.PROPERTY_B, null);

        // property B is no more modified (came back to original value)
        bean.setB(null);
        assertMonitor(monitor, BeanA.PROPERTY_A, "A");

        // property A and E were modified
        bean.setE(1);
        assertMonitor(monitor, BeanA.PROPERTY_A, "A", BeanA.PROPERTY_E, 0);

        // property A is no more modified (came back to original value)
        bean.setA("A");
        assertMonitor(monitor, BeanA.PROPERTY_E, 0);

        // property E is no more modified (came back to original value)
        bean.setE(0);
        assertMonitor(monitor);

        // property B was modified
        bean.setB("B");
        assertMonitor(monitor, BeanA.PROPERTY_B, null);

        // nothing is monitored (bean changed)
        monitor.setBean(bean);
        assertMonitor(monitor);

    }

    @Test
    public void testMonitorAno2048() throws Exception {

        BeanMonitor monitor = new BeanMonitor(BeanA.PROPERTY_A,
                                              BeanA.PROPERTY_B,
                                              BeanA.PROPERTY_E
        );

        BeanA bean = new BeanA();
        bean.setB(null);

        monitor.setBean(bean);

        // ano #2048 : no modification with two null properties
        bean.setB(null);
        assertMonitor(monitor);
    }

    protected void assertMonitor(BeanMonitor monitor,
                                 Object... propertyNamesAndOriginalValues) {

        if (propertyNamesAndOriginalValues.length % 2 != 0) {
            throw new IllegalArgumentException(
                    "propertiesAndValues should be couple of (propertyName, originalValue)");
        }
        String[] modifiedFields = monitor.getModifiedProperties();
        Map<String, Object> originalValues = monitor.getOriginalValues();

        if (propertyNamesAndOriginalValues.length == 0) {
            Assert.assertFalse(monitor.wasModified());
            Assert.assertEquals(0, modifiedFields.length);
        } else {
            Assert.assertTrue(monitor.wasModified());
            for (int i = 0; i < propertyNamesAndOriginalValues.length; i += 2) {
                String propertyName = (String) propertyNamesAndOriginalValues[i];
                Object value = propertyNamesAndOriginalValues[i + 1];
                Assert.assertEquals(propertyName, modifiedFields[i / 2]);
                Assert.assertEquals(value, originalValues.get(propertyName));
            }
        }
    }
}
