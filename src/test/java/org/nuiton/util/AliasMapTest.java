package org.nuiton.util;

/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class AliasMapTest {

    protected Object[] toArray(Collection c) {
        Object[] result = c.toArray();
        Arrays.sort(result);
        return result;
    }

    @Test
    public void testAlias() {
        AliasMap<String, String, String> map = new AliasMap<>();
        map.put("a", "A", "à", "â", "ä", "commun");
        map.put("b", "B");
        map.put("e", "E", "é", "è", "ê", "ë", "commun");
        map.put("ebis", "EBIS", "é", "è", "commun");

        Assert.assertEquals("A", map.get("a"));
        Assert.assertArrayEquals(new String[]{"commun", "à", "â", "ä"}, toArray(map.getAlias("a")));
        Assert.assertArrayEquals(new String[]{"a"}, toArray(map.getKeyAlias("à")));
        Assert.assertArrayEquals(new String[]{"e", "ebis"}, toArray(map.getKeyAlias("é")));
        Assert.assertArrayEquals(new String[]{"E", "EBIS"}, toArray(map.getValueAlias("é", "è")));
        Assert.assertArrayEquals(new String[]{}, toArray(map.getValueAlias("é", "è", "à")));

        String v = map.remove("e");
        Assert.assertEquals("E", v);
        Assert.assertArrayEquals(new String[]{}, toArray(map.getAlias("e")));
        Assert.assertArrayEquals(new String[]{"ebis"}, toArray(map.getKeyAlias("é")));
        Assert.assertArrayEquals(new String[]{"ebis"}, toArray(map.getKeyAlias("é")));

        Collection<String> cr = map.removeValue("à", "é");
        Assert.assertArrayEquals(new String[]{}, toArray(cr));

        cr = map.removeValue("commun");
        Assert.assertArrayEquals(new String[]{"A", "EBIS"}, toArray(cr));
    }

}
