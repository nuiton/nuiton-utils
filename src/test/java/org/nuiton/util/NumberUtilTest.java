package org.nuiton.util;

/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Kevin Morin - morin@codelutin.com
 * @since 3.0
 */
public class NumberUtilTest {

    private static final Log log = LogFactory.getLog(NumberUtilTest.class);

    @Test
    public void testDivideAndEnsureSum() {
        log.debug("test divideAndEnsureSum");
        Assert.assertNull(NumberUtil.divideAndEnsureSum(100, 0));
        Assert.assertArrayEquals(new int[]{ 34, 33, 33 }, NumberUtil.divideAndEnsureSum(100, 3));
        Assert.assertArrayEquals(new int[]{ 17, 17, 17, 17, 16, 16 }, NumberUtil.divideAndEnsureSum(100, 6));
    }

    @Test
    public void testRound3Digits() {

        assertRoundThreeDigits(1.2f, 1.2f);
        assertRoundThreeDigits(1.22f, 1.22f);
        assertRoundThreeDigits(1.222f, 1.222f);
        assertRoundThreeDigits(1.2222f, 1.222f);
        assertRoundThreeDigits(1.2225f, 1.222f);
        assertRoundThreeDigits(1.2226f, 1.223f);
        assertRoundThreeDigits(11.2226f, 11.223f);
        assertRoundThreeDigits(111.2226f, 111.223f);
        assertRoundThreeDigits(1111.2226f, 1111.223f);

        assertRoundThreeDigits(1111.999f, 1111.999f);
        assertRoundThreeDigits(1111.9994f, 1111.999f);
        assertRoundThreeDigits(1111.9995f, 1112f);
        assertRoundThreeDigits(1111.9996f, 1112f);
    }

    @Test
    public void testRoundOneDigit() {

        assertRoundOneDigit(1.2f, 1.2f);
        assertRoundOneDigit(1.22f, 1.2f);
        assertRoundOneDigit(1.5f, 1.5f);
        assertRoundOneDigit(1.55f, 1.5f);
        assertRoundOneDigit(1.56f, 1.6f);
        assertRoundOneDigit(1.9f, 1.9f);
        assertRoundOneDigit(1.222f, 1.2f);
        assertRoundOneDigit(11.2226f, 11.2f);
        assertRoundOneDigit(111.2226f, 111.2f);
        assertRoundOneDigit(1111.2226f, 1111.2f);

        assertRoundOneDigit(1111.999f, 1112.0f);
        assertRoundOneDigit(1111.9994f, 1112.0f);
        assertRoundOneDigit(1111.9995f, 1112.0f);
        assertRoundOneDigit(1111.9996f, 1112.0f);
    }

    protected void assertRoundThreeDigits(float number, float expected) {
        float actual = NumberUtil.roundThreeDigits(number);
        Assert.assertEquals("" + expected, "" + actual);
    }

    protected void assertRoundOneDigit(float number, float expected) {
        Float actual = NumberUtil.roundOneDigit(number);
        Assert.assertEquals("" + expected, "" + actual);
    }

}
