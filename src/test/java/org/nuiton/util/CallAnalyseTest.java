/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import junit.framework.TestCase;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created: 25 août 2005 21:03:50 CEST
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class CallAnalyseTest extends TestCase { // CallAnalyseTest

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(CallAnalyseTest.class);

    protected List<List<?>> memoryConsume = new ArrayList<List<?>>();

    public void testCall() throws Exception {
        CallAnalyse.activate();
        for (int i = 0; i < 10; i++) {
            eatMemory();
            freeMemory();
        }

        assertEquals(10, CallAnalyse.getThreadStatistics().get("eatMemory").getCalls());
        assertEquals(10, CallAnalyse.getThreadStatistics().get("freeMemory").getCalls());

        log.debug(CallAnalyse.getThreadStatistics());
    }

    protected void eatMemory() {
        CallAnalyse.enter("eatMemory");
        try {
            for (int i = 0; i < 100; i++) {
                memoryConsume.add(new ArrayList(100));
            }
        } catch (Exception eee) {
            // do nothing
        } finally {
            CallAnalyse.exit("eatMemory");
        }
    }

    protected void freeMemory() {
        CallAnalyse.enter("freeMemory");
        try {
            memoryConsume.clear();
        } catch (Exception eee) {
            // do nothing
        } finally {
            CallAnalyse.exit("freeMemory");
        }
    }

} // CallAnalyseTest

