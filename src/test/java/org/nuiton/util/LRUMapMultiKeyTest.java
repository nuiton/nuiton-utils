/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import junit.framework.TestCase;

import java.util.List;

/**
 * Created: 23 mai 2006 04:57:50
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class LRUMapMultiKeyTest extends TestCase {


    /*
     * Test method for 'org.nuiton.util.LRUMapMultiKey.clear()'
     */
    public void testClear() {
        System.out.println("testClear");
        LRUMapMultiKey map = new LRUMapMultiKey(10);
        LRUMapMultiKey.Key key1 = map.createKey("testClear", "toto", "tyty", "tutu");
        LRUMapMultiKey.Key key2 = map.createKey("toto", "titi", "tutu");
        LRUMapMultiKey.Key key3 = map.createKey("toto", "titi", "tata");

        map.put(key1, "value");
        map.put(key2, "value");
        map.put(key3, "value");

        map.clear();

        assertEquals(0, map.size());
        assertEquals(0, map.keys.size());
    }

    /*
     * Test method for 'java.util.WeakHashMap.get(Object)'
     */
    public void testGet() throws Exception {
        System.out.println("testGet");
        LRUMapMultiKey map = new LRUMapMultiKey(1);

        LRUMapMultiKey.Key key1 = map.createKey("toto", "titi", "tutu");
        map.put(key1, "value1");

        assertEquals("value1", map.get(map.createKey("toto", "titi", "tutu")));


        LRUMapMultiKey.Key key2 = map.createKey("tyty");
        map.put(key2, "value2");

        assertEquals(null, map.get(key1));
        assertEquals("value2", map.get(key2));

        assertEquals(1, map.size());
        assertEquals(1, map.keys.size());
    }

    /*
     * Test method for 'java.util.WeakHashMap.remove(Object)'
     */
    public void testRemoveObject() {
        System.out.println("testRemoveObject");
        LRUMapMultiKey map = new LRUMapMultiKey(10);
        LRUMapMultiKey.Key key1 = map.createKey("testRemoveObject", "toto", "tyty", "tutu");
        LRUMapMultiKey.Key key2 = map.createKey("toto", "titi", "tutu");
        LRUMapMultiKey.Key key3 = map.createKey("toto", "titi", "tata");

        map.put(key1, "value");
        map.put(key2, "value");
        map.put(key3, "value");

        Object l = map.remove("titi");

        assertTrue(l instanceof List);
        assertEquals(2, ((List) l).size());
        assertEquals(1, map.size());

        map.remove(key1);

        assertEquals(0, map.size());
    }

}


