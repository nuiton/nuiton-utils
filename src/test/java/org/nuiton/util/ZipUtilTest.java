/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created: 24 août 2006 10:47:21
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class ZipUtilTest {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ZipUtilTest.class);

    protected static final File DIR = new File(System.getProperty("java.home"),
            "bin");

    protected static File testWorkDir;

    protected static File testResourcesDir;

    @BeforeClass
    public static void initTest() throws IOException {
        // get maven env basedir
        String basedir = System.getenv("basedir");
        if (basedir == null) {

            // says basedir is where we start tests.
            basedir = new File("").getAbsolutePath();
        }
        File basedirFile = new File(basedir);
        testWorkDir = new File(basedirFile,
                "target" + File.separator +
                        "surefire-workdir");

        boolean b = testWorkDir.exists() || testWorkDir.mkdirs();
        if (!b) {
            throw new IOException(
                    "Could not create workdir directory " + testWorkDir);
        }

        testResourcesDir = new File(basedirFile,
                "src" + File.separator +
                        "test" + File.separator +
                        "resources" + File.separator +
                        "zip");
    }

    protected File createTempFile(String prefix, String suffix) throws IOException {
        File file = File.createTempFile(prefix, suffix, testWorkDir);
        return file;
    }

    protected File createTempDirectory(String prefix, String suffix) throws IOException {
        File file = FileUtil.createTempDirectory(prefix, suffix, testWorkDir);
        return file;
    }

    @Test
    public void testUncompress() throws IOException {
        //File dir = new File(System.getProperty("user.dir"));        
        File zipFile = createTempFile("testCompressZip", ".zip");
        zipFile.deleteOnExit();
        log.info("Compress " + DIR + " in zip file = " + zipFile);

        ZipUtil.compress(zipFile, DIR, null);

        File ucz = createTempDirectory("testUncompressZip", "");
        log.info("uncompress zip " + zipFile + " in " + ucz);

        ZipUtil.uncompress(zipFile, ucz);

        List<File> src = FileUtil.getFilteredElements(DIR, null, true);
        List<File> dest = FileUtil.getFilteredElements(ucz, null, true);

        // +1 car il y a le rep lui meme dans dest
        Assert.assertEquals(src.size() + 1, dest.size());

        // remove created temp dirs :
        FileUtils.deleteDirectory(ucz);
        Assert.assertFalse(ucz.isDirectory());
    }

    @Test
    public void testCompress() throws IOException {
        //File dir = new File(System.getProperty("user.dir"));
        File zipFile = createTempFile("testCompressZip", ".zip");
        zipFile.deleteOnExit();
        log.info("Compress " + DIR + " in zip file = " + zipFile);

        ZipUtil.compress(zipFile, DIR, null);

        Assert.assertTrue(zipFile.exists());
        Assert.assertTrue(0 != zipFile.length());
    }

    @Test
    public void testCompressFilter() throws IOException {
        //File dir = new File(System.getProperty("user.dir"));
        File zipFile = createTempFile("testCompressZip", ".zip");
        zipFile.deleteOnExit();
        log.info("Compress " + DIR + " in zip file = " + zipFile);

        FileFilter filter = new FileFilter() {
            public boolean accept(File pathname) {
                boolean result;
                result = !pathname.getPath().contains("/target");
                return result;
            }

        };

        ZipUtil.compress(zipFile, DIR, filter);

        Assert.assertTrue(zipFile.exists());
        Assert.assertTrue(0 != zipFile.length());
    }

    @Test
    public void testCompressFile() throws IOException {
        File dir = new File(DIR, "java");
//        File dir = new File(System.getProperty("java.home"), "bin"
//                                                             + File.separator + "java");
        File zipFile = createTempFile("testCompressZip", ".zip");
        zipFile.deleteOnExit();
        log.info("Compress " + dir + " in zip file = " + zipFile);

        ZipUtil.compress(zipFile, dir, null);

        Assert.assertTrue(zipFile.exists());
        Assert.assertTrue(0 != zipFile.length());
    }

    @Test
    public void testCompressFileMD5() throws IOException {
        File dir = new File(DIR, "java");
        File zipFile = createTempFile("testCompressZip", ".zip");
        zipFile.deleteOnExit();
        log.info("Compress " + dir + " in zip file = " + zipFile);

        Collection<File> files = Collections.singleton(dir);
        ZipUtil.compressFiles(zipFile, dir, files, true);

        Assert.assertTrue(zipFile.exists());
        Assert.assertTrue(zipFile.length() > 0);
        File md5File = new File(zipFile.getAbsoluteFile() + ".md5");
        md5File.deleteOnExit();
        Assert.assertTrue(md5File.exists());
        Assert.assertTrue(md5File.length() > 0);
    }

    /**
     * Test la fonction de decompression sans filtres.
     *
     * @throws IOException ?
     */
    @Test
    public void testUncompressFiltredWithoutFilter() throws IOException {
        File archive = new File(testResourcesDir, "test-uncompress.zip");
        File dest = new File(testWorkDir, "testunzip");

        if (log.isInfoEnabled()) {
            log.info("Extracting " + archive + " to " + dest);
        }

        ZipUtil.uncompressFiltred(archive, dest);
        File ruleFile = new File(dest, "test-nonregression-20090203" + File.separator + "scripts" + File.separator + "RuleUtil.java");
        Assert.assertTrue(ruleFile.exists());
    }

    /**
     * Test la fonction de decompression avec filtres.
     *
     * @throws IOException ?
     */
    @Test
    public void testUncompressFiltredWithFilter() throws IOException {
        File archive = new File(testResourcesDir, "test-uncompress.zip");
        File dest = new File(testWorkDir, "testunzip2");
        String pattern = ".*/scripts/.*";

        if (log.isInfoEnabled()) {
            log.info("Extracting " + archive + " to " + dest + " without " + pattern);
        }

        ZipUtil.uncompressFiltred(archive, dest, pattern);
        File ruleFile = new File(dest, "test-nonregression-20090203" + File.separator + "scripts" + File.separator + "RuleUtil.java");
        Assert.assertFalse(ruleFile.exists());
    }

    @Test
    public void testIsZipFile() throws Exception {

        URL resource;
        File file;
        boolean actual;

        // a zip
        resource = getClass().getResource("/zip/this-is-a-zip.zap");
        Assert.assertNotNull(resource);
        file = new File(resource.toURI());
        Assert.assertNotNull(file);
        actual = ZipUtil.isZipFile(file);
        Assert.assertTrue(actual);

        // a zip
        resource = getClass().getResource("/zip/test-uncompress.zip");
        Assert.assertNotNull(resource);
        file = new File(resource.toURI());
        Assert.assertNotNull(file);
        actual = ZipUtil.isZipFile(file);
        Assert.assertTrue(actual);

        // not a zip
        resource = getClass().getResource("/zip/not-a-zip.zip");
        Assert.assertNotNull(resource);
        file = new File(resource.toURI());
        Assert.assertNotNull(file);
        actual = ZipUtil.isZipFile(file);
        Assert.assertFalse(actual);


    }
}
