/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/*
 * Copyright (c) 2011 poussin. All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.nuiton.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class MatrixMapTest {

    @Test
    public void testSubMatrix() {
        List<String> sem1 = Arrays.asList("xA", "xB", "xC", "xD");
        List<String> sem2 = Arrays.asList("ya", "yb", "yc", "yd", "ye");
        MatrixMap<String> m = MatrixMap.Factory.create(new List[]{sem1, sem2});
        
        m.setValue("a1", "xA", "ya");
        m.setValue("a2", "xA", "yb");
        m.setValue("a3", "xA", "yc");
        m.setValue("a4", "xA", "yd");
        m.setValue("a5", "xA", "ye");

        m.setValue("b1", "xB", "ya");
        m.setValue("b2", "xB", "yb");
        m.setValue("b3", "xB", "yc");
        m.setValue("b4", "xB", "yd");
        m.setValue("b5", "xB", "ye");

        m.setValue("c1", "xC", "ya");
        m.setValue("c2", "xC", "yb");
        m.setValue("c3", "xC", "yc");
        m.setValue("c4", "xC", "yd");
        m.setValue("c5", "xC", "ye");

        m.setValue("d1", "xD", "ya");
        m.setValue("d2", "xD", "yb");
        m.setValue("d3", "xD", "yc");
        m.setValue("d4", "xD", "yd");
        m.setValue("d5", "xD", "ye");

        System.out.println(m.toString());

        MatrixMap sub = m.getSubMatrix(0, "xA", "xC");

        System.out.println(sub);
    }

    @Test
    public void testExtend() {
        List<String> sem1 = Arrays.asList("xA", "xB", "xC");
        List<String> sem2 = Arrays.asList("ya", "yb", "yc", "yd", "ye");
        MatrixMap<String> m = MatrixMap.Factory.createElastic(new List[]{sem1, sem2});

        m.setValue("a1", "xA", "ya");
        m.setValue("a2", "xA", "yb");
        m.setValue("a3", "xA", "yc");
        m.setValue("a4", "xA", "yd");
        m.setValue("a5", "xA", "ye");

        m.setValue("b1", "xB", "ya");
        m.setValue("b2", "xB", "yb");
        m.setValue("b3", "xB", "yc");
        m.setValue("b4", "xB", "yd");
        m.setValue("b5", "xB", "ye");

        m.setValue("c1", "xC", "ya");
        m.setValue("c2", "xC", "yb");
        m.setValue("c3", "xC", "yc");
        m.setValue("c4", "xC", "yd");
        m.setValue("c5", "xC", "ye");

        System.out.println(m.toString());

        m.setValue("d1", "xD", "ya");
        m.setValue("d2", "xD", "yb");
        m.setValue("d3", "xD", "yc");
        m.setValue("d4", "xD", "yd");
        m.setValue("d5", "xD", "ye");

        System.out.println(m.toString());
    }

}
