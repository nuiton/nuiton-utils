/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import junit.framework.TestCase;

/**
 * CardinalityHelper Tester.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @version 1.0
 * @since <pre>12/17/2007</pre>
 */
public class CardinalityHelperTest extends TestCase {

    String txt;
    Object[] result;
    StringBuilder sb;

    public void testNoMaxParsing() {
        txt = "*";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "", 0, -1);

        txt = "yo*";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 0, -1);

        txt = "yo+";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 1, -1);

        txt = "yo *";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 0, -1);

        txt = "yo +";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 1, -1);
    }

    public void testExactlyParsing() {
        txt = "{5}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "", 5, 5);

        txt = "yo{1}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 1, 1);

        txt = "yo {2}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 2, 2);

        txt = "yo {12}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 12, 12);
    }

    public void testBoundedParsing() {
        txt = "{1,2}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "", 1, 2);

        txt = "{2,*}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "", 2, -1);

        txt = "yo{1,2}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 1, 2);

        txt = "yo {10,20}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 10, 20);

        txt = "yo {10,*}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo", 10, -1);
    }

    public void testDefaultParsing() {
        txt = "yo {a}";
        result = CardinalityHelper.parseCardinalite(txt, false);
        assertCardinalite(result, "yo {a}", 0, 1);

        txt = "yo {a}";
        result = CardinalityHelper.parseCardinalite(txt, true);
        assertCardinalite(result, "yo {a}", 1, 1);
    }

    public void testPrintCardinality() {

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",0,1,true,"<",">","[","]");
        assertPrint(sb,"[yo]");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",0,5,false,"<",">","[","]");
        assertPrint(sb,"[yo]{0,5}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",0,-1,false,"<",">","[","]");
        assertPrint(sb,"[yo]*");
        
        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",0,1,true,"<",">","","");
        assertPrint(sb,"yo{0,1}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",0,1,false,"<",">","","");
        assertPrint(sb,"yo");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",0,5,false,"<",">","","");
        assertPrint(sb,"yo{0,5}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",0,-1,false,"<",">","","");
        assertPrint(sb,"yo*");

        
        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",1,1,false,"<",">","[","]");
        assertPrint(sb,"<yo>");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",5,5,true,"<",">","[","]");
        assertPrint(sb,"<yo>{5}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",1,5,true,"<",">","[","]");
        assertPrint(sb,"<yo>{1,5}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",1,-1,true,"<",">","[","]");
        assertPrint(sb,"<yo>+");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",3,-1,true,"<",">","[","]");
        assertPrint(sb,"<yo>{3,*}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",1,1,false,"","","[","]");
        assertPrint(sb,"yo{1}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",1,1,true,"","","[","]");
        assertPrint(sb,"yo");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",5,5,true,"","","[","]");
        assertPrint(sb,"yo{5}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",1,5,true,"","","[","]");
        assertPrint(sb,"yo{1,5}");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",1,-1,true,"","","[","]");
        assertPrint(sb,"yo+");

        CardinalityHelper.printCardinalite(sb = new StringBuilder(),"yo",3,-1,true,"","","[","]");
        assertPrint(sb,"yo{3,*}");
    }

    private void assertPrint(StringBuilder sb, String expectedResult) {
        assertEquals(expectedResult,sb.toString());
    }

    private void assertCardinalite(Object[] result, String key, Integer min, Integer max) {
        assertEquals(key, (String) result[0]);
        assertEquals(min, result[1]);
        assertEquals(max, result[2]);
    }

}
