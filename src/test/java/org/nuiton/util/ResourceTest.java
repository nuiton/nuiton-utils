/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Test class for {@link Resource}.
 *
 * @author chatellier
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ResourceTest { // ResourceTest

    /** Logger. */
    private static final Log log = LogFactory.getLog(ResourceTest.class);

    @Test
    public void testGetURL() {

        URL url = Resource.getURL("README.md");
        Assert.assertNotNull(url);

        url = Resource.getURL("/zip/not-a-zip.zip");
        Assert.assertNotNull(url);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetURLNotFound() {
        Resource.getURL("/zip/non-existent-file.zip");
    }

    @Test
    public void testGetURLs() {
        List<URL> result;

        result = Resource.getURLs(".*zip");
        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());

        result = Resource.getURLs(".*zap");
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());

        result = Resource.getURLs(".*zop");
        Assert.assertNotNull(result);
        Assert.assertEquals(0, result.size());

        result = Resource.getURLs(".*windows.properties");
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());

        result = Resource.getURLs(".*properties/windows.properties");
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void testGetURLsFromDirectory() throws Exception {

        assumeNotUnderWindows(getClass(), testName);

        List<URL> list = new ArrayList<URL>();
        File repository = new File(System.getProperty("java.home"));

        // test lorsqu'aucun fichier du repertoire ne correspond au pattern
        Assert.assertEquals(list, Resource.getURLsFromDirectory(repository, ".*.aucunFichierTrouve"));

        File file = new File(repository, "bin" + File.separator + "java");
        list.add(file.toURI().toURL());

        // test qui prouve que la methode retourne le bon fichier
        Assert.assertEquals(list, Resource.getURLsFromDirectory(new File(repository, "bin"), ".*" + File.separator + ".a.a$"));

        // test qui prouve la recursivite (va chercher en profondeur les fichiers)
        Assert.assertEquals(list, Resource.getURLsFromDirectory(repository, ".*" + File.separator + "j.v.$"));
    }

    @Test
    public void testGetURLsFromJarBeforeJava9() throws Exception {
        File repository = new File(System.getProperty("java.home"));
        File file = new File(repository, "lib" + File.separator + "rt.jar");
        Assume.assumeTrue(file.exists());

        List<URL> result = Resource.getURLsFromJar(file, ".*OutOfMemoryError.*");
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void testIsJar() throws Exception {
        Assert.assertTrue(Resource.isJar("toto.jar"));
        Assert.assertTrue(Resource.isJar("toto.JaR"));
        Assert.assertFalse(Resource.isJar("totojar"));
        Assert.assertFalse(Resource.isJar(""));
        Assert.assertFalse(Resource.isJar(null));
    }

    @Test
    public void testIsZip() throws Exception {
        Assert.assertTrue(Resource.isZip("toto.zip"));
        Assert.assertTrue(Resource.isZip("toto.zIp"));
        Assert.assertFalse(Resource.isZip("totojarzip"));
        Assert.assertFalse(Resource.isZip(""));
        Assert.assertFalse(Resource.isZip(null));
    }

    @Test
    public void testIsPattern() {
        Assert.assertTrue(Resource.isPattern(".*.zip"));
        Assert.assertTrue(Resource.isPattern(".?.zip"));
        Assert.assertFalse(Resource.isPattern("toto.zip"));
    }

    /**
     * Test de recherche de resource dans le classpath (jar).
     *
     * @throws IOException ?
     */
    @Test
    public void testGetResourcesJarClassPath() throws IOException {

        assumeNotUnderWindows(getClass(), testName);

        // ce test peut echoué a chaque changement dans les dépendances
        List<URL> urlsPattern = Resource.getResources("org/nuiton/util/beans/Bean.*Test\\.class");
        List<URL> urlsClass1 = Resource.getResources("org/nuiton/util/beans/BeanMonitorTest.class");
        List<URL> urlsClass2 = Resource.getResources("org/nuiton/util/beans/BeanUtilTest.class");
        Assert.assertEquals(2, urlsPattern.size());
        Assert.assertEquals(urlsPattern.size(), urlsClass1.size() + urlsClass2.size());

        // test sans dossier de recherche (directement un pattern)
        List<URL> urlsAllPattern = Resource.getResources("log4j\\..*");
        Assert.assertEquals(1, urlsAllPattern.size());
    }

    /**
     * Test de recherche de resource dans le classpath (dossier hors jar).
     *
     * @throws IOException ?
     */
    @Test
    public void testGetResourcesFileSystemClassPath() throws IOException {
        List<URL> urls = Resource.getResources("META-INF/services/.*apache.commons.*");
        // resources, test resources
        Assert.assertEquals("Should have 1 urls but had: " + urls, 1, urls.size());
    }

    /**
     * Test de recherche de resource dans le classpath (dossier hors jar) en
     * utiliant le classloader de test.
     *
     * @throws IOException ?
     */
    @Test
    public void testGetTestResourcesClassPath() throws IOException {

        assumeNotUnderWindows(getClass(), testName);

        URL url1;
        URL url2;
        url1 = getClass().getResource("/org/nuiton/util/fileUtilData.txt");
        url2 = getClass().getResource("/org/nuiton/util/reverseread.txt");

        List<URL> urls = Resource.getResources("org/nuiton/util/.*\\.txt");
        // there is at least two such files : fileUtilData.txt and
        // reverseread.txt
        Assert.assertTrue(urls.size() >= 2);
        Assert.assertTrue(urls.contains(url1));
        Assert.assertTrue(urls.contains(url2));

        urls = Resource.getResources("org/nuiton/util/.*\\.txt", ResourceTest.class.getClassLoader());

        url1 = ResourceTest.class.getClassLoader().getResource("org/nuiton/util/fileUtilData.txt");
        url2 = ResourceTest.class.getClassLoader().getResource("org/nuiton/util/reverseread.txt");

        // there is at least two such files : fileUtilData.txt and
        // reverseread.txt
        Assert.assertTrue(urls.size() >= 2);
        Assert.assertTrue(urls.contains(url1));
        Assert.assertTrue(urls.contains(url2));
    }

    protected String getJavaExecName() {
        String result;
        if (SystemUtils.IS_OS_WINDOWS) {
            result = "java.exe";
        } else {
            result = "java";
        }
        return result;
    }

    @Rule
    public final TestName testName = new TestName();

    public static void assumeNotUnderWindows(Class<?> testClass, TestName testName) {
        if (SystemUtils.IS_OS_WINDOWS) {
            log.warn("This test " + testClass.getName() + "#" + testName.getMethodName() +
                             " is still not compatible with windows OS");
            Assume.assumeTrue(false);
        }
    }
} // ResourceTest
