package org.nuiton.util.pagination;

/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class PaginationParameterTest {

    @Test
    public void testIndexes() {
        {
            PaginationParameter paginationParameter = PaginationParameter.of(0, 50);
            Assert.assertEquals(0, paginationParameter.getStartIndex());
            Assert.assertEquals(49, paginationParameter.getEndIndex());
        }
        {
            PaginationParameter paginationParameter = PaginationParameter.of(2, 50);
            Assert.assertEquals(100, paginationParameter.getStartIndex());
            Assert.assertEquals(149, paginationParameter.getEndIndex());
        }
        {
            PaginationParameter paginationParameter = PaginationParameter.of(0, -1);
            Assert.assertEquals(0, paginationParameter.getStartIndex());
            Assert.assertEquals(Integer.MAX_VALUE, paginationParameter.getEndIndex());
        }
        {
            PaginationParameter paginationParameter = PaginationParameter.of(0, 19);
            Assert.assertEquals(0, paginationParameter.getStartIndex());
            Assert.assertEquals(18, paginationParameter.getEndIndex());
        }
        {
            PaginationParameter paginationParameter = PaginationParameter.of(1, 19);
            Assert.assertEquals(19, paginationParameter.getStartIndex());
            Assert.assertEquals(37, paginationParameter.getEndIndex());
        }
        {
            PaginationParameter paginationParameter = PaginationParameter.of(2, 19);
            Assert.assertEquals(38, paginationParameter.getStartIndex());
            Assert.assertEquals(56, paginationParameter.getEndIndex());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncoherentNumbers() {
        PaginationParameter paginationParameter = PaginationParameter.of(2, -1);
        paginationParameter.getStartIndex();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidPageSizeTo0() {
        PaginationParameter paginationParameter = PaginationParameter.of(5, 0);
        paginationParameter.getStartIndex();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidPageSizeToMinus5() {
        PaginationParameter paginationParameter = PaginationParameter.of(5, -5);
        paginationParameter.getStartIndex();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidPageNumber() {
        PaginationParameter paginationParameter = PaginationParameter.of(-3, 25);
        paginationParameter.getStartIndex();
    }

    @Test
    public void testEquals() {

        // Tests without order clauses
        Assert.assertEquals(PaginationParameter.ALL, PaginationParameter.ALL);
        Assert.assertEquals(PaginationParameter.ALL, PaginationParameter.of(0, -1));
        Assert.assertNotEquals(PaginationParameter.ALL, PaginationParameter.of(0, 1));
        Assert.assertNotEquals(PaginationParameter.of(1, 1), PaginationParameter.of(0, 1));

        // Tests with order clauses
        Assert.assertNotEquals(
                PaginationParameter.of(0, 5),
                PaginationParameter.of(0, 5, "oc", true));
        Assert.assertEquals(
                PaginationParameter.of(0, 5, "oc", false),
                PaginationParameter.of(0, 5, "oc", false));
        Assert.assertNotEquals(
                PaginationParameter.of(0, 5, "oc", false),
                PaginationParameter.of(0, 5, "oc", true));
        Assert.assertNotEquals(
                PaginationParameter.of(0, 5, "oc5", false, "oc2", true),
                PaginationParameter.of(0, 5, "oc", false, "oc2", true));
        Assert.assertEquals(
                PaginationParameter.of(0, 5, "oc5", false, "oc2", true, "oc3", false),
                PaginationParameter.of(0, 5, "oc5", false, "oc2", true, "oc3", false));

    }

}
