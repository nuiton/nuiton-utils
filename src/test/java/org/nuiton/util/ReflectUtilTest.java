/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util;

import org.junit.Assert;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

/**
 * To test the class {@link ReflectUtil}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class ReflectUtilTest {

    public static class MyClass {

        @SuppressWarnings("")
        @Deprecated
        public String publicField;

        @SuppressWarnings("")
        @Deprecated
        protected String protectedField;

        @SuppressWarnings("")
        @Deprecated
        private String privateField;

        @SuppressWarnings("")
        @Deprecated
        String field;

        @SuppressWarnings("")
        String fieldSkipped;

        @SuppressWarnings("")
        @Deprecated
        public void publicMethod() {

        }

        @SuppressWarnings("")
        @Deprecated
        protected void protectedMethod() {

        }

        @SuppressWarnings("")
        @Deprecated
        private void privateMethod() {

        }

        @SuppressWarnings("")
        @Deprecated
        void method() {

        }

        @SuppressWarnings("")
        void methodSkipped() {

        }
    }

    public static class MySubClass extends MyClass {

        @SuppressWarnings("")
        @Deprecated
        String field2;

        @SuppressWarnings("")
        @Deprecated
        void method2() {

        }
    }

    @Test
    public void getAllDeclaredFields() throws NoSuchFieldException {

        Set<Field> fields;

        fields = ReflectUtil.getAllDeclaredFields(MyClass.class);

        assertGetAllDeclaredFields(fields,
                                   "publicField",
                                   "protectedField",
                                   "privateField",
                                   "field",
                                   "fieldSkipped"
        );

        fields = ReflectUtil.getAllDeclaredFields(MySubClass.class);

        assertGetAllDeclaredFields(fields,
                                   "publicField",
                                   "protectedField",
                                   "privateField",
                                   "field",
                                   "fieldSkipped",
                                   "field2"
        );

    }

    @Test
    public void getAllDeclaredMethods() throws NoSuchFieldException {

        Set<Method> methods;

        methods = ReflectUtil.getAllDeclaredMethods(MyClass.class);

        assertGetAllDeclaredMethods(methods,
                                    "publicMethod",
                                    "protectedMethod",
                                    "privateMethod",
                                    "method",
                                    "methodSkipped"
        );

        methods = ReflectUtil.getAllDeclaredMethods(MySubClass.class);

        assertGetAllDeclaredMethods(methods,
                                    "publicMethod",
                                    "protectedMethod",
                                    "privateMethod",
                                    "method",
                                    "methodSkipped",
                                    "method2"
        );

    }

    @Test
    public void getFieldAnnotation() throws NoSuchFieldException {

        Map<Field, ? extends Annotation> fieldAnnotation;

        // SuppressWarning is source scope so will disappear at compilation :)
        fieldAnnotation = ReflectUtil.getFieldAnnotation(MyClass.class,
                                                         SuppressWarnings.class);

        assertFieldAnnotation(MyClass.class, fieldAnnotation);

        // SuppressWarning is source scope so will disappear at compilation :)
        fieldAnnotation = ReflectUtil.getFieldAnnotation(MyClass.class,
                                                         SuppressWarnings.class,
                                                         true);

        assertFieldAnnotation(MyClass.class, fieldAnnotation);

        // SuppressWarning is source scope so will disappear at compilation :)
        fieldAnnotation = ReflectUtil.getFieldAnnotation(MySubClass.class,
                                                         SuppressWarnings.class);

        assertFieldAnnotation(MySubClass.class, fieldAnnotation);

        // SuppressWarning is source scope so will disappear at compilation :)
        fieldAnnotation = ReflectUtil.getFieldAnnotation(MySubClass.class,
                                                         SuppressWarnings.class,
                                                         true);

        assertFieldAnnotation(MySubClass.class, fieldAnnotation);

        fieldAnnotation = ReflectUtil.getFieldAnnotation(MyClass.class,
                                                         Deprecated.class);

        assertFieldAnnotation(MyClass.class,
                              fieldAnnotation,
                              "publicField",
                              "protectedField",
                              "privateField",
                              "field"
        );


        fieldAnnotation = ReflectUtil.getFieldAnnotation(MyClass.class,
                                                         Deprecated.class,
                                                         true);

        assertFieldAnnotation(MyClass.class,
                              fieldAnnotation,
                              "publicField",
                              "protectedField",
                              "privateField",
                              "field"
        );

        fieldAnnotation = ReflectUtil.getFieldAnnotation(MySubClass.class,
                                                         Deprecated.class);

        assertFieldAnnotation(MySubClass.class,
                              fieldAnnotation,
                              "field2"
        );

        fieldAnnotation = ReflectUtil.getFieldAnnotation(MySubClass.class,
                                                         Deprecated.class,
                                                         true);

        assertFieldAnnotation(MySubClass.class,
                              fieldAnnotation,
                              "publicField",
                              "protectedField",
                              "privateField",
                              "field",
                              "field2"
        );

    }

    @Test
    public void getMethodAnnotation() throws NoSuchMethodException {

        Map<Method, ? extends Annotation> methodAnnotation;

        // SuppressWarning is source scope so will disappear at compilation :)
        methodAnnotation = ReflectUtil.getMethodAnnotation(MyClass.class,
                                                           SuppressWarnings.class);

        assertMethodAnnotation(MyClass.class, methodAnnotation);

        // SuppressWarning is source scope so will disappear at compilation :)
        methodAnnotation = ReflectUtil.getMethodAnnotation(MyClass.class,
                                                           SuppressWarnings.class,
                                                           true);

        assertMethodAnnotation(MyClass.class, methodAnnotation);

        // SuppressWarning is source scope so will disappear at compilation :)
        methodAnnotation = ReflectUtil.getMethodAnnotation(MySubClass.class,
                                                           SuppressWarnings.class);

        assertMethodAnnotation(MySubClass.class, methodAnnotation);

        // SuppressWarning is source scope so will disappear at compilation :)
        methodAnnotation = ReflectUtil.getMethodAnnotation(MySubClass.class,
                                                           SuppressWarnings.class,
                                                           true);

        assertMethodAnnotation(MySubClass.class, methodAnnotation);

        methodAnnotation = ReflectUtil.getMethodAnnotation(MyClass.class,
                                                           Deprecated.class);

        assertMethodAnnotation(MyClass.class,
                               methodAnnotation,
                               "publicMethod",
                               "protectedMethod",
                               "privateMethod",
                               "method"
        );

        methodAnnotation = ReflectUtil.getMethodAnnotation(MyClass.class,
                                                           Deprecated.class,
                                                           true);

        assertMethodAnnotation(MyClass.class,
                               methodAnnotation,
                               "publicMethod",
                               "protectedMethod",
                               "privateMethod",
                               "method"
        );

        methodAnnotation = ReflectUtil.getMethodAnnotation(MySubClass.class,
                                                           Deprecated.class);

        assertMethodAnnotation(MySubClass.class,
                               methodAnnotation,
                               "method2"
        );

        methodAnnotation = ReflectUtil.getMethodAnnotation(MySubClass.class,
                                                           Deprecated.class,
                                                           true);

        assertMethodAnnotation(MySubClass.class,
                               methodAnnotation,
                               "publicMethod",
                               "protectedMethod",
                               "privateMethod",
                               "method",
                               "method2"
        );

    }

    protected void assertGetAllDeclaredFields(Set<Field> fields,
                                              String... fieldNames) {

        Assert.assertNotNull(fields);
        // À cause de l'instrumentation pour la couverture de code, on ne peut pas savoir à l'avance si le nombre est bon mais on connait la taille minimum
        Assert.assertTrue(fields.size() >= fieldNames.length);
        for (String fieldName : fieldNames) {

            Field field = getField(fieldName, fields);

            Assert.assertNotNull("Could not found field named " + fieldName, field);
        }
    }

    protected void assertGetAllDeclaredMethods(Set<Method> methods,
                                               String... methodNames) {

        Assert.assertNotNull(methods);
        // À cause de l'instrumentation pour la couverture de code, on ne peut pas savoir à l'avance si le nombre est bon mais on connait la taille minimum
        Assert.assertTrue(methods.size() >= methodNames.length);
        for (String methodName : methodNames) {

            Method method = getMethod(methodName, methods);
            Assert.assertNotNull("Could not found method named " + methodName, method);
        }

    }

    protected void assertFieldAnnotation(Class<?> type,
                                         Map<Field, ? extends Annotation> fieldAnnotation,
                                         String... fieldNames) {

        Set<Field> allDeclaredFields = ReflectUtil.getAllDeclaredFields(type);

        Assert.assertNotNull(fieldAnnotation);
        Assert.assertEquals(fieldNames.length, fieldAnnotation.size());
        for (String fieldName : fieldNames) {
            Field field = getField(fieldName, allDeclaredFields);
            Assert.assertNotNull(field);
            Annotation annotation = fieldAnnotation.get(field);
            Assert.assertNotNull("Could not found annotation for field " + fieldName, annotation);
        }
    }

    protected void assertMethodAnnotation(Class<?> type,
                                          Map<Method, ? extends Annotation> fieldAnnotation,
                                          String... methodNames) {

        Set<Method> allDeclaredMethods = ReflectUtil.getAllDeclaredMethods(type);

        Assert.assertNotNull(fieldAnnotation);
        Assert.assertEquals(methodNames.length, fieldAnnotation.size());
        for (String methodName : methodNames) {
            Method method = getMethod(methodName, allDeclaredMethods);
            Assert.assertNotNull(method);
            Annotation annotation = fieldAnnotation.get(method);
            Assert.assertNotNull("Could not found annotation for method " + methodName, annotation);
        }
    }

    protected Field getField(String fieldName, Set<Field> fields) {
        for (Field field : fields) {
            if (fieldName.equals(field.getName())) {
                return field;
            }
        }
        return null;
    }

    protected Method getMethod(String methodName, Set<Method> methods) {
        for (Method method : methods) {
            if (methodName.equals(method.getName())) {
                return method;
            }
        }
        return null;
    }
}
