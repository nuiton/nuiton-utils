/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;


/**
 * Created: 22 mai 2006 15:41:49
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class TransparenteReferenceTest extends TestCase {

    public void testHash() {
        Map<TransparenteSoftReference<?>, TransparenteSoftReference<?>> map = new HashMap<TransparenteSoftReference<?>, TransparenteSoftReference<?>>();

        String key = "key";
        TransparenteSoftReference<?> keyRef = new TransparenteSoftReference<String>(key);

        String value = "value";
        TransparenteSoftReference<?> valueRef = new TransparenteSoftReference<String>(value);

        map.put(keyRef, valueRef);


        assertEquals(valueRef, map.get(keyRef));
        assertEquals(valueRef.get(), map.get(keyRef).get());
        key = null;
        value = null;

        System.gc();
        Thread.yield();

        assertEquals("value".hashCode(), map.get(keyRef).hashCode());
    }
}


