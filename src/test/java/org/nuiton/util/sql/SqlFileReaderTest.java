package org.nuiton.util.sql;

/*-
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import junit.framework.TestCase;

/**
 * Test the SqlFileReader.
 * 
 * @author jruchaud
 */
public class SqlFileReaderTest extends TestCase {
    
    public void assertIterator(Iterable<String> iterable, String[] expectedValues) {
        int index = 0;
        for (String value : iterable) {
            String expectedValue = expectedValues[index].replaceAll("\n*$", "");
            assertEquals(expectedValue, value);
            index ++;
        }
        
        assertEquals(expectedValues.length, index);
    }
    
    public SqlFileReader getSqlFileReader(String[] sql) {
        String content = "";
        for (String value : sql) {
            content += value;
        }
        SqlFileReader reader = new SqlFileReader(content);
        return reader;
    }
    
    public void testBasic() {
        String[] sql = {"INSERT INTO client (prenom, age) VALUES ('John', 11)"};
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
    public void testBasicWithSemiColon() {
        String[] sql = {"INSERT INTO client (prenom, age) VALUES ('John', 11);", ""};
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
    public void testBasicMultiInserts() {
        String[] sql = {
            "INSERT INTO client (prenom, age) VALUES ('John', 11);\n\n\n\n\n",
            "INSERT INTO client (prenom, age) VALUES ('Jack', 12);",
            "INSERT INTO client (prenom, age) VALUES ('Boby', 13);",
            ""
        };
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
    public void testQuoteWithSemiColon() {
        String[] sql = {"INSERT INTO client (prenom, age) VALUES ('Jo;hn', 11)"};
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
    public void testQuoteWithQuote() {
        String[] sql = {"INSERT INTO client (prenom, age) VALUES ('Jo''hn', 11)"};
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
    public void testQuoteMultiInserts() {
        String[] sql = {
            "INSERT INTO client (prenom, age) VALUES ('John\n', 11);",
            "INSERT INTO client (prenom, age) VALUES ('Jack;\n', 12);",
            "INSERT INTO client (prenom, age) VALUES ('Boby', 13);",
            ""
        };
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
    public void testComment() {
        String[] sql = {"-- Comment"};
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
    public void testCommentWithSemiColon() {
        String[] sql = {"-- Comm;ent"};
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
    public void testCommentMultiInserts() {
        String[] sql = {
            "INSERT INTO client (prenom, age) VALUES ('John', 11);\n",
            "-- Comment\n",
            "-- Comment\n",
            "INSERT INTO client (prenom, age) VALUES ('Jack', 12);",
            "INSERT INTO client (prenom, age) VALUES ('Boby', 13);",
            ""
        };
        
        SqlFileReader reader = getSqlFileReader(sql);
        assertIterator(reader, sql);
    }
    
}
