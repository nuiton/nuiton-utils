/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;

/**
 * Test for reverse reader utility.
 * 
 * @author chatellier
 */
public class ReverseFileReaderTest {

    /**
     * Test to read test file in reverse order.
     * 
     * @throws IOException ?
     */
    @Test
    public void testReverseRead() throws IOException {
        URL url = ReverseFileReaderTest.class.getResource("reverseread.txt");
        String reverseFile = url.getFile();
        ReverseFileReader reader = new ReverseFileReader(reverseFile);

        String lastRead = reader.readLine();
        Assert.assertEquals("Line 4", lastRead);
        
        lastRead = reader.readLine();
        Assert.assertEquals("Line 3", lastRead);
        
        lastRead = reader.readLine();
        Assert.assertEquals("Line 2", lastRead);
        
        reader.close();
    }
}
