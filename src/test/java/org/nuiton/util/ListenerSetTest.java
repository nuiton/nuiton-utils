/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * ListenerSetTest.java
 *
 * Created: 10 mai 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * Copyright Code Lutin
 *
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.util;

import junit.framework.TestCase;

public class ListenerSetTest extends TestCase { // ListenerSetTest

    int callCount;

    /** si la class n'est pas public fire ne retrouve pas les méthodes :( */
    public class Listener {
        public void event1() {
            callCount++;
        }

        public void event2(Object o) {
            callCount++;
        }
    }

    class PrivateListener {
        public void event1() {
            callCount++;
        }

        public void event2(Object o) {
            callCount++;
        }
    }

    public void testAdd() throws Exception {
        ListenerSet<Object> set = new ListenerSet<Object>();
        Listener l = new Listener();

        // ajout d'un listener
        set.add(l);

        callCount = 0;
        set.fire("event1");
        assertEquals(1, callCount);

        // ajout du meme listener
        set.add(l);
        callCount = 0;
        set.fire("event1");
        assertEquals(1, callCount);

        // destruction de la reference sur le listener
        l = null;
        System.gc();

        callCount = 0;
        set.fire("event1");
        assertEquals(0, callCount);
    }

    public void testAddContrained() throws Exception {
        ListenerSet<Listener> set = new ListenerSet<Listener>();
        Listener l = new Listener();

        // ajout d'un listener
        set.add(l);

        callCount = 0;
        set.fire("event1");
        assertEquals(1, callCount);
    }

    public void testRemove() throws Exception {
        ListenerSet<Object> set = new ListenerSet<Object>();
        Listener l = new Listener();
        set.add(l);

        callCount = 0;
        set.fire("event1");
        assertEquals(1, callCount);

        set.remove(l);

        callCount = 0;
        set.fire("event1");
        assertEquals(0, callCount);
    }

    public void testFire() throws Exception {
        ////////////////////////////////////////////////:
        // avec le listener public
        //
        {
            ListenerSet<Object> set = new ListenerSet<Object>();
            Listener l = new Listener();
            set.add(l);

            callCount = 0;
            set.fire("event1");
            assertEquals(1, callCount);

            callCount = 0;
            set.fire("event2", null);
            assertEquals(1, callCount);
        }
        ////////////////////////////////////////////////:
        // avec le listener privé
        //
        {
            ListenerSet<Object> set = new ListenerSet<Object>();
            PrivateListener li = new PrivateListener();
            set.add(li);

            callCount = 0;
            for (Object aSet : set) {
                PrivateListener l = (PrivateListener) aSet;
                l.event1();
            }
            assertEquals(1, callCount);

            callCount = 0;
            for (Object aSet : set) {
                PrivateListener l = (PrivateListener) aSet;
                l.event2(null);
            }
            assertEquals(1, callCount);
        }
    }

} // ListenerSetTest

