/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created: 22 nov. 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 */
public class FileUtilTest { // FileUtilTest

    public static final long TIMESTAMP = System.nanoTime();

    @Rule
    public final TestName testName = new TestName();

    protected File parent;

    @Before
    public void setUp() {

        parent = FileUtil.getTestSpecificDirectory(
                getClass(),
                testName.getMethodName(), null, TIMESTAMP);

    }

    @Test
    public void testFind() throws Exception {
        List<File> result = FileUtil.find(new File("."), ".*FileUtil.*", true);
        Assert.assertTrue(result.size() != 0);
    }

    @Test
    public void testBasename() throws Exception {
        String result = FileUtil.basename(new File("/tmp/toto.xml"), ".xml");
        Assert.assertEquals("toto", result);
    }

    @Test
    public void testExtension() throws Exception {
        String result = FileUtil.extension(new File("/tmp/toto.xml"));
        Assert.assertEquals("xml", result);
        result = FileUtil.extension(new File("/tmp/toto.xml"), ".", "o");
        Assert.assertEquals("xml", result);
        result = FileUtil.extension(new File("/tmp/toto.xml"), "t", ".");
        Assert.assertEquals("o.xml", result);
    }

    @Test
    public void testCopyRecursively() throws Exception {
        File srcDir = FileUtil.createTempDirectory("test-copyRecursively", "", parent);
        File destDir1 = FileUtil.createTempDirectory("test-copyRecursively", "", parent);
        File destDir2 = FileUtil.createTempDirectory("test-copyRecursively", "", parent);

        new File(srcDir, "toto").createNewFile();
        new File(srcDir, "titi").createNewFile();
        new File(srcDir, "tutu").createNewFile();
        new File(srcDir, "tata").createNewFile();
        File subdir = new File(srcDir, "subdir");
        subdir.mkdirs();
        new File(subdir, "tyty").createNewFile();
        new File(subdir, "titi").createNewFile();

        FileUtil.copyRecursively(srcDir, destDir1);
        FileUtil.copyRecursively(srcDir, destDir2, ".*titi$");

        // remove created temp dirs :
        FileUtils.deleteDirectory(srcDir);
        FileUtils.deleteDirectory(destDir1);
        FileUtils.deleteDirectory(destDir2);
    }

    /**
     * Test grep on a single file.
     * <p>
     * Search for grep() method count.
     *
     * @throws IOException ?
     */
    @Test
    public void testGrepSingleFile() throws IOException {

        // search for 
        File testUtilFile = FileUtil.find(new File("."), ".*fileUtilData\\.txt", true).get(0);

        List<CharSequence> lines = FileUtil.grep("grep\\(String .*\\)", testUtilFile, "UTF-8");

        Assert.assertNotNull(lines);
        Assert.assertEquals(4, lines.size());

        // assert result are ordered
        Assert.assertTrue(lines.get(0).toString().contains("CharBuffer"));
        Assert.assertTrue(lines.get(1).toString().contains("File f,"));
        Assert.assertTrue(lines.get(2).toString().contains("rootDirectory"));
        Assert.assertTrue(lines.get(3).toString().contains("String searchRegex, String fileRegex, String encoding"));
    }

    /**
     * Test grep on a multiple files.
     * <p>
     * Try to find all java file containing "CodeLutin". Can fail if some
     * src files are deleted.
     *
     * @throws IOException ?
     */
    @Test
    public void testGrepMultiple() throws IOException {

        File rootDir = new File("src");

        Map<File, List<CharSequence>> results = FileUtil.grep("CodeLutin", rootDir, ".*\\.java", "UTF-8");

        Assert.assertTrue("should have more than 50 files, but found : " +
                results.size(), results.size() > 50);

    }

    /**
     * Test sed method on a single file.
     *
     * @throws IOException ?
     */
    @Test
    public void testSedSingleFile() throws IOException {
        //FIXME tchemit 20100924 Do tests in target, not in /tmp please!

        ResourceTest.assumeNotUnderWindows(getClass(), testName);

        // try to not make sed in real src dir ;)
        File testDirectory = FileUtil.createTempDirectory("sed", "test");
        FileUtil.copyRecursively(new File("src").getAbsoluteFile(), testDirectory);
        File testUtilFile = FileUtil.find(testDirectory, ".*fileUtilData\\.txt", true).get(0);

        List<CharSequence> lines = FileUtil.grep("grep\\(String .*\\)", testUtilFile, "UTF-8");
        Assert.assertEquals(4, lines.size());

        lines = FileUtil.grep("sedfoo", testUtilFile, "UTF-8");
        Assert.assertNull(lines);

        // real method to test here : sed
        FileUtil.sed("grep", "sedfoo", testUtilFile, "UTF-8");

        lines = FileUtil.grep("grep\\(String .*\\)", testUtilFile, "UTF-8");
        Assert.assertNull(lines);

        lines = FileUtil.grep("sedfoo", testUtilFile, "UTF-8");
        Assert.assertEquals(9, lines.size());

        // clean
        FileUtils.deleteDirectory(testDirectory);
    }

    /**
     * Test sed on a multiple files.
     * <p>
     * Try to replace all "CodeLutin" by "nuiton" in all files. Can fail if some
     * src files are deleted.
     *
     * @throws IOException ?
     */
    @Test
    public void testSedMultiple() throws IOException {

        //FIXME tchemit 20100924 Do tests in target, not in /tmp please!

        ResourceTest.assumeNotUnderWindows(getClass(), testName);

        // try to not make sed in real src dir ;)
        File testDirectory = FileUtil.createTempDirectory("sed", "test");
        FileUtil.copyRecursively(new File("src").getAbsoluteFile(), testDirectory);

        Map<File, List<CharSequence>> results = FileUtil.grep("CodeLutin", testDirectory, ".*\\.java", "UTF-8");
        Assert.assertTrue("should have more than 50 files, but found : " +
                results.size(), results.size() > 50);

        FileUtil.sed("CodeLutin", "Nuiton", testDirectory, ".*\\.java", "UTF-8");

        results = FileUtil.grep("CodeLutin", testDirectory, ".*\\.java", "UTF-8");
        Assert.assertTrue(results.isEmpty());

        results = FileUtil.grep("Nuiton", testDirectory, ".*\\.java", "UTF-8");
        Assert.assertTrue("should have more than 50 files, but found : " +
                results.size(), results.size() > 50);


        // clean
        FileUtils.deleteDirectory(testDirectory);
    }

    /**
     * Test that sed result which produce shorter file work as well.
     *
     * @throws IOException ?
     */
    @Test
    public void testSedComment() throws IOException {

        ResourceTest.assumeNotUnderWindows(getClass(), testName);

        // try to not make sed in real src dir ;)
        File testDirectory = FileUtil.createTempDirectory("sed", "test", parent);
        FileUtil.copyRecursively(new File("src").getAbsoluteFile(), testDirectory);
        File testUtilFile = FileUtil.find(testDirectory, ".*fileUtilData\\.txt", true).get(0);

        // real method to test here : sed
        FileUtil.sed(".*\\*.*", "** skipped **", testUtilFile, "UTF-8");

        List<CharSequence> lines = FileUtil.grep("/var/tmp/bidulle", testUtilFile, "UTF-8");
        Assert.assertNull(lines);

        // test une presence en debut et fin de fichier
        List<CharSequence> lines2 = FileUtil.grep("// FileUtil", testUtilFile, "UTF-8");
        Assert.assertEquals(2, lines2.size());

        // clean
        FileUtils.deleteDirectory(testDirectory);
    }

    public void testChangeExtension() throws IOException {
        String name = "toto.yo";
        String exepectedName = "toto.ya";
        String newExtension = "ya";
        String actualName = FileUtil.changeExtension(name, newExtension);

        Assert.assertEquals(exepectedName, actualName);

        File file = new File(parent, name);
        File expectedFile = new File(parent, exepectedName);
        File actualFile = FileUtil.changeExtension(file, newExtension);
        Assert.assertEquals(expectedFile, actualFile);
    }

    public void testGetRelativeFile() {
        File inputDirectory = new File(parent, "in");
        File outputDirectory = new File(parent, "out");
        File file = new File(inputDirectory, "yoyo.to");
        File expectedFile = new File(outputDirectory, file.getName());
        File actualFile = FileUtil.getRelativeFile(inputDirectory, outputDirectory, file);
        Assert.assertEquals(expectedFile, actualFile);

        file = new File(inputDirectory, "rep" + File.separator + "yoyo.to");
        expectedFile = new File(outputDirectory, "rep" + File.separator + file.getName());
        actualFile = FileUtil.getRelativeFile(inputDirectory, outputDirectory, file);
        Assert.assertEquals(expectedFile, actualFile);
    }

    @Test
    public void testGetFileFromPaths() {
        File actual = FileUtil.getFileFromPaths(parent, "target", "surefire-workdir", "FileUtil");
        File expected = new File(parent, "target" + File.separator + "surefire-workdir" + File.separator + "FileUtil");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetFileFromFQN() {
        File actual = FileUtil.getFileFromFQN(parent, "target.surefire-workdir.FileUtil");
        File expected = new File(parent, "target" + File.separator + "surefire-workdir" + File.separator + "FileUtil");
        Assert.assertEquals(expected, actual);
    }

} // FileUtilTest
