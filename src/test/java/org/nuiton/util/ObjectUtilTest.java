/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created: 19 nov. 07 12:39:28
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class ObjectUtilTest extends Assert {

    @Test
    public void testCall() throws Exception {
        Dummy dummy = new Dummy();

        List<Method> methods = ObjectUtil.getMethod(Dummy.class, "setfile", true);
        assertEquals(1, methods.size());
        ObjectUtil.call(dummy, methods.get(0), "/tmp");
        assertEquals(new File("/tmp"), dummy.getFile());

        methods = ObjectUtil.getMethod(Dummy.class, "setAllFile", true);
        assertEquals(1, methods.size());
        ObjectUtil.call(dummy, methods.get(0), "toto", "/tmp", "/tmp/titi");
        assertEquals("toto", dummy.getName());
        assertEquals(Arrays.asList(new File("/tmp"), new File("/tmp/titi")),
                Arrays.asList(dummy.getAllFile()));

        ObjectUtil.call(dummy, methods.get(0), "toto");
        assertEquals("toto", dummy.getName());
        assertEquals(new ArrayList<File>(),
                Arrays.asList(dummy.getAllFile()));
    }

    @Test
    public void testClone() throws Exception {
        ArrayList c = new ArrayList();
        ArrayList inner = new ArrayList();
        c.add(inner);

        ArrayList n = ObjectUtil.clone(c);
        Assert.assertNotSame(c, n);
        Assert.assertSame(c.get(0), n.get(0));
    }

    @Test
    public void testDeepClone() throws Exception {
        ArrayList c = new ArrayList();
        ArrayList inner = new ArrayList();
        c.add(inner);

        ArrayList n = ObjectUtil.deepClone(c);
        Assert.assertNotSame(c, n);
        Assert.assertNotSame(c.get(0), n.get(0));
    }

    @Test
    public void testCreate() throws Exception {
        Object o = ObjectUtil.create("java.lang.StringBuffer");
        assertTrue(o != null);
        assertTrue(o instanceof StringBuffer);

        Dummy dummy = (Dummy) ObjectUtil.create(
                "org.nuiton.util.ObjectUtilTest$Dummy(name=\"coucou le monde\", file=/tmp/fileTest, integer=50)");
        assertTrue(dummy != null);
        assertEquals("coucou le monde", dummy.getName());
        assertEquals(50, dummy.getInteger());
        assertEquals(new File("/tmp/fileTest"), dummy.getFile());
    }

    @Test
    public void testIsNullValue() throws Exception {

        assertTrue(ObjectUtil.isNullValue(null));

        assertTrue(ObjectUtil.isNullValue(false));
        assertTrue(ObjectUtil.isNullValue(0));
        assertTrue(ObjectUtil.isNullValue((char) 0));
        assertTrue(ObjectUtil.isNullValue(0f));
        assertTrue(ObjectUtil.isNullValue(0d));
        assertTrue(ObjectUtil.isNullValue((byte) 0));

        assertFalse(ObjectUtil.isNullValue(Boolean.FALSE));
        assertFalse(ObjectUtil.isNullValue(Integer.valueOf(0)));
        assertFalse(ObjectUtil.isNullValue(Character.valueOf((char) 0)));
        assertFalse(ObjectUtil.isNullValue(Float.valueOf(0)));
        assertFalse(ObjectUtil.isNullValue(Double.valueOf(0)));
        assertFalse(ObjectUtil.isNullValue(Byte.valueOf((byte) 0)));
    }

    public static class Dummy {

        String name;
        int integer;
        File file;
        File[] allFile;

        public File[] getAllFile() {
            return allFile;
        }

        public void setAllFile(String name, File... allFile) {
            this.name = name;
            this.allFile = allFile;
        }

        /**
         * @return the file
         */
        public File getFile() {
            return this.file;
        }

        /**
         * @return the integer
         */
        public int getInteger() {
            return this.integer;
        }

        /**
         * @return the name
         */
        public String getName() {
            return this.name;
        }

        /**
         * @param file the file to set
         */
        public void setFile(File file) {
            this.file = file;
        }

        /**
         * @param integer the integer to set
         */
        public void setInteger(int integer) {
            this.integer = integer;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }
    }
}
