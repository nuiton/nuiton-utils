/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

/**
 * CollectionUtil Tester.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @version 1.0
 * @since <pre>02/04/2008</pre>
 */
public class CollectionUtilTest extends TestCase {
    public CollectionUtilTest(String name) {
        super(name);
    }

    public void testToGenericList() throws Exception {
        List<String> list = new ArrayList<String>();
        list.add("a");
        list.add("b");
        list.add("c");

        assertEquals(CollectionUtil.toGenericList(list, String.class), list);
        assertEquals(CollectionUtil.toGenericList(list, Object.class), list);

        try {
            CollectionUtil.toGenericList(list, Integer.class);
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }

    }

}
