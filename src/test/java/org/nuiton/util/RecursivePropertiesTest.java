/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */ 

package org.nuiton.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Test recursives properties class.
 *
 * @author chatellier
 */
public class RecursivePropertiesTest {
    
    /**
     * Test string by \\ in value.
     * 
     * @throws IOException ?
     */
    @Test
    public void testWindowsFile() throws IOException {
        RecursiveProperties props = new RecursiveProperties();
        props.load(new FileInputStream("src/test/resources/properties/windows.properties"));
        
        Assert.assertEquals("C:\\Documents and Settings\\guest\\.ssh\\id_rsa", props.getProperty("vcs.ssh.keyFile"));
    }
    
    /**
     * Test with recursive properties.
     * 
     * @throws IOException ?
     */
    @Test
    public void testRecursive() throws IOException {
        RecursiveProperties props = new RecursiveProperties();
        props.load(new FileInputStream("src/test/resources/properties/recursive.properties"));
        
        Assert.assertEquals("World", props.getProperty("username"));
        Assert.assertEquals("Hello World !", props.getProperty("helloWorld"));
    }
}
