/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import junit.framework.TestCase;

/**
 * Created: 3 janv. 2006 23:27:42
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */
public class CategorisedListenerSetTest extends TestCase {

    /*
     * Test method for
     * 'org.nuiton.util.CategorisedListenerSet.getAllListeners(Object)'
     */
    public void testGetAllListeners() {

        {
            CategorisedListenerSet<Object> cls = new CategorisedListenerSet<Object>();

            String[] ls = new String[]{"Double", "Long", "Number", "Object",
                    "LoggingException", "Throwable"};

            cls.add(Double.class, ls[0]);
            cls.add(Long.class, ls[1]);
            cls.add(Number.class, ls[2]);
            cls.add(Object.class, ls[3]);

            assertEquals(3, cls.getAllListeners(Double.class).size());
            assertEquals(3, cls.getAllListeners(Long.class).size());
            assertEquals(2, cls.getAllListeners(Number.class).size());
            assertEquals(1, cls.getAllListeners(Object.class).size());

            cls.addCategory(LoggingException.class, Number.class);

            cls.add(LoggingException.class, ls[4]);
            cls.add(Throwable.class, ls[5]);

            assertEquals(5, cls.getAllListeners(Double.class).size());
            assertEquals(5, cls.getAllListeners(Long.class).size());
            assertEquals(4, cls.getAllListeners(Number.class).size());
            assertEquals(1, cls.getAllListeners(Object.class).size());

            assertEquals(3, cls.getAllListeners(LoggingException.class).size());
            assertEquals(2, cls.getAllListeners(Throwable.class).size());

        }
        {
            CategorisedListenerSet<Object> cls = new CategorisedListenerSet<Object>(false);

            String[] ls = new String[]{"Double", "Long", "Number", "Object",
                    "LoggingException", "Throwable"};

            cls.add(Double.class, ls[0]);
            cls.add(Long.class, ls[1]);
            cls.add(Number.class, ls[2]);
            cls.add(Object.class, ls[3]);

            assertEquals(1, cls.getAllListeners(Double.class).size());
            assertEquals(1, cls.getAllListeners(Long.class).size());
            assertEquals(1, cls.getAllListeners(Number.class).size());
            assertEquals(1, cls.getAllListeners(Object.class).size());

            cls.addCategory(LoggingException.class, Number.class);

            cls.add(LoggingException.class, ls[4]);
            cls.add(Throwable.class, ls[5]);

            assertEquals(1, cls.getAllListeners(Double.class).size());
            assertEquals(1, cls.getAllListeners(Long.class).size());
            assertEquals(2, cls.getAllListeners(Number.class).size());
            assertEquals(1, cls.getAllListeners(Object.class).size());

            assertEquals(1, cls.getAllListeners(LoggingException.class).size());
            assertEquals(1, cls.getAllListeners(Throwable.class).size());
        }
    }

}
