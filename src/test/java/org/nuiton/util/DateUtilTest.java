/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author fdesbois
 * @since 1.4.1
 */
public class DateUtilTest {

    /** Logger */
    private static final Log log = LogFactory.getLog(DateUtilTest.class);

    /**
     * Test of createDate method, of class DateUtil.
     */
    @Test
    public void testCreateDate() {
        log.info("createDate");

        Date newDate = DateUtil.createDate(3, 3, 2009);
        Calendar calendar = DateUtil.getDefaultCalendar(newDate);

        Assert.assertEquals(3, calendar.get(Calendar.DAY_OF_MONTH));
        Assert.assertEquals(2, calendar.get(Calendar.MONTH));
        Assert.assertEquals(2009, calendar.get(Calendar.YEAR));

        Assert.assertEquals(0, calendar.get(Calendar.HOUR));
        Assert.assertEquals(0, calendar.get(Calendar.MINUTE));
        Assert.assertEquals(0, calendar.get(Calendar.SECOND));
        Assert.assertEquals(0, calendar.get(Calendar.MILLISECOND));
    }

    /**
     * Non regression test for bug #1157
     * @since 1.5.2
     * @throws ParseException ?
     */
    @Test
    public void testCreateDateIsDeterministic() throws ParseException {
        Date createdDate = DateUtil.createDate(0, 33, 18, 22, 12, 2010);
        Date parsedDate = DateUtil.parseDate("22/12/2010 18:33", "dd/MM/yyyy HH:mm");
        log.info(createdDate.getTime() + " ?= " + parsedDate.getTime());
        Assert.assertEquals(createdDate, parsedDate);
    }

    /**
     * Test of setLastDayOfMonth method, of class DateUtil.
     */
    @Test
    public void testSetFistLastDayOfMonth() {
        Date february = DateUtil.createDate(22, 2, 2011);
        Date firstDayOfFebruary = DateUtil.setFirstDayOfMonth(february);
        Date lastDayOfFebruary = DateUtil.setLastDayOfMonth(february);

        Date day1 = DateUtil.createDate(0, 0, 2, 1, 2, 2011);
        Date day2 = DateUtil.createDate(67, 45, 23, 28, 2, 2011);

        if (log.isDebugEnabled()) {
            log.debug("first day of february = " + firstDayOfFebruary + ", last day of february = "
                    + lastDayOfFebruary + ", day1 = " + day1 + ", day 2 = " + day2);
        }

        Assert.assertTrue(DateUtil.between(day1, firstDayOfFebruary, lastDayOfFebruary));

        // In this case, day2'hour is after lastDayOfMonth
        Assert.assertFalse(DateUtil.between(day2, firstDayOfFebruary, lastDayOfFebruary));
    }
    /**
     * Test of setLastDayOfMonth method, of class DateUtil.
     */
    @Test
    public void testSetFistLastDayOfYear() {
        Date february = DateUtil.createDate(22, 2, 2011);
        Date firstDayOf2011 = DateUtil.setFirstDayOfYear(february);
        Date lastDayOf2011 = DateUtil.setLastDayOfYear(february);

        Date day1 = DateUtil.createDate(0, 0, 2, 1, 2, 2011);
        Date day2 = DateUtil.createDate(67, 45, 23, 28, 2, 2012);

        if (log.isDebugEnabled()) {
            log.debug("first day of february = " + firstDayOf2011 + ", last day of february = "
                    + lastDayOf2011 + ", day1 = " + day1 + ", day 2 = " + day2);
        }

        Assert.assertTrue(DateUtil.between(day1, firstDayOf2011, lastDayOf2011));
        Assert.assertFalse(DateUtil.between(day2, firstDayOf2011, lastDayOf2011));
    }

    /**
     * Test of between method, of class DateUtil.
     */
    @Test
    public void testBetween() {
        log.info("between");

        Date middle = DateUtil.createDate(3, 3, 2009);

        // middle = begin, and end = null
        Date begin = DateUtil.createDate(3, 3, 2009);
        Date end = null;
        
        boolean result = DateUtil.between(middle, begin, end);
        Assert.assertTrue(result);

        // middle between the period : march 2009
        PeriodDates period = new PeriodDates(begin, begin);
        period.initDayOfMonthExtremities();

        log.info("period : " + period);

        result = DateUtil.between(middle, period.getFromDate(), period.getThruDate());
        Assert.assertTrue(result);

        // middle = period begin
        middle = DateUtil.setFirstDayOfMonth(middle);
        result = DateUtil.between(middle, period.getFromDate(), period.getThruDate());
        Assert.assertTrue(result);

        // test with different implementation of Date
        Timestamp middle2 = new Timestamp(middle.getTime());
        result = DateUtil.between(middle2, period.getFromDate(), period.getThruDate());
        Assert.assertTrue(result);

        // middle before begin
        middle = DateUtil.createDate(2, 2, 2009);
        result = DateUtil.between(middle, period.getFromDate(), period.getThruDate());
        Assert.assertFalse(result);
    }

    @Test
    public void testCetVsCest() {
        Date observationBeginDay = DateUtils.truncate(DateUtil.createDate(0, 0, 21, 18, 3, 2011), Calendar.DAY_OF_MONTH);
        Date observationEndDay = DateUtils.truncate(DateUtil.createDate(0, 0, 23, 28, 3, 2011), Calendar.DAY_OF_MONTH);
        int observationTimeInDays = DateUtil.getDifferenceInDays(observationBeginDay, observationEndDay);
        Assert.assertEquals(10, observationTimeInDays);
    }

    @Test
    public void testGetDifferenceInSeconds() {
        log.info("getDifferenceInSecondes");

        Date beginDate = DateUtil.createDate(30, 10, 0, 3, 2, 2009);
        Date endDate = DateUtil.createDate(0, 11, 0, 3, 2, 2009);

        int result = DateUtil.getDifferenceInSeconds(beginDate, endDate);
        Assert.assertEquals(30, result);

        beginDate = DateUtil.createDate(9, 28, 0, 28, 1, 2009);
        endDate = DateUtil.createDate(50, 30, 0, 28, 1, 2009);

        result = DateUtil.getDifferenceInSeconds(beginDate, endDate);
        Assert.assertEquals(161, result);
    }

    @Test
    public void testGetDifferenceInMinutes() {
        log.info("getDifferenceInMinutes");

        Date beginDate = DateUtil.createDate(30, 10, 0, 3, 2, 2009);
        Date endDate = DateUtil.createDate(0, 12, 0, 3, 2, 2009);

        int result = DateUtil.getDifferenceInMinutes(beginDate, endDate);
        Assert.assertEquals(1, result);

        beginDate = DateUtil.createDate(9, 28, 0, 28, 1, 2009);
        endDate = DateUtil.createDate(50, 30, 0, 28, 1, 2009);

        result = DateUtil.getDifferenceInMinutes(beginDate, endDate);
        Assert.assertEquals(2, result);
    }

    @Test
    public void testGetDifferenceInHours() {
        log.info("getDifferenceInHours");

        Date beginDate = DateUtil.createDate(30, 10, 0, 3, 2, 2009);
        Date endDate = DateUtil.createDate(0, 11, 0, 4, 2, 2009);

        int result = DateUtil.getDifferenceInHours(beginDate, endDate);
        Assert.assertEquals(24, result);

        beginDate = DateUtil.createDate(9, 28, 0, 28, 1, 2009);
        endDate = DateUtil.createDate(50, 30, 8, 28, 1, 2009);

        result = DateUtil.getDifferenceInHours(beginDate, endDate);
        Assert.assertEquals(8, result);
    }

    @Test
    public void testGetDifferenceInDays() {
        log.info("getDifferenceInDays");

        Date beginDate = DateUtil.createDate(3, 2, 2009);
        Date endDate = DateUtil.createDate(8, 2, 2009);

        int result = DateUtil.getDifferenceInDays(beginDate, endDate);
        Assert.assertEquals(5, result);

        beginDate = DateUtil.createDate(28, 1, 2009);
        endDate = DateUtil.createDate(8, 2, 2009);
        result = DateUtil.getDifferenceInDays(beginDate, endDate);
        Assert.assertEquals(11, result);

        beginDate = DateUtil.createDate(1,1,1996);
        endDate = DateUtil.createDate(5,3,2007);
        result = DateUtil.getDifferenceInDays(beginDate, endDate);
        Assert.assertEquals(4081, result);
    }

    @Test
    public void testGetDifferenceInMonths() {
        log.info("getDifferenceInMonths");

        Date beginDate = DateUtil.createDate(3, 2, 2009);
        Date endDate = DateUtil.createDate(8, 8, 2010);

        int result = DateUtil.getDifferenceInMonths(beginDate, endDate);
        log.info("result1 : " + result);
        Assert.assertEquals(19, result);

        beginDate = DateUtil.createDate(1, 1, 2009);
        endDate = DateUtil.createDate(28, 2, 2009);

        result = DateUtil.getDifferenceInMonths(beginDate, endDate);
        log.info("result2 : " + result);
        Assert.assertEquals(2, result);

        beginDate = DateUtil.createDate(31, 1, 2009);
        endDate = DateUtil.createDate(1, 2, 2009);

        result = DateUtil.getDifferenceInMonths(beginDate, endDate);
        log.info("result3 : " + result);
        Assert.assertEquals(1, result);
    }

    @Test
    public void testGetAge() {
        log.info("getAge");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -20);
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        int result = DateUtil.getAge(calendar.getTime());
        log.info("result1 : " + result);
        Assert.assertEquals(20, result);

        calendar.add(Calendar.DATE, 2);

        result = DateUtil.getAge(calendar.getTime());
        log.info("result2 : " + result);
        Assert.assertEquals(19, result);

        calendar.add(Calendar.YEAR, 25);

        result = DateUtil.getAge(calendar.getTime());
        log.info("result3 : " + result);
        Assert.assertEquals(0, result);
    }
    
    @Test
    public void testGetMonthLibelle() {
        log.info("getMonthLibelle");

        Locale.setDefault(Locale.FRENCH);
        String janvier = DateUtil.getMonthLibelle(1);
        Assert.assertEquals("janvier", janvier);

        String juli = DateUtil.getMonthLibelle(7, Locale.GERMAN);
        Assert.assertEquals("Juli", juli);
    }

    /**
     * Shows that {@link DateUtil#truncateToDayOfWeek(Date)} works
     * fine and that using commons library for this operation is not possible.
     */
    @Test
    public void testTruncateToDayOfWeek() {

        Date aDate = DateUtil.createDate(23, 45, 6, 19, 10, 2011);
        Date expectedWeek = DateUtil.createDate(17, 10, 2011);
        Date actualWeek;

        // show that commons-lang do not support this operation with truncate
        try {
            actualWeek = DateUtils.truncate(aDate, Calendar.DAY_OF_WEEK);
            Assert.assertEquals(expectedWeek, actualWeek);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(String.format("The field %d is not supported", Calendar.DAY_OF_WEEK), e.getMessage());
        }

        actualWeek = DateUtil.truncateToDayOfWeek(aDate);
        Assert.assertEquals(expectedWeek, actualWeek);
    }

    @Test
    public void testGetYesterday() {

        //Test normal yesterday
        Date aDate = DateUtil.createDate(3,4,5);
        Date expectedADate = DateUtil.createDate(2,4,5);

        //Test month change
        Date bDate = DateUtil.createDate(1,12,12);
        Date expectedBDate = DateUtil.createDate(30,11,12);

        //Test year change
        Date cDate = DateUtil.createDate(1,1,12);
        Date expectedCDate = DateUtil.createDate(31,12,11);

        //Test possible wrong change
        Date dDate = DateUtil.createDate(3,1,12);
        Date expectedDDate = DateUtil.createDate(2,1,12);


        Assert.assertEquals(expectedADate, DateUtil.getYesterday(aDate));
        Assert.assertEquals(expectedBDate, DateUtil.getYesterday(bDate));
        Assert.assertEquals(expectedCDate, DateUtil.getYesterday(cDate));
        Assert.assertEquals(expectedDDate, DateUtil.getYesterday(dDate));

    }
}
