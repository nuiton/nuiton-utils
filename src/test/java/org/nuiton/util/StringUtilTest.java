/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/*******************************************************************************
 * StringUtilTest.java
 *
 * Created: 7 oct. 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 *
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.util;

import org.junit.Assert;
import org.junit.Test;

import java.awt.Color;
import java.util.Arrays;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class StringUtilTest {
    private static final String[] EMPTY_STRING_ARRAY = new String[0]; // StringUtilTest

    @Test
    public void testSubstring() throws Exception {
        String s = "Bonjour le monde";
        String r = StringUtil.substring(s, -5);
        assertEquals("monde", r);

        r = StringUtil.substring(s, 7, -5);
        assertEquals(" le ", r);

        r = StringUtil.substring(s, 0, s.length());
        assertEquals(s, r);

        r = StringUtil.substring(s, -5, s.length());
        assertEquals("monde", r);

        r = StringUtil.substring("a", 1, -1);
        assertEquals("", r);

        r = StringUtil.substring("", 0, -1);
        assertEquals("", r);

    }
    
    @Test
    public void testToColor() throws Exception {
        Color c;

        c = StringUtil.toColor("#FF55AA");
        assertEquals(c, new Color(255, 85, 170));

        c = StringUtil.toColor("#FF55AA55");
        assertEquals(c, new Color(255, 85, 170, 85));

        try {
            c = null;
            c = StringUtil.toColor("toto");
            assertFalse(true);
        } catch (StringUtilException eee) {
            assertNull(c);
        }

        try {
            c = null;
            c = StringUtil.toColor("#ZRETJ4040R");
            assertFalse(true);
        } catch (StringUtilException eee) {
            assertNull(c);
        }
    }
    
    @Test
    public void testSplit() {
        assertTrue(Arrays.equals(StringUtil.split("'toto',titi,tutu"), new String[]{"'toto'", "titi", "tutu"}));
        assertTrue(Arrays.equals(StringUtil.split("toto"), new String[]{"toto"}));
        assertTrue(Arrays.equals(StringUtil.split(""), EMPTY_STRING_ARRAY));
        assertTrue(Arrays.equals(StringUtil.split(null), EMPTY_STRING_ARRAY));

        String argTest = "toto, titi, titi($tru('roer'), erke), \"t|u(t{u\\\"ti[ti'\", fin";
        String[] arg = StringUtil.split(argTest);
        assertEquals(5, arg.length);

        argTest = "";
        arg = StringUtil.split(argTest);
        assertEquals(0, arg.length);

        argTest = "tptp";
        arg = StringUtil.split(argTest);
        assertEquals(1, arg.length);

        argTest = ",ooo|ooo|o|,ooo'ooo(',ooo-";
        arg = StringUtil.split(argTest, "ooo");
        assertEquals(5, arg.length);
        
        argTest = "1,2,\"3,3\",\"4,4,4\",5";
        arg = StringUtil.split(argTest, ",");
        assertEquals(5, arg.length);

    }

   @Test
    public void testConvert() throws Exception {
        assertEquals("365d", StringUtil.convertTime(31536000000000000L));
        assertEquals("2d", StringUtil.convertTime(172800000000000L));
        assertEquals("2h", StringUtil.convertTime(7200000000000L));
        assertEquals("2m", StringUtil.convertTime(120000000000L));
        
        assertEquals("2s", StringUtil.convertTime(2000000002L));
        assertEquals("2s", StringUtil.convertTime(2000000000L));
        assertEquals("2ms", StringUtil.convertTime(2000000L));
        assertEquals("2ns", StringUtil.convertTime(2L));
        assertEquals("0ns", StringUtil.convertTime(0L));

        assertEquals("0o", StringUtil.convertMemory(0L));
        assertEquals("2o", StringUtil.convertMemory(2L));
        assertEquals("2Ko", StringUtil.convertMemory(2048L));
        assertEquals("2Mo", StringUtil.convertMemory(2097152L));
        assertEquals("2Mo", StringUtil.convertMemory(2097154L));

        assertEquals("2Go", StringUtil.convertMemory(2147483648L));
        assertEquals("2To", StringUtil.convertMemory(2199023255552L));
        assertEquals("2000To", StringUtil.convertMemory(2199023255552000L));

        assertEquals("-2Mo", StringUtil.convertMemory(-2097152L));
        assertEquals("-2Mo", StringUtil.convertMemory(-2097154L));

        Locale oldLocale = Locale.getDefault();
        // test in french locale
        Locale.setDefault(Locale.FRENCH);
        assertEquals("2,02s", StringUtil.convertTime(2020000002L));
        assertEquals("2,094Mo", StringUtil.convertMemory(2196152L));
        assertEquals("-2,094Mo", StringUtil.convertMemory(-2196152L));

        // test in english locale
        Locale.setDefault(Locale.ENGLISH);
        assertEquals("2.02s", StringUtil.convertTime(2020000002L));
        assertEquals("2.094Mo", StringUtil.convertMemory(2196152L));
        assertEquals("-2.094Mo", StringUtil.convertMemory(-2196152L));

        // push back previous locale
        Locale.setDefault(oldLocale);
    }

    @Test
    public void testConvertToConstantName() throws Exception {
        assertEquals("YES", StringUtil.convertToConstantName("yes"));
        assertEquals("YES", StringUtil.convertToConstantName("*$$?YEs"));
        assertEquals("YES", StringUtil.convertToConstantName("_yes!$*_"));
        assertEquals("YES", StringUtil.convertToConstantName("_Yes____"));

        assertEquals("YES_OR_NO", StringUtil.convertToConstantName("__yesOrNo_"));
        assertEquals("YES_OR_NO", StringUtil.convertToConstantName("Yes-or-!*=No"));
        assertEquals("YES_OR_NO", StringUtil.convertToConstantName("_yes__or__no"));
        assertEquals("YES_OR_NO", StringUtil.convertToConstantName("_YesOR___No"));

    }

    @Test
    public void testEncodeMD5() throws Exception {
        assertEquals("9c4fddff843d03cefe44421acf22c6ec", StringUtil.encodeMD5("natcho"));
    }

    /**
     * Test que le hash sha1 d'une chaine est celui attendu.
     */
    @Test
    public void testEncodeSHA1() {
        assertEquals("36fde5ec018f636fdb246986ed2eaa2d64dc6b10", StringUtil.encodeSHA1("Test encodage sha1"));
        assertEquals("639d6aeefd0e3c347e23408b0b47bdd7b928b78f",
                StringUtil.encodeSHA1("Cela ressemblait aux gros ordinateurs que David avait pu voir dans des films de science fiction."));
        assertEquals("53776c1a117089016498c1162db30265baaac558",
                StringUtil.encodeSHA1("Cela ressemblait aux gros ordinateurs que david avait pu voir dans des films de science fiction."));
    }

    @Test
    public void testIsEmail() {
        assertFalse(StringUtil.isEmail("abc.def+ghimonte.st"));
        assertFalse(StringUtil.isEmail("abc.def+ghimonte.st"));
        assertTrue(StringUtil.isEmail("a@monte.st"));
        assertTrue(StringUtil.isEmail("a@monte.st".toUpperCase()));
        assertTrue(StringUtil.isEmail("abc.def+ghi@monte.st"));
        assertTrue(StringUtil.isEmail("abc.def+ghi@monte.st".toUpperCase()));
        assertTrue(StringUtil.isEmail("aBc.def+ghi@monte.st"));
        assertTrue(StringUtil.isEmail("aBC.def+ghi@monte.st"));
        assertTrue(StringUtil.isEmail("ABC.def+ghi@monte.st"));
        assertTrue(StringUtil.isEmail("ABC.Def+ghi@monte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEf+ghi@monte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+ghi@monte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+Ghi@monte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHi@monte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@monte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@Monte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@MOnte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@MONte.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@MONTe.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@MONTE.st"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@MONTE.St"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@MONTE.ST"));
        assertTrue(StringUtil.isEmail("ABC.DEF+GHI@MONTE.STT"));
        assertTrue(StringUtil.isEmail("abC.Def+Ghi@Monte.st"));
        assertTrue(StringUtil.isEmail("abC.Def+Ghi@MOnte.st"));
        assertTrue(StringUtil.isEmail("abC.Def+Ghi@Monte.St"));
        assertTrue(StringUtil.isEmail("abC.Def+Ghi@Monte.SSt"));
        assertTrue(StringUtil.isEmail("abC.Def+Ghi@Monte.SST"));
        assertTrue(StringUtil.isEmail("abC.Def+Ghi@Monte.consulting"));
    }

    /**
     * Test les cas d'échapement des caracteres speciaux csv.
     */
    @Test
    public void testEscapeCsvValue() {
        Assert.assertEquals("allo ?", StringUtil.escapeCsvValue("allo ?", ";"));
        Assert.assertEquals("\"action;réaction\"", StringUtil.escapeCsvValue("action;réaction", ";"));
        Assert.assertEquals("\"cause, effet\"", StringUtil.escapeCsvValue("cause, effet", ";"));
        Assert.assertEquals("\"tee|/dev/null\"", StringUtil.escapeCsvValue("tee|/dev/null", "|"));
        Assert.assertEquals("\"\"\"Amazing\"\" spiderman\"", StringUtil.escapeCsvValue("\"Amazing\" spiderman", ";"));
    }
} // StringUtilTest

