/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/** @author fdesbois */
public class PeriodDatesTest {

    /** Logger */
    private static final Log log = LogFactory.getLog(PeriodDatesTest.class);

    /** Test of initDayOfMonthExtremities method, of class PeriodDates. */
    @Test
    public void testInitDayOfMonthExtremities() {
        PeriodDates period = new PeriodDates();
        period.setFromDate(DateUtil.createDate(9, 8, 7, 23, 12, 2010));
        period.setThruDate(DateUtil.createDate(6, 5, 4, 26, 12, 2010));

        period.initDayOfMonthExtremities();

        Assert.assertTrue(period.between(DateUtil.createDate(3, 2, 1, 1, 12, 2010)));

        Calendar lastDayOf2010 = new GregorianCalendar(2010, 11, 31, 23, 59, 59);
        lastDayOf2010.set(Calendar.MILLISECOND, 999);

        Assert.assertEquals(DateUtil.createDate(0, 0, 0, 1, 12, 2010), period.getFromDate());
        Assert.assertEquals(lastDayOf2010.getTime(), period.getThruDate());
    }

    /** Test of getMonths method, of class PeriodDates. */
    @Test
    public void testGetMonths() {
        // Prepare two calendars with time not equals to 0
        Calendar cal1 = DateUtil.getDefaultCalendar(new Date());
        cal1.set(Calendar.DAY_OF_MONTH, 3);
        cal1.set(Calendar.MONTH, 2);
        cal1.set(Calendar.YEAR, 2009);
        cal1.set(Calendar.HOUR, 8);

        DateFormat timeFormat = DateFormat.getTimeInstance();
        log.info("getMonths:: time for calendarFrom : " +
                         timeFormat.format(cal1.getTime()));

        Calendar cal2 = DateUtil.getDefaultCalendar(new Date());
        cal2.set(Calendar.DAY_OF_MONTH, 22);
        cal2.set(Calendar.MONTH, 6);
        cal2.set(Calendar.YEAR, 2009);
        cal2.set(Calendar.HOUR, 4);

        log.info("getMonths:: time for calendarThru : " +
                         timeFormat.format(cal2.getTime()));

        PeriodDates period = new PeriodDates(cal1, cal2);

        List<Date> months = period.getMonths();
        Assert.assertEquals(5, months.size());
        for (int i = 0; i < 5; i++) {
            Date monthDate = months.get(i);
            int month = DateUtil.getDefaultCalendar(monthDate).get(Calendar.MONTH);
            Assert.assertEquals(i + 2, month); // 2, 3, 4, 5, 6
        }
    }

    @Test(timeout = 60 * 1000, expected = IllegalArgumentException.class)
    public void whatIfPeriodDatesIsNotInChronologicalOrder() {
        Date january = DateUtil.createDate(1, 1, 2011);
        Date december = DateUtil.createDate(1, 12, 2011);

        // trying to break PeriodDates by creating an anti-chronological period
        try {
            PeriodDates periodDates = new PeriodDates(december, january);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            // as expected
        }

        // got ya!
        PeriodDates periodDates = new PeriodDates();
        periodDates.setFromDate(december);
        periodDates.setThruDate(january);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetFromDateAfterThruDate() {
        Date january = DateUtil.createDate(1, 1, 2011);
        Date december = DateUtil.createDate(1, 12, 2011);

        PeriodDates periodDates = new PeriodDates();
        // Set thruDate before setting fromDate
        periodDates.setThruDate(january);
        // FromDate is after thruDate : should throw IllegalArgumentException
        periodDates.setFromDate(december);
    }


}
