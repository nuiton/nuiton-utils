package org.nuiton.util;

/*-
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Year;
import java.util.EnumSet;
import java.util.Set;

public class WorkdayUtilTest {

    @Test
    public void testEasterAscensionDayWit() {
        LocalDate easter2006 = WorkdayUtil.computeEaster(Year.of(2006));
        Assert.assertEquals(LocalDate.of(2006, 4, 16), easter2006);

        LocalDate easter2018 = WorkdayUtil.computeEaster(Year.of(2018));
        Assert.assertEquals(LocalDate.of(2018, 4, 2), WorkdayUtil.computeEasterMonday(easter2018));
        Assert.assertEquals(LocalDate.of(2018, 5, 10), WorkdayUtil.computeAscensionDay(easter2018));
        Assert.assertEquals(LocalDate.of(2018, 5, 21), WorkdayUtil.computeWhitMonday(easter2018));

        LocalDate easter2019 = WorkdayUtil.computeEaster(Year.of(2019));
        Assert.assertEquals(LocalDate.of(2019, 4, 22), WorkdayUtil.computeEasterMonday(easter2019));
        Assert.assertEquals(LocalDate.of(2019, 5, 30), WorkdayUtil.computeAscensionDay(easter2019));
        Assert.assertEquals(LocalDate.of(2019, 6, 10), WorkdayUtil.computeWhitMonday(easter2019));

        LocalDate easter2020 = WorkdayUtil.computeEaster(Year.of(2020));
        Assert.assertEquals(LocalDate.of(2020, 4, 13), WorkdayUtil.computeEasterMonday(easter2020));
        Assert.assertEquals(LocalDate.of(2020, 5, 21), WorkdayUtil.computeAscensionDay(easter2020));
        Assert.assertEquals(LocalDate.of(2020, 6, 1), WorkdayUtil.computeWhitMonday(easter2020));
    }

    @Test
    public void testWorkDays() {
        Set<DayOfWeek> weekWorkday = EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY);
        WorkdayUtil.FrenchPublicHoliday publicHoliday = new WorkdayUtil.FrenchPublicHoliday();

        {
            LocalDate start = LocalDate.of(2017, 12, 22);
            LocalDate end = LocalDate.of(2017, 12, 22);

            Assert.assertEquals(0, WorkdayUtil.computeWorday(start, end, weekWorkday, publicHoliday));
        }
        {
            LocalDate start = LocalDate.of(2017, 12, 22);
            LocalDate end = LocalDate.of(2017, 12, 25);

            Assert.assertEquals(0, WorkdayUtil.computeWorday(start, end, weekWorkday, publicHoliday));
        }
        {
            LocalDate start = LocalDate.of(2017, 12, 22);
            LocalDate end = LocalDate.of(2017, 12, 26);

            Assert.assertEquals(1, WorkdayUtil.computeWorday(start, end, weekWorkday, publicHoliday));
        }
        {
            LocalDate start = LocalDate.of(2017, 12, 22);
            LocalDate end = LocalDate.of(2018, 1, 6);

            Assert.assertEquals(9, WorkdayUtil.computeWorday(start, end, weekWorkday, publicHoliday));
        }
        {
            LocalDate start = LocalDate.of(2017, 12, 25);
            LocalDate end = LocalDate.of(2017, 12, 26);

            Assert.assertEquals(0, WorkdayUtil.computeWorday(start, end, weekWorkday, publicHoliday));
        }
        {
            LocalDate start = LocalDate.of(2017, 12, 25);
            LocalDate end = LocalDate.of(2018, 1, 26);

            Assert.assertEquals(22, WorkdayUtil.computeWorday(start, end, weekWorkday, publicHoliday));
        }
    }
}
