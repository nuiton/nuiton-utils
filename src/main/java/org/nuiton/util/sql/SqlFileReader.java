package org.nuiton.util.sql;

/*-
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Create an iterable on a SQL text content. The content is iterated on each SQL
 * statement. For information the class handles semi-colon in quote.
 * 
 * File example:
 * INSERT INTO client (prenom, age) VALUES ('John', 11);
 * INSERT INTO client (prenom, age) VALUES ('Jack', 12);
 * 
 * Then:
 * SqlFileReader reader = new SqlFileReader(stream);
 * for (String sql : reader) {
 *     // process sql variable
 * }
 * 
 * @author jruchaud
 */
public class SqlFileReader implements Iterable<String> {
    
    protected Reader source;

    public SqlFileReader(String source) {
        this.source = new StringReader(source);
    }

    public SqlFileReader(Reader source) {
        this.source = source;
    }

    @Override
    public Iterator<String> iterator() {
        return new SqlFileReaderIterator(source);
    }
    
    enum SqlFileParserState {NORMAL, QUOTE, COMMENT};
        
    /**
     * Use to create an iterator on the iterable.
     */
    public static class SqlFileReaderIterator implements Iterator<String> {

        protected Reader source;
        
        protected StringBuilder buffer;
    
        /** The variable is used to keep if the iterator reach the end */
        protected int scanner;
        
        public SqlFileReaderIterator(Reader source) {
            this.source = source;
            this.buffer = new StringBuilder();
        }

        @Override
        public boolean hasNext() {
            return this.scanner != - 1;
        }

        @Override
        public String next() {
            if (this.scanner == -1) {
                throw new NoSuchElementException();
            }
            
            SqlFileParserState state = SqlFileParserState.NORMAL;
            this.buffer.setLength(0);
                    
            try {
                
                while ((this.scanner = this.source.read()) != -1) {
                    char character = (char) this.scanner;
                    
                    switch (state) {
                        case NORMAL:
                            switch (character) {
                                // Remove useless character
                                case '\n':
                                case '\r':
                                    break;

                                // Search the end of query
                                case ';':
                                    this.buffer.append(";");
                                    return this.buffer.toString();

                                // Enter comment state if you have --
                                case '-':
                                    int length = this.buffer.length();
                                    if (length != 0 && this.buffer.charAt(length - 1) == '-') {
                                        state = SqlFileParserState.COMMENT;
                                    }
                                    this.buffer.append(character);
                                    break;

                                // Enter quote state
                                case '\'':
                                    state = SqlFileParserState.QUOTE;
                                    this.buffer.append(character);
                                    break;

                                // By default append character
                                default:
                                    this.buffer.append(character);
                                    break;
                            }
                            break;
                            
                        case QUOTE:
                            // Remove useless character
                            switch (character) {
                                // Search the end of quote
                                case '\'':
                                    state = SqlFileParserState.NORMAL;
                                    this.buffer.append(character);
                                    break;

                                // By default append character
                                default:
                                    this.buffer.append(character);
                                    break;

                            }
                            break;
                            
                        case COMMENT:
                            switch (character) {
                                // Search the end of comment
                                case '\n':
                                case '\r':
                                    return this.buffer.toString();

                                // By default append character
                                default:
                                    this.buffer.append(character);
                                    break;

                            }
                            break;
                    }
                }
                
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            
            return this.buffer.toString();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }

    }
}
