/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/**
 * ArrayUtil.java
 * <p>
 * Created: 31 oct. 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */

package org.nuiton.util;

import java.lang.reflect.Array;
import java.util.Collection;

public class ArrayUtil { // ArrayUtil

    public static int[] asIntArray(String[] a) {
        int[] result = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = StringUtil.toInt(a[i]);
        }
        return result;
    }


    /**
     * Fait la somme des 2 tableaux et retourne un nouveau tableau, les
     * 2 tableaux passés en argument ne sont pas modifiés. Les deux tableaux
     * doivent être non null et avoir la même taille.
     *
     * @param a le premier tableau
     * @param b le second tableau
     * @return le tableau des sommes
     */
    public static int[] sum(int[] a, int[] b) {
        if (a == null || b == null || a.length != b.length) {
            throw new IllegalArgumentException("Au moins des tableaux est null ou les tableaux ne font pas la même taille");
        }
        int[] result = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = a[i] + b[i];
        }
        return result;
    }

    public static int[] concat(int[]... tabs) {
        int length = 0;
        for (int[] tab : tabs) {
            if (tab != null) {
                length += tab.length;
            }
        }
        int[] result = new int[length];
        length = 0;
        for (int[] tab : tabs) {
            if (tab != null) {
                System.arraycopy(tab, 0, result, length, tab.length);
                length += tab.length;
            }
        }
        return result;
    }

    /**
     * Retourne un nouveau tableau qui est la concatenation des deux autres.
     * Essai de garder pour le tableau resultat le type des tableaux en entré
     * si possible. [Double], [Number] → [Number]; [Double], [Long] → [Object]
     *
     * @param tabs les tableaux
     * @return le nouveau tableau ou null, si les deux tableaux sont null
     * todo essayer de retourner le meilleur type de tableau possible
     * [Double], [Long] → [Number]
     */
    public static Object[] concat(Object[]... tabs) {
        Object[] result = null;
        Class<?> clazz = null;
        int length = 0;
        for (Object[] tab : tabs) {
            if (tab != null) {
                length += tab.length;
                Class<?> tmp = tab.getClass().getComponentType();
                if (clazz == null) {
                    clazz = tmp;
                } else if (tmp.isAssignableFrom(clazz)) {
                    clazz = tmp;
                } else if (clazz.isAssignableFrom(tmp)) {
                    // do nothing, because clazz can't be better
                } else {
                    clazz = Object.class;
                }
            }
        }

        if (clazz != null) {
            result = (Object[]) Array.newInstance(clazz, length);
            length = 0;
            for (Object[] tab : tabs) {
                if (tab != null) {
                    System.arraycopy(tab, 0, result, length, tab.length);
                    length += tab.length;
                }
            }
        }
        return result;
    }

    /**
     * Ajoute a un tableau un ensemble d'element. Le type du tableau retourné
     * est le meilleur possible.
     *
     * @param <E>   FIXME
     * @param <F>   FIXME
     * @param tab   les valeurs initiales du tableau
     * @param elems les elemements a ajouter
     * @return un nouveau tableau contenant a la fin les elements souhaites
     */
    @SuppressWarnings("unchecked")
    public static <E, F extends E> E[] concatElems(E[] tab, F... elems) {
        E[] result;
        result = (E[]) concat(tab, elems);
        return result;
    }

    /**
     * Recherche dans le table le 1er element qui correspond a la classe
     * passée en argument.
     *
     * @param <A> FIXME
     * @param tab   le tableau dans lequel il faut chercher
     * @param clazz la classe de l'objet souhaité
     * @return un objet de la classe demandé, ou null si aucun ne correspond
     */
    public static <A> A search(Object[] tab, Class<A> clazz) {
        A result = null;
        for (Object o : tab) {
            if (clazz.isInstance(o)) {
                result = clazz.cast(o);
            }
        }
        return result;
    }

    @SuppressWarnings({"unchecked"})
    public static <T> T[] toArray(Collection list, Class<T> clazz) {
        T[] result = (T[]) Array.newInstance(clazz, list == null ? 0 : list.size());
        int i = 0;
        for (Object o : list) {
            result[i++] = (T) o;
        }
        return result;
    }
} // ArrayUtil

