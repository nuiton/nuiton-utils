/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * TransparenteWeakReference.java
 *
 * Created: 10 mai 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * Copyright Code Lutin
 *
 *
 * Mise a jour: $Date$
 * par : */
package org.nuiton.util;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 * Cette classe etant WeakReference et surcharge les méthodes equals et
 * hashCode pour que ces méthodes retournes les mêmes résultat que les objets
 * contenu.
 *
 * @param <T> type of object
 */
public class TransparenteWeakReference<T> extends WeakReference<T> {

    protected int hash;

    protected String toString;

    protected boolean objectToStringUsed = true;

    public TransparenteWeakReference(T o) {
        this(o, true);
    }

    public TransparenteWeakReference(T o, ReferenceQueue<? super T> queue) {
        this(o, queue, true);
    }

    /**
     * @param o                  TODO ?
     * @param objectToStringUsed if true, this ref used toString method of
     *                           encapsulated object otherwize used default Object toString
     */
    public TransparenteWeakReference(T o, boolean objectToStringUsed) {
        super(o);
        init(o, objectToStringUsed);
    }

    public TransparenteWeakReference(T o,
                                     ReferenceQueue<? super T> queue,
                                     boolean objectToStringUsed) {
        super(o, queue);
        init(o, objectToStringUsed);
    }

    /**
     * On conserve le hash pour que la Reference puisse encore se faire
     * passer pour l'objet alors que celui-ci a disparu de la memoire
     *
     * @param o                  TODO ?
     * @param objectToStringUsed TODO ?
     */
    protected void init(T o, boolean objectToStringUsed) {
        if (o == null) {
            hash = 0;
        } else {
            hash = o.hashCode();
            if (objectToStringUsed) {
                toString = o.toString();
            }
            if (toString == null) {
                toString = o.getClass().getName() + '@' +
                           Integer.toHexString(hash);
            }
            if (toString.length() > 100) {
                toString = toString.substring(0, 100) + "...";
            }
        }
    }

    /**
     * @param o l'objet a comparer
     * @return {@code true} si meme reference memoire on les objets
     *         references sont egaux
     */
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        // on travail avec un variable local pour ne pas etre obligé de
        // synchroniser la méthode
        Object local = get();
        Object other = o;
        if (o instanceof Reference<?>) {
            other = ((Reference<?>) o).get();

            if (other == null) {
                // on fait l'egalite sur les hash car on a perdu les objets
                return o.hashCode() == hashCode();
            }
        }

        return other == null && local == null ||
               other != null && other.equals(local);
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public String toString() {
        return toString;
    }
}
