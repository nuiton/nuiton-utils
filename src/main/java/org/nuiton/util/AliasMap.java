package org.nuiton.util;

/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

/**
 * Une map base sur une HashMap qui permet pour une valeur d'ajouter d'autres
 * cles. Cette nouvelle cle est un alias, un alias peut-etre utilise pour
 * plusieurs valeurs. Si l'on demande la valeur associe a un Alias, cela
 * retourne une liste contenant toutes les valeurs pour lequel cette Alias est
 * utilise.
 * 
 * Cela permet de stocker des objets avec une cle principale et unique, puis
 * avec des alias.
 *
 * Et ainsi recherche les valeurs qui ont un ensemble d'alias via la methode
 * {@link #getValueAlias(Object[]) } ou supprimer les valeurs qui
 * ont un ensemble d'Alias en commun via la methode {@link #removeValue}
 *
 * Si la cle ne vous importe que peu, vous pouvez par exemple utiliser
 * {@link UUID#randomUUID()} pour generer une cle unique.
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @since 3.6.9
 */
public class AliasMap<K, V, A> extends HashMap<K, V> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AliasMap.class);
    private static final long serialVersionUID = 1L;

    /** key: alias, value: key */
    protected MultiValuedMap<A, K> aliases;
    /** key: key, value: alias */
    protected MultiValuedMap<K, A> keys;

    public AliasMap() {
        aliases = new HashSetValuedHashMap<>();
        keys = new HashSetValuedHashMap<>();
    }

    /**
     * Ajoute une valeur dans la map avec un ensemble d'alias associe
     *
     * @param key identifiant unique pour cette valeur
     * @param value la valeur
     * @param alias1 le premier alias à saisir
     * @param alias les alias de la valeur
     * @return FIXME
     */
    public V put(K key, V value, A alias1, A ... alias) {
        V result = put(key, value);
        putAlias(key, alias1, alias);
        
        return result;
    }

    /**
     * Ajoute des alias a une cle
     * @param key FIXME
     * @param alias1 FIXME
     * @param alias FIXME
     */
    protected void putAlias(K key, A alias1, A ... alias) {
        aliases.put(alias1, key);
        keys.put(key, alias1);
        for (A a : alias) {
            aliases.put(a, key);
            keys.put(key, a);
        }
    }

    /**
     * Retoure les cles en commun de tous les alias. Les cles retournees sont
     * celle qui ont tous les alias.
     * <pre>
     * K1: a, b, c
     * K2: b, c, d
     * K3: c, d, e
     *
     * getKeyAlias(a, b, c) retourne [K1]
     * getKeyAlias(b, c) retourne [K1, K2]
     * getKeyAlias(c) retourne [K1, K2, K3]
     * getKeyAlias(d) retourne [K2, K3]
     * getKeyAlias(z) retourne []
     * </pre>
     *
     *
     * @param alias FIXME
     * @return une liste vide si aucune valeur ne correspond au alias en argument
     */
    public Collection<K> getKeyAlias(A ... alias) {
        Collection result = null;
        for (A a : alias) {
            Collection tmp = aliases.get(a);
            if (tmp != null) {
                if (result == null) {
                    result = new HashSet(tmp);
                } else {
                    result.retainAll(tmp);
                }
            }
        }
        if (result == null) {
            result = Collections.emptySet();
        }
        return result;
    }

    /**
     * Retoure les valeurs en commun de tous les alias. Les valeurs retournees sont
     * celle qui ont tous les alias.
     * <pre>
     * V1: a, b, c
     * V2: b, c, d
     * V3: c, d, e
     *
     * getKeyAlias(a, b, c) retourne [V1]
     * getKeyAlias(b, c) retourne [V1, V2]
     * getKeyAlias(c) retourne [V1, V2, V3]
     * getKeyAlias(d) retourne [V2, V3]
     * getKeyAlias(z) retourne []
     * </pre>
     *
     *
     * @param alias FIXME
     * @return une liste vide si aucune valeur ne correspond au alias en argument
     */
    public Collection<V> getValueAlias(A ... alias) {
        Collection keys = getKeyAlias(alias);
        Collection result = new HashSet(keys.size());
        for (Object k : keys) {
            result.add(get(k));
        }
        return result;
    }

    /**
     * Retourne la liste d'alias associee avec une cle
     *
     * <pre>
     * K1: a, b, c
     * K2: b, c, d
     * K3: c, d, e
     *
     * getAlias(K1) retourne [a, b, c]
     * getAlias(k3) retourne [c, d, e]
     * getKeyAlias(k9) retourne []
     * </pre>
     *
     * @param key FIXME
     * @return FIXME
     */
    public Collection<A> getAlias(K key) {
        Collection<A> result = keys.get(key);
        if (result == null) {
            result = Collections.emptySet();
        }
        return result;
    }

    /**
     * Retire une cle ainsi que tous ses alias
     *
     * <pre>
     * K1: a, b, c
     * K2: b, c, d
     * K3: c, d, e
     *
     * remove(K1) il reste K2: [b, c, d], K3: [c, d, e]
     * </pre>
     *
     * @param key FIXME
     * @return FIXME
     */
    @Override
    public V remove(Object key) {
        V result = super.remove(key);
        Collection<A> alias = getAlias((K)key);
        if (alias != null) {
            for (A a : alias) {
                aliases.removeMapping(a, key);
                if (CollectionUtils.isEmpty(aliases.get(a))) {
                    aliases.remove(a);
                }
            }
        }
        keys.remove(key);
        return result;
    }

    /**
     * Supprime toutes les valeurs et leur cle associe aux alias
     *
     * <pre>
     * K1: a, b, c
     * K2: b, c, d
     * K3: c, d, e
     *
     * removeValue(b, c) il reste K3: [c, d, e]
     * </pre>
     *
     * @param alias FIXME
     * @return la liste de valeur qui a ete supprime de la map
     */
    public Collection<V> removeValue(A ... alias) {
        Collection keys = getKeyAlias(alias);
        Collection result = new ArrayList(keys.size());
        for (Object k : keys) {
            result.add(remove(k));
        }
        return result;
    }

    /**
     * Supprime des alias quelque soit leur cle
     *
     * <pre>
     * K1: a, b, c
     * K2: b, c, d
     * K3: c, d, e
     *
     * removeAlias(a, b) alors K1: [c], k2: [c, d], k3: [c, d, e]
     * removeAlias(c) alors K1: [a, b], k2: [b, d], k3: [d, e]
     * getKeyAlias(z) alors rien ne change car cette alias n'existe pas
     * </pre>
     *
     * @param alias FIXME
     */
    public void removeAlias(A ... alias) {
        for (A a : alias) {
            Collection ks = aliases.get(a);
            aliases.remove(a);
            if (ks != null) {
                for (Object k : ks) {
                    keys.removeMapping(k, a);
                }
            }
        }
    }
}
