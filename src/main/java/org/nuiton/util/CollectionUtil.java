/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;


/**
 * Created: 23 févr. 2006 09:03:39
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */

public class CollectionUtil {

    /**
     * Ajoute a la collection tous les elements passés en parametre
     *
     * @param <A> FIXME
     * @param <E> FIXME
     * @param col la collection
     * @param e   les elements a ajouter
     * @return la collection passé en parametre
     */
    public static <A, E extends Collection<A>> E addAll(E col, A... e) {
        Collections.addAll(col, e);
        return col;
    }

    /**
     * Ajoute a la liste tous les elements passés en parametre
     *
     * @param <A> FIXME
     * @param <E> FIXME
     * @param col la liste
     * @param pos le premier index où insérer les données
     * @param e   les elements a ajouter
     * @return la liste passé en parametre
     */
    public static <A, E extends List<A>> E addAll(E col, int pos, A... e) {
        col.addAll(pos, Arrays.asList(e));
        return col;
    }

    /**
     * Permet de convertir une liste non typée, en une liste typée.
     * <p>
     * La liste en entrée en juste bien castée.
     * <p>
     * On effectue une vérification sur le typage des élements de la liste.
     * <p>
     * Note : <b>Aucune liste n'est créee, ni recopiée</b>
     *
     * @param <O>  data type
     * @param list la liste à convertir
     * @param type le type des éléments de la liste
     * @return la liste typée
     * @throws IllegalArgumentException si un élément de la liste en entrée n'est
     *                                  pas en adéquation avec le type voulue.
     */
    @SuppressWarnings({"unchecked"})
    public static <O> List<O> toGenericList(
            List<?> list, Class<O> type) throws IllegalArgumentException {
        if (list.isEmpty()) {
            return (List<O>) list;
        }
        for (Object o : list) {
            if (!type.isAssignableFrom(o.getClass())) {
                throw new IllegalArgumentException(
                        "can not cast List with object of type " +
                                o.getClass() + " to " + type + " type!");
            }
        }
        return (List<O>) list;
    }

    /**
     * Permet de convertir une collection non typée, en une collection typée.
     * <p>
     * La collection en entrée en juste bien castée.
     * <p>
     * On effectue une vérification sur le typage des élements de la collection.
     * <p>
     * Note : <b>Aucune collection n'est créee, ni recopiée</b>
     *
     * @param <O>  data type
     * @param list la collection à convertir
     * @param type le type des éléments de la collection
     * @return la collection typée
     * @throws IllegalArgumentException si un élément de la collection en entrée n'est
     *                                  pas en adéquation avec le type voulue.
     */
    @SuppressWarnings({"unchecked"})
    public static <O> Collection<O> toGenericCollection(
            Collection<?> list, Class<O> type) throws IllegalArgumentException {
        if (list.isEmpty()) {
            return (Collection<O>) list;
        }
        for (Object o : list) {
            if (!type.isAssignableFrom(o.getClass())) {
                throw new IllegalArgumentException(
                        "can not cast Collection with object of type " +
                                o.getClass() + " to " + type + " type!");
            }
        }
        return (Collection<O>) list;
    }

    /**
     * Permet de convertir un ensemble non typée, en un ensemble typée.
     * <p>
     * L'ensemble  en entrée en juste bien castée.
     * <p>
     * On effectue une vérification sur le typage des élements de la collection.
     * <p>
     * Note : <b>Aucun ensemble n'est créee, ni recopiée</b>
     *
     * @param <O>  data type
     * @param list l'ensemble  à convertir
     * @param type le type des éléments de l'ensemble
     * @return l'ensemble typée
     * @throws IllegalArgumentException si un élément de l'ensemble en entrée n'est
     *                                  pas en adéquation avec le type voulue.
     */
    @SuppressWarnings({"unchecked"})
    public static <O> Set<O> toGenericSet(
            Set<?> list, Class<O> type) throws IllegalArgumentException {
        if (list.isEmpty()) {
            return (Set<O>) list;
        }
        for (Object o : list) {
            if (!type.isAssignableFrom(o.getClass())) {
                throw new IllegalArgumentException(
                        "can not cast Set with object of type " +
                                o.getClass() + " to " + type + " type!");
            }
        }
        return (Set<O>) list;
    }

    /**
     * Get data at given {@code index} from the given collection or
     * {@code null} if no element at given position.
     *
     * @param collection the collection to scan
     * @param index      index to seek
     * @param <T>        type of data in collection
     * @return the data found at given index, or {@code null} if not found
     * @since 2.6.4
     */
    public static <T> T getOrNull(Collection<T> collection, int index) {
        T result = null;
        if (collection != null) {
            int i = 0;
            for (T t : collection) {
                if (index == i) {
                    result = t;
                    break;
                }
                i++;
            }
        }
        return result;
    }

    /**
     * Get data at given {@code index} from the given collection.
     *
     * @param collection the collection to scan
     * @param index      index to seek
     * @param <T>        type of data in collection
     * @return the data found at given index
     * @throws IndexOutOfBoundsException if there is such index in collection
     * @since 2.6.4
     */
    public static <T> T get(Collection<T> collection, int index) throws IndexOutOfBoundsException {
        T result = null;
        if (collection != null) {
            int i = 0;
            for (T t : collection) {
                if (index == i) {
                    result = t;
                    break;
                }
                i++;
            }
            if (i != index) {
                throw new IndexOutOfBoundsException("No element at index " + index);
            }
        }
        return result;
    }

    /**
     * Get data at given {@code index} from the given list.
     *
     * @param list  the list to scan
     * @param index index to seek
     * @param <T>   type of data in collection
     * @return the data found at given index
     * @throws IndexOutOfBoundsException if there is such index in list
     * @since 2.6.4
     */
    public static <T> T get(List<T> list, int index) throws IndexOutOfBoundsException {
        T result = null;
        if (list != null) {
            if (index >= list.size()) {
                throw new IndexOutOfBoundsException("No element at index " + index);
            }
            result = list.get(index);
        }
        return result;
    }

    /**
     * Get data at given {@code index} from the given list or {@code null} if
     * no such index in list.
     *
     * @param list  the list to scan
     * @param index index to seek
     * @param <T>   type of data in collection
     * @return the data found at given index or {@code null} if no such index in list
     * @since 2.6.4
     */
    public static <T> T getOrNull(List<T> list, int index) {
        T result = null;
        if (list != null) {
            if (index < list.size()) {
                result = list.get(index);
            }
        }
        return result;
    }
}


