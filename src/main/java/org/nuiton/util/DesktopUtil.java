/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * Utility class for methods to interact with Desktop Environment
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.4.3
 */
public class DesktopUtil {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DesktopUtil.class);

    /**
     * Method to open an URI in the user default web browser. It uses the Java
     * Desktop API on Windows and Gnome environment and xdg-open on other
     * platforms (non-gnome linux distribution for example).
     *
     * A Bug report have been opened in 2006 so that java.awt.Desktop can
     * support all environments but it is not fixed yet :
     * http://bugs.sun.com/view_bug.do?bug_id=6486393 this utility method should
     * be removed when the bug is fixed.
     *
     * @param uri the URI to open
     */
    public static void browse(URI uri) {
        boolean useXdg = true;
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                if (desktop.isSupported(Desktop.Action.BROWSE)) {
                    useXdg = false;
                    desktop.browse(uri);
                }
            }
        } catch (IOException ioe) {
            // ignore
        }
        
        if (useXdg) {
            if (log.isDebugEnabled()) {
                log.debug("Desktop API not supported, launching xdg-open");
            }
            ProcessBuilder pb = new ProcessBuilder("xdg-open", uri.toString());
            try {
                pb.start();
            } catch (IOException e) {
                if (log.isDebugEnabled()) {
                    log.debug("Could not launch browser, there is maybe no " +
                              "default browser configured on the system", e);
                }
            }
        }
    }

    /**
     * Method to open an URI in the user default open client. It uses the Java
     * Desktop API on Windows and Gnome environment and xdg-open on other
     * platforms (non-gnome linux distribution for example).
     *
     * A Bug report have been opened in 2006 so that java.awt.Desktop can
     * support all environments but it is not fixed yet :
     * http://bugs.sun.com/view_bug.do?bug_id=6486393 this utility method should
     * be removed when the bug is fixed.
     *
     * @param file the file to open
     */
    public static void open(File file) {
        boolean useXdg = true;
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                if (desktop.isSupported(Desktop.Action.OPEN)) {
                    useXdg = false;
                    desktop.open(file);
                }
            }
        } catch (IOException ioe) {
        
        }
        
        if (useXdg) {
            if (log.isDebugEnabled()) {
                log.debug("Desktop API not supported, launching xdg-open");
            }

            ProcessBuilder pb = new ProcessBuilder("xdg-open", file.toURI().toString());
            try {
                pb.start();
            } catch (IOException e) {
                if (log.isDebugEnabled()) {
                    log.debug("Could not open file, there is maybe no " +
                              "default viewer configured on the system", e);
                }
            }

        }

    }
    
    /**
     * Method to open an URI in the user default mail client. It uses the Java
     * Desktop API on Windows and Gnome environment and xdg-email on other
     * platforms (non-gnome linux distribution for example).
     *
     * A Bug report have been opened in 2006 so that java.awt.Desktop can
     * support all environments but it is not fixed yet :
     * http://bugs.sun.com/view_bug.do?bug_id=6486393 this utility method should
     * be removed when the bug is fixed.
     *
     * @param uri the uri to open
     */
    public static void mail(URI uri) {
        boolean useXdg = true;
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                if (desktop.isSupported(Desktop.Action.MAIL)) {
                    useXdg = false;
                    desktop.mail(uri);
                }
            }
        } catch (IOException ioe) {
        
        }
        
        if (useXdg) {
            if (log.isDebugEnabled()) {
                log.debug("Desktop API not supported, launching xdg-open");
            }

            ProcessBuilder pb = new ProcessBuilder("xdg-email", uri.toString());
            try {
                pb.start();
            } catch (IOException e) {
                if (log.isDebugEnabled()) {
                    log.debug("Could not open file, there is maybe no " +
                              "default viewer configured on the system", e);
                }
            }

        }

    }
}
