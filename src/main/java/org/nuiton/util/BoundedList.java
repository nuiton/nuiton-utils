/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * ArrayList with minimum and maximum sizes. For each operation, the size is
 * checked and the BoundedList to ensure that it is kept in the range.
 *
 * Created: 10 juin 2005
 *
 * @author Arnaud Thimel - thimel@codelutin.com
 *
 */
public class BoundedList<E> extends ArrayList<E> {
    /**  */
    private static final long serialVersionUID = -3211387041114409849L;

    //By defaut, maxSize is set to -1 (infinite)
    private int minSize = 0;

    private int maxSize = -1;

    /**
     * Indicates if the list size is at the maximum
     *
     * @return true is the maximum size has been reached
     */
    public boolean isFull() {
        return (maxSize == -1 || size() >= maxSize);
    }

    /**
     * Indicates if the list size is to the minimum
     *
     * @return true is the list size is the minimum size
     */
    public boolean isToMinimum() {
        return (size() <= minSize);
    }

    /**
     * Creates a new BoundedList with the specified initialCapacity. The min size is set to 0 and the max size to the infinite.
     *
     * @param initialCapacity FIXME
     */
    public BoundedList(int initialCapacity) {
        super(initialCapacity);
    }

    /** Creates a new BoundedList with the default capacity. The min size is set to 0 and the max size to the infinite. */
    public BoundedList() {
        super();
    }

    /**
     * Creates a new BoundedList with the specified collection of elements. The min size is set to 0 and the max size to the infinite.
     *
     * @param arg0 FIXME
     */
    public BoundedList(Collection<E> arg0) {
        super(arg0);
    }

    /**
     * Creates a new empty BoundedList with the specified min and max sizes. Please be informed that -1 represents the infinite.
     *
     * @param minSize FIXME
     * @param maxSize FIXME
     */
    public BoundedList(int minSize, int maxSize) {
        super();
        if (minSize > 0)
            this.minSize = minSize;
        if (minSize > -1)
            this.maxSize = maxSize;
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param o the Object to be added
     */
    @Override
    public boolean add(E o) {
        if (checkNewSize(1))
            return super.add(o);
        else
            throw getException("Cannot add element. Use set to replace element or remove to free space or addAll");
    }

    /**
     * Inserts the specified element at the specified position in this list.
     *
     * @param index the position where to add the Object
     * @param o     the Object to be added
     */
    @Override
    public void add(int index, E o) {
        if (checkNewSize(1))
            super.add(index, o);
        else
            throw getException("Cannot add element. Use set to replace element or remove to free space or addAll");
    }

    /**
     * Appends all of the elements in the specified Collection to the end of this list, in the order that they are returned by the specified Collection's Iterator.
     *
     * @param c the collection of Objects to be added
     */
    @Override
    public boolean addAll(Collection<? extends E> c) {
        if (checkNewSize(c.size()))
            return super.addAll(c);
        else
            throw getException("Cannot add element. Use set to replace element or remove to free space or addAll");
    }

    /**
     * Inserts all of the elements in the specified Collection into this list, starting at the specified position.
     *
     * @param index the index where to start adding the collection of objects
     * @param c     the collection of Objects to be added
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if (checkNewSize(c.size()))
            return super.addAll(index, c);
        else
            throw getException("Cannot add element. Use set to replace element or remove to free space or addAll");
    }

    /** Returns a shallow copy of this BoundedList instance */
    @SuppressWarnings("unchecked")
    @Override
    public Object clone() {
        BoundedList<E> bL = (BoundedList<E>) super.clone();
        bL.minSize = minSize;
        bL.maxSize = maxSize;
        return bL;
    }

    /*
    public Object set(int index, Object o) {
       //Pas d'action particulière à priori
       return super.set(index, o);
    }
    */

    /**
     * Removes the element at the specified position in this list.
     *
     * @param index the position of the element to be removed
     */
    @Override
    public E remove(int index) {
        if (checkNewSize(-1))
            return super.remove(index);
        else
            throw getException("Cannot remove element, minSize is " + minSize);
    }

    /**
     * Removes from this List all of the elements whose index is between fromIndex, inclusive and toIndex, exclusive.
     *
     * @param fromIndex index of first element to be removed
     * @param toIndex   index after last element to be removed
     */
    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        if (checkNewSize(-(toIndex - fromIndex)))
            super.removeRange(fromIndex, toIndex);
        else
            throw getException("Cannot remove element, minSize is " + minSize);
    }

    /** Removes a single instance of the specified element from this list, if it is present (optional operation). */
    @Override
    public boolean remove(Object o) {
        if (checkNewSize(-1) || !contains(o))
            return super.remove(o);
        else
            throw getException("Cannot remove element, minSize is " + minSize);
    }

    /** Removes from this collection all of its elements that are contained in the specified collection (optional operation). */
    @Override
    public boolean removeAll(Collection<?> c) {
        //On ne sait pas si les éléments de la Collection sont contenus dans la liste.
        //De plus, on ne sait pas si les instances sont présentes un ou plusieurs fois (Si elles y sont +sieurs fois elles sont supprimées +sieurs fois)
        if (checkNewSize(-c.size()))
            return super.removeAll(c);
        else
            throw getException("Cannot remove element, minSize is " + minSize);
    }

    private boolean checkNewSize(int size) {
        if (size == 0) //Pas de changement de taille !
            return (((size()) >= minSize) && ((maxSize == -1) || (size() <= maxSize)));
        else if (size > 0) //Ajout d'un elem - vérification qu'on ne dépasse pas la borne max
            return ((maxSize == -1) || ((size() + size) <= maxSize));
        return ((size() + size) >= minSize); //Suppression d'un élem - on vérifie qu'on ne descend pas en dessous de la borne min
    }

    private RuntimeException getException(String msg) {
        return new BoundedListOutOfBoundsException(msg);
    }

    @Override
    public String toString() {
        return /*"(" + minSize + ", " + maxSize + ") " +*/ super.toString();
    }
}
