/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* TransparenteSoftReference.java
*
* Created: 10 mai 2004
*
* @author Benjamin Poussin - poussin@codelutin.com
* Copyright Code Lutin
*
*
* Mise a jour: $Date$
* par : $Author$
*/
package org.nuiton.util;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;

/**
 * Cette classe etant SoftReference et surcharge les méthodes equals et
 * hashCode pour que ces méthodes retournes les mêmes résultat que les objets
 * contenu.
 */
public class TransparenteSoftReference<T> extends SoftReference<T> {

    protected int hash;

    protected String toString;

    /**
     * DOCUMENTME Constructor for the TransparenteSoftReference object
     *
     * @param o DOCUMENTME Description of the Parameter
     */
    public TransparenteSoftReference(T o) {
        this(o, true);
    }

    public TransparenteSoftReference(T o, ReferenceQueue<? super T> queue) {
        this(o, queue, true);
    }

    public TransparenteSoftReference(T o, boolean objectToStringUsed) {
        super(o);
        init(o, objectToStringUsed);
    }

    public TransparenteSoftReference(T o,
                                     ReferenceQueue<? super T> queue,
                                     boolean objectToStringUsed) {
        super(o, queue);
        init(o, objectToStringUsed);
    }

    /**
     * On conserve le hash pour que la Reference puisse encore se faire
     * passer pour l'objet alors que celui-ci a disparu de la memoire
     *
     * @param o                  TODO
     * @param objectToStringUsed TODO
     */
    protected void init(T o, boolean objectToStringUsed) {
        if (o == null) {
            hash = 0;
        } else {
            hash = o.hashCode();
            if (objectToStringUsed) {
                toString = o.toString();
            }
            if (toString == null) {
                toString = o.getClass().getName() + '@' +
                           Integer.toHexString(hash);
            }
            if (toString.length() > 100) {
                toString = toString.substring(0, 100) + "...";
            }
        }
    }

    /**
     * DOCUMENTME Method
     *
     * @param o DOCUMENTME Description of the Parameter
     * @return DOCUMENTME Description of the Return Value
     */
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        // on travail avec un variable local pour ne pas etre obligé de
        // synchroniser la méthode
        Object local = get();
        if (o instanceof Reference) {
            o = ((Reference<?>) o).get();
        }

        boolean result =
                o == null && local == null
                || o != null && o.equals(local);

        return result;
    }

    /**
     * DOCUMENTME Method
     *
     * @return DOCUMENTME Description of the Return Value
     */
    public int hashCode() {
        return hash;
    }

    public String toString() {
        return toString;
    }

}
