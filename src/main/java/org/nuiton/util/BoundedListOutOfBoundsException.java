/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
* BoundedListOfBoundsException.java
*
* Created: 30 mai 2005
*
* @author Arnaud Thimel - thimel@codelutin.com
*
*/


package org.nuiton.util;


public class BoundedListOutOfBoundsException extends RuntimeException {

    private static final long serialVersionUID = 7006384682459926080L;

    public BoundedListOutOfBoundsException() {
        super();
    }

    public BoundedListOutOfBoundsException(String message) {
        super(message);
    }

    public BoundedListOutOfBoundsException(String message, Throwable cause) {
        super(message, cause);
    }

    public BoundedListOutOfBoundsException(Throwable cause) {
        super(cause);
    }

}
