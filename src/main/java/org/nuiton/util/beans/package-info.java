/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * Packages for all stuff of bean transformations (binder, and others...).
 *
 * This package contains two api :
 * <ul>
 * <li> the Binder api to copy objects</li>
 * <li> Some javabeans compiliant api</li>
 * </ul>
 *
 * <h2>The <b>Binder</b> api</h2>
 * <p>
 * This api permits to some object properties from an object to another one.
 * </p>
 * <h3>Obtain a binder</h3>
 * <p>A {@link org.nuiton.util.beans.Binder} contains a safe model named
 * {@link org.nuiton.util.beans.Binder.BinderModel} which knows
 * all properties that can be copied.
 * </p>
 * To use this api, you have only to get a {@link org.nuiton.util.beans.Binder}
 * object from the {@link org.nuiton.util.beans.BinderFactory} like this :
 * <pre>
 * Binder&lt;A, A&gt; binder = BinderFactory.newBinder(A.class);
 * </pre>
 *
 * It is also possible to build a more sofisticated binder which will only copy
 * some properties, using the {@link org.nuiton.util.beans.BinderModelBuilder}.
 *
 * <h3>Use a binder</h3>
 * Once you have a binder, you use the {@link org.nuiton.util.beans.Binder} api :
 *
 * To copy all properties from an object to another one :
 * <pre>
 * binder.copy(source, target);
 * </pre>
 *
 * To copy just some properties from an object to another one :
 * <pre>
 * binder.copy(source, target, "propertyOne", "propertyTwo");
 * </pre> 
 *
 * To copy all properties except some :
 * <pre>
 * binder.copyExcluding(source, target, "propertyToExeclude");
 * </pre>
 *
 * To obtain some properties from an object, use the following code :
 * <pre>
 * Map&lt;String, Object&gt; properties = binder.obtainProperties(source, "propertyOne", "propertyTwo");
 * </pre>
 *
 * <h3>Building a new BinderModel</h3>
 * <p>
 * In two words, you have to use the
 * {@link org.nuiton.util.beans.BinderModelBuilder} object to do this.
 * then register your binder model into the
 * {@link org.nuiton.util.beans.BinderFactory} using one of the method
 * {@code org.nuiton.util.beans.BinderFactory#registerBinderModel(XXX)}.
 * </p>
 * More explainations will come soon...
 * <p>
 * You can go and look on the unit tests which describe it pretty well :) :
 * </p>
 * <pre>
 * org.nuiton.util.beans.BinderModelBuilderTest
 * </pre>
 *
 * <h2>JavaBeans api</h2>
 *
 * <h3>{@link org.nuiton.util.beans.BeanMonitor} class</h3>
 * This object permits to listen javaBeans and keep modifications made on a bean.
 *
 * More explanations will come soon, meanwhile you can see the test class :
 * <pre>
 * org.nuiton.util.beans.BeanMonitorTest
 * </pre>
 *
 * @since 1.1.5
 */
package org.nuiton.util.beans;
