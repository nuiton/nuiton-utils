package org.nuiton.util;

/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;

/**
 * @author Kevin Morin - morin@codelutin.com
 * @since 3.0
 */
public class NumberUtil {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(NumberUtil.class);

    /**
     * Divide the divisor by the dividend.
     * Returns an array containing the quotients rounded up or down
     * to ensure the sum of the quotients equals the divisor.
     * <p>
     * e.g. divideAndEnsureSum(100, 3) returns {34, 33, 33}
     *
     * @param divisor  the divisor
     * @param dividend the dividend
     * @return an array whose length equals dividend
     */
    public static int[] divideAndEnsureSum(int divisor, int dividend) {
        // dividing by 0 is not permitted
        if (dividend == 0) {
            return null;
        }

        int quotient = (int) ((double) divisor) / dividend;
        int[] result = new int[dividend];
        Arrays.fill(result, quotient);

        int sum = quotient * dividend;
        int i = 0;
        while (sum != divisor && i < dividend) {
            result[i]++;
            sum++;
            i++;
        }

        return result;
    }

    public static final Predicate<Integer> NULL_OR_ZERO_INTEGER = new Predicate<Integer>() {

        @Override
        public boolean apply(Integer input) {
            return input == null || input == 0;
        }

        @Override
        public boolean test(Integer input) {
            return apply(input);
        }
    };

    public static final Predicate<Float> NULL_OR_ZERO_FLOAT_ONE_DIGIT = new Predicate<Float>() {

        @Override
        public boolean apply(Float input) {
            return input == null || Math.abs(roundOneDigit(input)) < 0.1;
        }

        @Override
        public boolean test(Float input) {
            return apply(input);
        }
    };
    public static final Predicate<Float> NULL_OR_ZERO_FLOAT_TWO_DIGITS = new Predicate<Float>() {

        @Override
        public boolean apply(Float input) {
            return input == null || Math.abs(roundTwoDigits(input)) < 0.01;
        }

        @Override
        public boolean test(Float input) {
            return apply(input);
        }
    };
    public static final Predicate<Float> NULL_OR_ZERO_FLOAT_THREE_DIGITS = new Predicate<Float>() {

        @Override
        public boolean apply(Float input) {
            return input == null || Math.abs(roundThreeDigits(input)) < 0.001;
        }

        @Override
        public boolean test(Float input) {
            return apply(input);
        }
    };
    public static final Predicate<Float> NULL_OR_ZERO_FLOAT_FOUR_DIGITS = new Predicate<Float>() {

        @Override
        public boolean apply(Float input) {
            return input == null || Math.abs(roundFourDigits(input)) < 0.0001;
        }

        @Override
        public boolean test(Float input) {
            return apply(input);
        }
    };

    public static final Predicate<Float> NULL_OR_ZERO_FLOAT_FIVE_DIGITS = new Predicate<Float>() {

        @Override
        public boolean apply(Float input) {
            return input == null || Math.abs(roundFiveDigits(input)) < 0.00001;
        }

        @Override
        public boolean test(Float input) {
            return apply(input);
        }
    };

    protected static final MathContext mc1Digit = new MathContext(1);
    protected static final MathContext mc2Digits = new MathContext(2);
    protected static final MathContext mc3Digits = new MathContext(3);
    protected static final MathContext mc4Digits = new MathContext(4);
    protected static final MathContext mc5Digits = new MathContext(5);

    public static Float roundOneDigit(Float number) {
        return round(number, mc1Digit);
    }

    public static Float roundTwoDigits(Float number) {
        return round(number, mc2Digits);
    }

    public static Float roundThreeDigits(Float number) {
        return round(number, mc3Digits);
    }

    public static Float roundFourDigits(Float number) {
        return round(number, mc4Digits);
    }

    public static Float roundFiveDigits(Float number) {
        return round(number, mc5Digits);
    }

    public static Float roundNDigits(Float number, int digits) {
        return round(number, new MathContext(digits));
    }

    public static Float round(Float number, MathContext mc) {
        float old = number;
        float partieEntier = (int) old;
        float digit = old - (int) old;
        number = partieEntier + new BigDecimal(digit).round(mc).floatValue();
        if (log.isDebugEnabled()) {
            log.debug("round " + old + " to " + number + " with mc = " + mc);
        }
        return number;
    }

}
