/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Class used to not print first line into delegated {@link OutputStream}.
 *
 * Used to remove first comment line writed by {@link Properties#store(OutputStream, String)}.
 *
 * @author chatellier
 */
public class PropertiesDateRemoveFilterStream extends FilterOutputStream {

    protected boolean firstLineOver;

    protected char endChar;

    public PropertiesDateRemoveFilterStream(OutputStream out) {
        super(out);
        firstLineOver = false;
        String lineSeparator = System.getProperty("line.separator");
        endChar = lineSeparator.charAt(lineSeparator.length() - 1);
    }

    @Override
    public void write(int b) throws IOException {
        if (!firstLineOver) {
            char c = (char) b;
            if (c == endChar) {
                firstLineOver = true;
            }
        } else {
            out.write(b);
        }
    }

}
