/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * LogException.java
 *
 * Created: Sat Apr 20 2002
 *
 * @author POUSSIN Benjamin <bpoussin@free.fr>
 * Copyright Code Lutin
 *
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.util;

public class LoggingException extends RuntimeException { // LogException

    private static final long serialVersionUID = 3495450140612716283L;

    public LoggingException(String msg) {
        super(msg);
    }

    public LoggingException(String msg, Throwable e) {
        super(msg, e);
    }

} // LogException
