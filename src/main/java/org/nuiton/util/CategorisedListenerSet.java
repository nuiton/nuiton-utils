/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
 * CategorisedListenerSet.java
 *
 * Created: 13 mai 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * Copyright Code Lutin
 *
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.Statement;
import java.util.Iterator;
import java.util.WeakHashMap;

/**
 * Objet permettant de géré plusieurs liste de listener de facon simple.
 * Chaque liste de listener est rangé en fonction d'une cle (categorie)
 * Une categorie peut avoir un pere, dans ce cas si un event doit etre lancé
 * sur une categorie il est aussi lancer sur le pere de la categorie.
 * Mais attention l'inverse n'est pas vrai, un event lancé sur un père n'est
 * jamais lancé sur ses fils.
 * Il existe une Category spéciale {@link #ALL} qui permet d'envoyer un event
 * a tous les listeners.
 * Si cette classe est la derniere classe a conserver l'objet categorie
 * alors la categorie est libere et ainsi que les listeners si c'etait aussi
 * leur derniers referencements
 *
 * <p> Si les categories sont representees par des Class, alors vous pouvez
 * utiliser la hierachie de classe pour creer de facon automatique les peres.
 *
 * @param <L> listener type
 * @see ListenerSet
 */
public class CategorisedListenerSet<L> { // CategorisedListenerSet

    /** Logger. */
    private static final Log log = LogFactory.getLog(CategorisedListenerSet.class);

    /**
     * permet de remplacer toutes les categories.
     * Si on utilise cette category, alors tous les listeners present
     * seront utilisé.
     */
    public static final Object ALL = new Object();

    /**
     * HashMap de ListenerSet, en cle l'objet qui caracterise la categorie
     * en valeur un ListenerSet
     */
    protected WeakHashMap<Object, ListenerSet<L>> listeners = new WeakHashMap<Object, ListenerSet<L>>();

    protected WeakHashMap<Object, Object> categoryParent = new WeakHashMap<Object, Object>();

    protected boolean isClassCategory = true;

    /** Empty constructor. */
    public CategorisedListenerSet() {

    }

    /**
     * @param isClassCategory si vrai et que les categorie passé en arguement
     *                        lors de l'ajout sont de type Class alors lors du fire on recherche aussi
     *                        les peres dans la hierarchie d'heritage de la classe (Super class et
     *                        interfaces)
     */
    public CategorisedListenerSet(boolean isClassCategory) {
        this();
        this.isClassCategory = isClassCategory;
    }

    protected void checkCategory(Object category) {
        if (ALL.equals(category)) {
            throw new IllegalArgumentException(
                    "ALL category can't be use to add listener or add Category");
        }
    }

    /**
     * Ajoute une categorie en indiquant sont pere. Une categorie ne peut
     * avoir qu'un seul pere, si la nouvelle categorie existait deja
     * alors l'appel a cette methode change son pere.
     *
     * @param parent      le pere de la categorie, null si on ne souhaite pas de pere
     * @param newCategory la nouvelle caterogie
     */
    public void addCategory(Object parent, Object newCategory) {
        checkCategory(parent);
        checkCategory(newCategory);
        categoryParent.put(newCategory, parent);
    }

    /**
     * Ajoute un listener sur une certaine categorie, si la categorie n'existe
     * alors on la crée en ne lui affectant pas de père
     *
     * @param category la categorie dans lequel il faut ajouter le listener
     * @param l        le listener a ajouter
     */
    public void add(Object category, L l) {
        checkCategory(category);
        ListenerSet<L> listeners = getListeners(category);
        listeners.add(l);
    }

    /**
     * Supprime un listener d'une categorie, si la categorie ou le listener
     * n'existe pas, rien ne se passe.
     *
     * @param category la categorie dans lequel il faut supprimer le listener
     * @param l        le listener a supprimer
     */
    public void remove(Object category, L l) {
        ListenerSet<L> listeners = getListeners(category);
        listeners.remove(l);
    }

    /**
     * Permet de lancer un event dans une categorie, l'event est aussi propagé
     * sur les ancètres de la categorie
     *
     * @param category   la categorie a partir duquel il faut lancer l'evenement
     * @param methodName le nom de la méthode de la classe listener
     * @param event      l'objet event a passer en paramètre de la methode du
     *                   listener
     * @throws Exception if event can't be fired
     */
    public void fire(Object category, String methodName, Object event)
            throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("fire category: " + category + " method: " + methodName);
        }
        ListenerSet<L> ls = getAllListeners(category);
        ls.fire(methodName, event);
    }

    /**
     * Permet de lancer un event dans une categorie, l'event est aussi propagé
     * sur les ancètres de la categorie, si un meme objet etait listener
     * dans plusieurs categories alors il ne recevra qu'une seul notification
     *
     * @param category   la categorie a partir duquel il faut lancer l'evenement
     * @param methodName le nom de la méthode de la classe listener
     * @throws Exception if event can't be fired
     */
    public void fire(Object category, String methodName) throws Exception {
        for (L l : getAllListeners(category)) {
            Statement stm = new Statement(l, methodName, null);
            stm.execute();
        }
    }

    /**
     * Retourne un Iterator sur tous les listeners qu'il faut prevenir si on
     * souhaite prevenir une certaine categorie. Ceci inclue les ancetre de la
     * categorie
     *
     * @param category category to get iterator on
     * @return iterator
     */
    public Iterator<L> iterator(Object category) {
        return getAllListeners(category).iterator();
    }

    /**
     * @param category categorie demandee
     * @return un ListenerSet contenant tous les listeners c'est à dire les
     *         listener de la categorie demandé mais aussi les listeners des ancetres
     */
    protected ListenerSet<L> getAllListeners(Object category) {
        ListenerSet<L> result = new ListenerSet<L>();
        if (ALL.equals(category)) {
            for (ListenerSet<L> ls : listeners.values()) {
                result.addAll(ls);
            }
        } else {
            Object parentCategory = category;
            while (parentCategory != null) {
                result.addAll(getListeners(parentCategory));
                if (isClassCategory && parentCategory instanceof Class) {
                    result.addAll(getListenersClass((Class<?>) parentCategory));
                }
                parentCategory = categoryParent.get(parentCategory);
            }
        }
        if (log.isTraceEnabled()) {
            log.trace("getAllListeners category: " + category + " result: "
                      + result);
        }
        return result;
    }

    protected ListenerSet<L> getListenersClass(Class<?> category) {
        ListenerSet<L> result = new ListenerSet<L>();
        Class<?> superClass = category.getSuperclass();
        if (superClass != null) {
            result.addAll(getAllListeners(superClass));
        }
        for (Class<?> c : category.getInterfaces()) {
            result.addAll(getAllListeners(c));
        }
        return result;
    }

    /**
     * @param category categorie demandee
     * @return un ListenerSet contenant seulement les listener de la categorie
     *         demandé. Si la categorie n'existe pas alors elle est créé.
     */
    protected ListenerSet<L> getListeners(Object category) {
        ListenerSet<L> result = listeners.get(category);
        if (result == null) {
            listeners.put(category, result = new ListenerSet<L>());
        }
        if (log != null && log.isTraceEnabled()) {
            log.trace("getListeners category: " + category + " result: "
                      + result);
        }
        return result;
    }

    public String toString() {
        return "Listeners Category: " + categoryParent + "\nListener: "
               + listeners;
    }

} // CategorisedListenerSet

