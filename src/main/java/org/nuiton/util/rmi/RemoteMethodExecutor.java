/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This class will act as an InvocationHandler except that it is distributed.
 *
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public interface RemoteMethodExecutor extends Remote {

    /**
     * Acts like an InvocationHandler.
     *
     * @param methodName     name of the method to invoke
     * @param parametersType parameters type to reliably identify the method
     * @param args           method arguments to process the effective call
     * @return the result of the delegate method
     * @throws RemoteException for any error. Business exceptions will be
     *                         wrapped.
     */
    Object execute(String methodName, Class<?>[] parametersType, Object[] args)
            throws RemoteException;

}
