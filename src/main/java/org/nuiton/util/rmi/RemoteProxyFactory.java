/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.rmi;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.ServerException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Factory to create RMI proxies to some given services.
 *
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class RemoteProxyFactory {

    private static final Log log = LogFactory.getLog(RemoteProxyFactory.class);

    // TODO AThimel 12/01/2011 This settings has to be externalized
    protected final static int PORT = 12345;

    protected final static String REGISTRY_IP = "127.0.0.1";

    /**
     * Create a RMI proxy on the wanted service interface. The default RMI name
     * will be used to find this service in the Registry.
     *
     * @param serviceInterface The class of the service proxy to create
     * @param <T>              some interface class
     * @return A newly created proxy which interface is &lt;T&gt;
     * @throws RemoteException   in case the registry is not reachable
     * @throws NotBoundException if the default RMI name cannot be found in the
     *                           registry
     */
    public static <T> T createProxy(final Class<T> serviceInterface)
            throws RemoteException, NotBoundException {

        // The default RMI name will be the FQN of the service interface
        String rmiName = serviceInterface.getName();
        T result = createProxy(rmiName, serviceInterface);

        return result;
    }

    /**
     * Create a RMI proxy on the wanted service interface. The specific given
     * RMI name will be used to find this service in the Registry.
     *
     * @param rmiName          The specific RMI name to use to find the service
     *                         in the registry
     * @param serviceInterface The class of the service proxy to create
     * @param <T>              some interface class
     * @return A newly created proxy which interface is &lt;T&gt;
     * @throws RemoteException   in case the registry is not reachable
     * @throws NotBoundException if the default RMI name cannot be found in the
     *                           registry
     */
    public static <T> T createProxy(String rmiName, Class<T> serviceInterface)
            throws RemoteException, NotBoundException {

        // Lookup the registry and the remote executor from the registry
        Registry registry = LocateRegistry.getRegistry(REGISTRY_IP, PORT);
        try {
            registry.list();
        } catch (ConnectException ce) {
            // That means the registry is not on the specified port, try the default one
            registry = LocateRegistry.getRegistry();
        }
        final RemoteMethodExecutor stub =
                (RemoteMethodExecutor) registry.lookup(rmiName);

        InvocationHandler handler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args)
                    throws Throwable {

                // Get parameters types and values to prepare delegate call
                String methodName = method.getName();
                Class<?>[] parametersType = method.getParameterTypes();

                // Delegate the execution and manage business exception cases
                Object result;
                try {
                    result = stub.execute(methodName, parametersType, args);
                } catch (ServerException se) {
                    if (log.isInfoEnabled()) {
                        log.info("Server exception: " + se.getMessage());
                    }
                    Throwable cause = se.getCause();
                    if (cause instanceof RemoteException) {
                        RemoteException re = (RemoteException) cause;
                        cause = re.getCause();
                    }
                    throw cause;
                }

                return result;
            }
        };

        // Invocation handler is ready, now create the proxy
        T proxy = (T) Proxy.newProxyInstance(
                ServiceExporter.class.getClassLoader(),
                new Class<?>[]{serviceInterface},
                handler);

        return proxy;
    }

}
