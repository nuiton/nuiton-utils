/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.rmi;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;

/**
 * RMI implementation of an invocation handler. This object will be exported to
 * a RMI registry and will delegate method calls to some business service. The
 * service is provided in the constructor.
 *
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class RemoteMethodExecutorImpl<T> implements RemoteMethodExecutor {

    /** The target service on which calls will be made */
    protected T service;

    /**
     * This is the only available constructor. It is mandatory to specify a
     * target service on which call will be
     * delegated.
     *
     * @param service the mandatory service which calls will be delegated on
     */
    public RemoteMethodExecutorImpl(T service) {
        if (service == null) {
            throw new NullPointerException("Service cannot be null");
        }
        this.service = service;
    }

    @Override
    public Object execute(
            String methodName, Class<?>[] parametersType, Object[] args)
            throws RemoteException {

        Object result;
        try {

            // Get the method on the target service then invoke it
            Method method = service.getClass().getMethod(
                    methodName, parametersType);
            result = method.invoke(service, args);

        } catch (InvocationTargetException ite) {
            // This is the normal behaviour if a business exception is thrown
            Throwable targetException = ite.getTargetException();
            throw new RemoteException(
                    "Business exception occurred", targetException);
        } catch (NoSuchMethodException nsme) {
            throw new RemoteException("Delegate method not found", nsme);
        } catch (IllegalAccessException iae) {
            throw new RemoteException("Delegate method not accessible", iae);
        }

        return result;
    }

}
