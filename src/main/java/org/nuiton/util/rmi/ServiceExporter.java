/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.util.rmi;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;

/**
 * This class allows to make some service available throw RMI. For each service,
 * a wrapper will be created which will be put in the RMI registry. This wrapper
 * will intercept calls to the service and delegate them to it.
 *
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public final class ServiceExporter {

    private static final Log log = LogFactory.getLog(ServiceExporter.class);

    // TODO AThimel 12/01/2011 This settings has to be externalized
    private static final int PORT = 12345;

    /** Does some checks on RMI configuration */
    protected static void testRmiConfig() {
        String rmiHost = System.getProperty("java.rmi.server.hostname");
        if ((rmiHost == null || "".equals(rmiHost.trim()))
            && log.isWarnEnabled()) {
            log.warn("Server might not have been initialized properly, " +
                     "please specify '-Djava.rmi.server.hostname=<IP-address>'");
        }
    }

    /**
     * Will look for the RMI registry. It an external registry cannot be found,
     * a new one will be created.
     *
     * @return the registry found or created
     * @throws RemoteException in case it is not possible to get the registry
     */
    protected static Registry getRegistry() throws RemoteException {
        Registry result;
        try {
            result = LocateRegistry.getRegistry(PORT);
            // To test that registry has been created. An exception will be
            // thrown if registry cannot be called
            result.list();
        } catch (ConnectException ce) {
            if (log.isWarnEnabled()) {
                log.warn("Registry not found, creating a new one");
            }
            try {
                result = LocateRegistry.createRegistry(PORT);
            } catch (ExportException ee) { // This is the particular case when a registry is already running but not on the correct port.
                if (log.isWarnEnabled()) {
                    log.warn("Unable to create registry, try using the default one", ee);
                }

                // Try the default port
                result = LocateRegistry.getRegistry();
            }
        }
        return result;
    }

    /**
     * Will register a service using the default name.
     *
     * @param serviceInterface the interface used to bind the service. The RMI
     *                         name will be generated from this class name
     * @param instance         the service instance to bind
     * @param <E>              some interface class
     * @throws RemoteException in case the registry is not reachable
     */
    public static <E> void registerService(Class<E> serviceInterface, E instance)
            throws RemoteException {
        String rmiName = serviceInterface.getName();
        registerService(rmiName, instance);
    }

    /**
     * Will register a service using the given RMI name.
     *
     * @param rmiName  the RMI name used to bind the service in the registry
     * @param instance the service instance to bind
     * @param <E>      some interface class
     * @throws RemoteException in case the registry is not reachable
     */
    public static <E> void registerService(String rmiName, E instance)
            throws RemoteException {

        testRmiConfig();

        // Create the proxy and let him be a stub
        RemoteMethodExecutorImpl<E> executor =
                new RemoteMethodExecutorImpl<E>(instance);
        Remote stub = UnicastRemoteObject.exportObject(executor, 0);

        // Bind into the registry
        Registry registry = getRegistry();
        registry.rebind(rmiName, stub);
    }

    /**
     * Will unregister a service using the default name.
     *
     * @param serviceInterface the interface used to unbind the service. The RMI
     *                         name will be generated from this class name
     * @throws RemoteException   in case the registry is not reachable
     * @throws NotBoundException in case the given name is not bound
     */
    public static void unregisterService(Class<?> serviceInterface)
            throws RemoteException, NotBoundException {
        String rmiName = serviceInterface.getName();
        unregisterService(rmiName);
    }

    /**
     * Will unregister a service using the given RMI name.
     *
     * @param rmiName the RMI name used to unbind the service in the registry
     * @throws RemoteException   in case the registry is not reachable
     * @throws NotBoundException in case the given name is not bound
     */
    public static void unregisterService(String rmiName)
            throws RemoteException, NotBoundException {

        // Bind into the registry
        Registry registry = getRegistry();
        registry.unbind(rmiName);
    }

}
