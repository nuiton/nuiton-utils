/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * This package contains classes to easily export services to a RMI registry
 * then get a proxy to access them. The provided classes will hide RMI
 * complexity.
 *
 * Use the {@link org.nuiton.util.rmi.ServiceExporter} to register an service to the RMI registry.
 *
 * Use the {@link org.nuiton.util.rmi.RemoteProxyFactory} to get a proxy an call the RMI exported
 * service.
 *
 * @author Arnaud Thimel - thimel@codelutin.com
 */
package org.nuiton.util.rmi;
