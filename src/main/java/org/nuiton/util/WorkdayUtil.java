package org.nuiton.util;

/*-
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Year;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * Permet de calculer le nombre de jour ouvré entre deux dates.
 *
 * du 3 au 3 donne 0.
 * du 3 au 4 donne 1 (si 3 n'est pas un jour fermé)
 */
public class WorkdayUtil {

    public static long computeWorday(LocalDate start, LocalDate end, Set<DayOfWeek> weekWorkday, Function<Year, List<LocalDate>> publicHoliday) {

        // recherche tous les jours fériés des années entre les deux dates
        Set<LocalDate> publicHolidays = new HashSet<>();
        for (int year = start.getYear(), lastYear = end.getYear(); year <= lastYear; year++) {
            publicHolidays.addAll(publicHoliday.apply(Year.of(year)));
        }

        // calcul le nombre de jour férié dans l'interval
        long publicHolidayNumber = publicHolidays.stream()
                // On garde que des jours fériés qui tombe un jour ouvré
                .filter(d -> weekWorkday.contains(d.getDayOfWeek()))
                // On garde que des jours fériés qui sont dans l'interval de date
                .filter(d -> start.compareTo(d) * d.compareTo(end) >= 0)
                .count();

        // calcul le nombre de jour ouvré en semaine pleine
        long days = ChronoUnit.DAYS.between(start, end);
        long week = days / DayOfWeek.values().length;

        long result = week * weekWorkday.size();

        // on calcul le nombre de jour ouvré de la dernière semaine non pleine
        LocalDate lastDays = end.minusDays(days % DayOfWeek.values().length);

        while (lastDays.isBefore(end)) {
            if (weekWorkday.contains(lastDays.getDayOfWeek())) {
                result++;
            }
            lastDays = lastDays.plusDays(1);
        }

        // on retranche les jours fériés
        result -= publicHolidayNumber;

        return result;
    }

    public static class FrenchPublicHoliday implements Function<Year, List<LocalDate>> {

        @Override
        public List<LocalDate> apply(Year year) {
            LocalDate easter = computeEaster(year);

            List<LocalDate> result = new LinkedList<>();

            result.add(computeEasterMonday(easter));
            result.add(computeAscensionDay(easter));
            result.add(computeWhitMonday(easter));
            result.add(LocalDate.of(year.getValue(), 1, 1));
            result.add(LocalDate.of(year.getValue(), 5, 1));
            result.add(LocalDate.of(year.getValue(), 5, 8));
            result.add(LocalDate.of(year.getValue(), 7, 14));
            result.add(LocalDate.of(year.getValue(), 8, 15));
            result.add(LocalDate.of(year.getValue(), 11, 1));
            result.add(LocalDate.of(year.getValue(), 11, 11));
            result.add(LocalDate.of(year.getValue(), 12, 25));

            return result;
        }
    }

    /**
     * Calcul fait par la méthode de Butcher-Meeus (valide si année ≥ 1583)
     *
     * @param year l'année pour lequel on souhaite calculer le dimanche de paques
     * @return la date du dimanche paques
     */
    public static LocalDate computeEaster(Year year) {
        int y = year.getValue();
        int n = y % 19;

        int c = y / 100;
        int u = y % 100;

        int s = c / 4;
        int t = c % 4;

        int p = (c + 8) / 25;
        int q = (c - p + 1) / 3;

        int e = (19 * n + c - s - q + 15) % 30;

        int b = u / 4;
        int d = u % 4;

        int L = (32 + 2 * t + 2 * b - e - d) % 7;
        int h = (n + 11 * e + 22 * L) / 451;

        int m = (e + L - 7 * h + 114) / 31;
        int j = (e + L - 7 * h + 114) % 31;

        LocalDate easterMonday = LocalDate.of(y, m, j + 1);

        return easterMonday;
    }

    public static LocalDate computeEasterMonday(LocalDate easter) {
        LocalDate easterMonday = easter.plusDays(1);
        return easterMonday;
    }

    public static LocalDate computeAscensionDay(LocalDate easter) {
        LocalDate ascensionDay = easter.plusDays(39);
        return ascensionDay;
    }

    public static LocalDate computeWhitMonday(LocalDate easter) {
        LocalDate whitMonday = easter.plusDays(50);
        return whitMonday;
    }
}
