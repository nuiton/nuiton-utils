/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * GZUtil.java
 *
 * Created: 3 nov. 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 *
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZUtil { // GZUtil

    /**
     * Tests if an inputStream is GZipped.
     * <p>
     * <b>Note:</b> the stream is not closed and is reset.
     *
     * @param inputStream inputStream to test
     * @return {@code true} if inputStream is gzipped, {@code false} otherwise
     * @throws IOException if any io errors while reading inputStream
     * @since 3.0
     */
    public static boolean isGzipStream(InputStream inputStream) throws IOException {

        inputStream.mark(2);
        // read header to see if is compressed file
        int b = inputStream.read();
        // redundant cast : int magic = ((int) in.read() << 8) | b;
        int magic = inputStream.read() << 8 | b;
        inputStream.reset();
        boolean gzip = magic == GZIPInputStream.GZIP_MAGIC;
        return gzip;

    }

    /**
     * @param in FIXME
     * @return Retourne la string decompressee
     */
    public static StringBuffer bytesToStringBuffer(byte[] in) {
        try {
            if (in == null || in.length == 0) {
                return new StringBuffer("");
            }
            GZIPInputStream gz = new GZIPInputStream(new BufferedInputStream(new ByteArrayInputStream(in)));
            StringBuffer sb = new StringBuffer();
            int c;

            while ((c = gz.read()) != -1) {
                sb.append((char) c);
            }
            gz.close();
            return sb;
        } catch (IOException eee) {
            throw new GZUtilException("Probleme dans la decompression", eee);
        }
    }

    /**
     * @param in FIXME
     * @return la string decompressee
     */
    public static String bytesToString(byte[] in) {
        return bytesToStringBuffer(in).toString();
    }

    /**
     * @param elem FIXME
     * @return Retourne la string compressee
     */
    public static byte[] stringBufferToBytes(StringBuffer elem) {
        return stringToBytes(elem.toString());
    }

    /**
     * @param elem  FIXME
     * @return Retourne la string compressee
     */
    public static byte[] stringToBytes(String elem) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            GZIPOutputStream gz = new GZIPOutputStream(new BufferedOutputStream(baos));
            Reader sr = new BufferedReader(new StringReader(elem));
            int c;
            while ((c = sr.read()) != -1) {
                gz.write((char) c);
            }
            gz.close();

            return baos.toByteArray();
        } catch (IOException eee) {
            throw new GZUtilException("Probleme dans la compression", eee);
        }
    }

} // GZUtil

