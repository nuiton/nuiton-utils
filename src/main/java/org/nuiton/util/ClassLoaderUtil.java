/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * A usefull class with method for ClassLoader
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ClassLoaderUtil {


    /** Logger. */
    private static final Log log = LogFactory.getLog(ClassLoaderUtil.class);

    /**
     * Returns the all urls to be used in a {@link URLClassLoader}.
     *
     * If classloader  has only one url and the url is a jar, try to load in
     * manifest class-path.
     *
     * @param loader the classloader (if null will use system one)
     * @return all the url found in the classloader
     */
    public static URL[] getDeepURLs(URLClassLoader loader) {
        Stack<URL> urlToTreate = new Stack<URL>();
        List<URL> urlTreated = new ArrayList<URL>();

        // first get the urls from classloader
        URL[] result = getURLs(loader);

        urlToTreate.addAll(Arrays.asList(result));
        while (!urlToTreate.isEmpty()) {
            URL currentUrl = urlToTreate.pop();
            // save the url
            urlTreated.add(currentUrl);
            if (Resource.isJar(currentUrl.toString())) {
                // jar invocation
                try {
                    URL[] newArrayURLs =
                            Resource.getClassPathURLsFromJarManifest(
                                    currentUrl);
                    if (newArrayURLs == null) {
                        continue;
                    }
                    List<URL> newURLs = Arrays.asList(newArrayURLs);
                    for (URL newURL : newURLs) {
                        if (!urlTreated.contains(newURL) &&
                            !urlToTreate.contains(newURL)) {
                            urlToTreate.add(newURL);
                        }
                    }
                } catch (Exception e) {
                    if (log.isDebugEnabled()) {
                        // this is not a such error, but some jar can not be 
                        log.debug("error with url" + currentUrl +
                                  " for reason : " + e.getMessage());
                    }
                }
            }
        }
        return urlTreated.toArray(new URL[urlToTreate.size()]);
    }

    /**
     * Recupere la liste des urls d'un {@link URLClassLoader}.
     *
     * Note : Un cas particulier est positionné pour JBoss qui utilise
     * la method getAllURLs.
     *
     * @param classLoader le class loader a scanner
     * @return les urls du classloade.
     */
    public static URL[] getURLs(URLClassLoader classLoader) {
        if (classLoader == null) {
            classLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        }
        Method m;
        try {
            // Essai de récupération de la méthode getAllURLs() de
            // RepositoryClassLoader (JBoss)
            m = classLoader.getClass().getMethod("getAllURLs");
        } catch (Exception e) {
            m = null;
        }
        URL[] result;
        if (m == null) {
            result = classLoader.getURLs();
        } else {
            try {
                result = (URL[]) m.invoke(classLoader);
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return result;
    }

    public static void printLoader(ClassLoader loader) {
        log.info(loader);
        if (loader instanceof URLClassLoader) {
            URL[] urls = getURLs((URLClassLoader) loader);
            for (URL url : urls) {
                log.info(url);
            }
        }
    }

}
