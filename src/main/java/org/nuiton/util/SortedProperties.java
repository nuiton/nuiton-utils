/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

/**
 * Permet d'avoir les propriétés triées.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 *
 */
public class SortedProperties extends Properties {

    private static final long serialVersionUID = -1147150444452577558L;


    public SortedProperties() {
    }

    public SortedProperties(Properties defaults) {
        super(defaults);
    }

    @Override
    public synchronized Enumeration<Object> keys() {
        List<Object> objects = Collections.list(super.keys());
        Vector<Object> result;
        try {
            // Attention, si les clef ne sont pas des string, ca ne marchera pas
            List<String> list = CollectionUtil.toGenericList(objects,
                                                             String.class);
            Collections.sort(list);
            result = new Vector<Object>(list);
        } catch (IllegalArgumentException e) {
            // keys are not string !!!
            // can not sort keys
            result = new Vector<Object>(objects);
        }
        return result.elements();
    }
}
