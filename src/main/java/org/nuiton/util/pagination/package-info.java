/**
 * This package contains all about pagination : <ul>
 *     <li>{@link org.nuiton.util.pagination.PaginationParameter} to express the input pagination parameters when
 *     preparing a query</li>
 *     <li>{@link org.nuiton.util.pagination.PaginationOrder} represents an order clause together with asc/desc</li>
 *     <li>{@link org.nuiton.util.pagination.PaginationResult} represents a list together with the pagination parameters
 *     used to get the list of elements. It also contains methods to navigate throw the other pages</li>
 * </ul>
 */
package org.nuiton.util.pagination;

/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
