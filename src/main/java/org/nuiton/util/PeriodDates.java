/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * TODO tchemit 2010-08-25 JAVADOC + I18N
 * PeriodDates.java
 *
 * Created on 2009-08-13
 *
 * @author fdesbois
 */
public class PeriodDates implements Serializable {

    private static final long serialVersionUID = 1L;

    private String pattern;

    private Calendar fromCalendar;

    private Calendar thruCalendar;

    public PeriodDates() {
    }

    public PeriodDates(Date fromDate, Date thruDate) throws IllegalArgumentException {
        if (thruDate != null && fromDate != null && fromDate.after(thruDate)) {
            throw new IllegalArgumentException("The fromDate can't be after the thruDate !");
        }

        setFromDate(fromDate);
        setThruDate(thruDate);

        pattern = DateUtil.DEFAULT_PATTERN;
    }

    public PeriodDates(Calendar fromCalendar, Calendar thruCalendar) throws IllegalArgumentException {
        if (thruCalendar != null && fromCalendar != null && fromCalendar.after(thruCalendar)) {
            throw new IllegalArgumentException("The fromDate can't be after the thruDate !");
        }
        this.fromCalendar = fromCalendar;
        this.thruCalendar = thruCalendar;

        pattern = DateUtil.DEFAULT_PATTERN;
    }

    /**
     * Create a period from the date of today. If monthAfter is negative, the current day will be
     * the thruDate of the period. The period extremities will be initialized (first day of month for
     * fromDate and last day of month for thruDate). Time is set to 0 for both
     * dates.
     * <pre>
     * Ex 1 :
     * Today = 12/05/2009
     * monthAfter = 5
     * Period = 01/05/2009 to 31/10/2009
     * Ex 2 :
     * Today = 12/05/2009
     * monthAfter = -4
     * Period = 01/01/2009 to 31/05/2009
     * </pre>
     *
     * @param monthAfter the number of month from the current one
     * @return a new PeriodDates
     */
    public static PeriodDates createMonthsPeriodFromToday(int monthAfter) {
        Calendar calendarFrom = Calendar.getInstance();
        Calendar calendarThru = Calendar.getInstance();

        // Reset time
        Date minTime = DateUtil.setMinTimeOfDay(new Date());
        calendarFrom.setTime(minTime);
        calendarThru.setTime(minTime);

        if (monthAfter < 0) {
            calendarFrom.add(Calendar.MONTH, monthAfter);
        } else {
            calendarThru.add(Calendar.MONTH, monthAfter);
        }

        PeriodDates period = new PeriodDates(calendarFrom, calendarThru);
        period.initDayOfMonthExtremities();
        period.setPattern(DateUtil.MONTH_PATTERN);
        return period;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    /** Set first day of month for dateFrom and last day of month for dateThru */
    public void initDayOfMonthExtremities() {
        if (fromCalendar == null || thruCalendar == null) {
            throw new NullPointerException("fromDate or thruDate can't be null to" +
                                           " change dayOfMonth extremities");
        }
        Date fromDate = fromCalendar.getTime();
        fromDate = DateUtil.setFirstDayOfMonth(fromDate);
        fromDate = DateUtil.setMinTimeOfDay(fromDate);
        fromCalendar.setTime(fromDate);

        Date thruDate = thruCalendar.getTime();
        thruDate = DateUtil.setLastDayOfMonth(thruDate);
        thruDate = DateUtil.setMaxTimeOfDay(thruDate);
        thruCalendar.setTime(thruDate);
    }

    public void setFromDate(Date fromDate) {
        //this.fromDate = fromDate;
        if (fromDate != null) {
            if (thruCalendar != null && fromDate.after(thruCalendar.getTime())) {
                throw new IllegalArgumentException("The fromDate can't be after the thruDate !");
            }
            if (fromCalendar == null) {
                fromCalendar = Calendar.getInstance();
            }
            fromCalendar.setTime(fromDate);
        } else {
            fromCalendar = null;
        }
    }

    public Date getFromDate() {
        return fromCalendar != null ? fromCalendar.getTime() : null;
    }

    public int getFromMonth() {
        return fromCalendar.get(Calendar.MONTH);
    }

    public void setThruDate(Date thruDate) {
        //this.thruDate = thruDate;
        if (thruDate != null) {
            if (fromCalendar != null && thruDate.before(fromCalendar.getTime())) {
                throw new IllegalArgumentException("The thruDate can't be before the fromDate !");
            }
            if (thruCalendar == null) {
                thruCalendar = Calendar.getInstance();
            }
            thruCalendar.setTime(thruDate);
        } else {
            thruCalendar = null;
        }
    }

    public Date getThruDate() {
        return thruCalendar != null ? thruCalendar.getTime() : null;
    }

    public List<Date> getMonths() {
        List<Date> months = new ArrayList<Date>();
        if (fromCalendar == null || thruCalendar == null) {
            return months;
        }

//        Calendar current = (Calendar) fromCalendar.clone();
//        current.set(Calendar.DAY_OF_MONTH, 1);
//        Calendar end = (Calendar) thruCalendar.clone();
//        end.set(Calendar.DAY_OF_MONTH, 1);

        // Prepare calendars for while condition :
        // set first day of month + reset time to 00:00:00.000
        Date tmp = DateUtil.setFirstDayOfMonth(fromCalendar.getTime());
        tmp = DateUtil.setMinTimeOfDay(tmp);
        Calendar current = DateUtil.getDefaultCalendar(tmp);

        tmp = DateUtil.setFirstDayOfMonth(thruCalendar.getTime());
        tmp = DateUtil.setMinTimeOfDay(tmp);
        Calendar end = DateUtil.getDefaultCalendar(tmp);

        while (!current.equals(end)) {
            months.add(current.getTime());
            current.add(Calendar.MONTH, 1);
        }
        months.add(current.getTime());

        return months;
    }

    public List<String> getFormatedMonths() {
        List<String> results = new ArrayList<String>();
        for (Date date : getMonths()) {
            String str = DateUtil.formatDate(date, pattern);
            results.add(str);
        }
        return results;
    }

    public boolean beforeEnd(Calendar calendar) {
        return calendar.before(thruCalendar) || calendar.equals(thruCalendar);
    }

    public boolean afterEnd(Calendar calendar) {
        return calendar.after(thruCalendar);
    }

    public boolean afterBegin(Calendar calendar) {
        return calendar.after(fromCalendar) || calendar.equals(fromCalendar);
    }

    public boolean between(Calendar calendar) {
        return beforeEnd(calendar) && afterBegin(calendar);
    }

    public boolean beforeEnd(Date date) {
        Calendar calendar = DateUtil.getDefaultCalendar(date);
        return beforeEnd(calendar);
    }

    public boolean afterEnd(Date date) {
        Calendar calendar = DateUtil.getDefaultCalendar(date);
        return afterEnd(calendar);
    }

    public boolean afterBegin(Date date) {
        Calendar calendar = DateUtil.getDefaultCalendar(date);
        return afterBegin(calendar);
    }

    public boolean between(Date date) {
        Calendar calendar = DateUtil.getDefaultCalendar(date);
        return between(calendar);
    }


    @Override
    public String toString() {
        String fromStr = DateUtil.formatDate(getFromDate(), pattern);
        String thruStr = DateUtil.formatDate(getThruDate(), pattern);
        return "[ " + getFromDate() + " (pattern: " + fromStr + ") → " +
               getThruDate() + " (pattern: " + thruStr + ") ]";
    }

}
