/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/** The Class ZipStreamEncoder. */
public class ZipStreamEncoder extends Thread {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ZipStreamEncoder.class);

    /** The Constant BUFFER. */
    static final int BUFFER = 2048;

    /** The files. */
    private Map<String, InputStream> files;

    /** The zos. */
    private ZipOutputStream zos;

    /**
     * Instantiates a new zip stream encoder.
     *
     * @param files the files
     * @param os    the os
     */
    public ZipStreamEncoder(Map<String, InputStream> files, OutputStream os) {
        this.files = files;

        zos = new ZipOutputStream(os);
        zos.setMethod(ZipOutputStream.DEFLATED);
        zos.setLevel(Deflater.BEST_COMPRESSION);
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        byte data[] = new byte[BUFFER];
        try {
            for (Map.Entry<String, InputStream> kv : files.entrySet()) {
                ZipEntry entry = new ZipEntry(kv.getKey());
                InputStream origin = kv.getValue();
                zos.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    zos.write(data, 0, count);
                }
                origin.close();
            }

            zos.close();
        } catch (IOException e) {
            for (Map.Entry<String, InputStream> kv : files.entrySet()) {
                InputStream origin = kv.getValue();
                try {
                    origin.close();
                } catch (IOException ioe) {
                    log.error("Impossible to close " + kv.getKey());
                }
            }
            log.error("Impossible to compress in stream");
            throw new RuntimeException("Impossible to compress in stream");
        }
    }

}
