/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import java.util.Properties;

/**
 * Overrides {@link Properties} in order to check if the expected value
 * contains another property key like "${...}". It that case, the key
 * will be replaced by its value if possible.
 *
 * Example :
 * <pre>
 * myFirstName=Arnaud
 * myName=Thimel
 * org.nuiton.topia.userInfo.fullName=${fullName}
 * fullName=${myFirstName} ${myName}
 * namePhrase=My name is ${myName}.
 * instruction=Put your text like this : ${myText}
 * </pre>
 *
 * Dans ce cas,
 * <ul>
 * <li>getProperty("org.nuiton.topia.userInfo.fullName") renverra "Arnaud Thimel"
 * <li>getProperty("namePhrase") renverra "My name is Thimel."
 * <li>getProperty("instruction") renverra "Put your text like this : ${myText}"
 * </ul>
 *
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class RecursiveProperties extends Properties {

    private static final long serialVersionUID = -5012939272780929116L;

    public RecursiveProperties() {
    }

    public RecursiveProperties(Properties defaults) {
        super(defaults);
    }

    @Override
    public String getProperty(String key) {
        String result = super.getProperty(key);
        if (result == null) {
            return null;
        }
        //Ex : result="My name is ${myName}."
        int pos = result.indexOf("${", 0);
        //Ex : pos=11
        while (pos != -1) {
            int posEnd = result.indexOf("}", pos + 1);
            //Ex : posEnd=19
            if (posEnd != -1) {
                String value = getProperty(result.substring(pos + 2, posEnd));
                // Ex : getProperty("myName");
                if (value != null) {
                    // Ex : value="Thimel"
                    result = result.substring(0, pos) + value + result.substring(posEnd + 1);
                    // Ex : result="My name is " + "Thimel" + "."
                    pos = result.indexOf("${", pos + value.length());
                    // Ex : pos=-1
                } else {
                    // Ex : value=null
                    pos = result.indexOf("${", posEnd + 1);
                    // Ex : pos=-1
                }
                // Ex : pos=-1
            }
        }
        return result;
    }

} //RecursiveProperties
