/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * Ensemble de classe Java permettant de simplifier le développement en
 * factorisant des besoins que l'on retrouve dans tous les développement.
 * On y trouve de fontion de travail sur les chaînes de caractères. Des
 * fonctions de parsage des arguements de la ligne de commande. Des fonctions
 * permettant très simplement de rechercher une resource (images, fichier de
 * propriétés, ...).
 */
package org.nuiton.util;
