/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * GZUtilException.java
 *
 * Created: 3 nov. 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 *
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.util;

public class GZUtilException extends RuntimeException { // GZUtilException

    /**  */
    private static final long serialVersionUID = -3342417793974741697L;

    public GZUtilException(String msg) {
        super(msg);
    }

    public GZUtilException(String msg, Throwable eee) {
        super(msg, eee);
    }
} // GZUtilException

