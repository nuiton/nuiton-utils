/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.net.URL;

/**
 * FasterCachedResourceResolver is a URIResolver using a optimized search time algorithm.
 * <p>
 * For a given href, will try to search the exact path in classp-ath in the exact directory META-INF/cache.
 * </p>
 * The fact of using META-INF directory is optimized (special jvm directory).
 *
 * The other fact of using an exact directory makes also decrease dramaticly time search.
 *
 * Finally, we spend ten times less time in searching; moreover using the path of
 * searched resource resolve the probleme of name colision and this is also a good thing...
 *
 * Exemple :
 * <pre>
 * href = http://foo/bar/file.txt
 * search resource : /META-INF/cache/foo/bar/file.txt
 * </pre>
 *
 * @author chorlet
 */
public class FasterCachedResourceResolver extends ResourceResolver {

    /** log. */
    private static final Log log = LogFactory.getLog(FasterCachedResourceResolver.class);

    protected boolean offline;

    public FasterCachedResourceResolver() {
        this(null);
    }

    public FasterCachedResourceResolver(String base) {
        super(base);
    }

    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    @Override
    protected Source findHrefSource(String path) {
        long t0 = System.nanoTime();
        String resource = "META-INF/cache/" + path;

        if (verbose) {
            log.info("will discover " + resource);
        }

        URL url = cl.getResource(resource);

        Source source = null;

        if (url != null) {
            if (verbose) {
                log.info(url.toString());
            }
            source = new StreamSource(url.toString());
        }
        if (source == null) {
            if (offline) {
                throw new IllegalStateException("offline - could not find extact resource at location " + resource);
            }
            log.warn("could not find extact resource at location " + resource);
        }
        if (verbose) {
            log.info("resolved in " + StringUtil.convertTime(System.nanoTime() - t0));
        }
        return source;
    }
}
