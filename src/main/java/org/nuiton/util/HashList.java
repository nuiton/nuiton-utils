/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * HashList.java
 *
 * Created: 2 nov. 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 *
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * Cette objet permet de gerer l'unicité des objets ajouté.
 * Lorsque l'on appelle la methode add sur cette objet, il verifie
 * en premier que l'objet n'est pas deja dans la liste. S'il y est
 * alors il n'est pas ajouter, sinon il est ajouter.
 * L'utilisation de la methode set n'est pas permise
 *
 * FIXME: la serialisation n'est pas modifier, c-a-d que seul le tableau est
 * conserve et pas la hashSet, donc apres recuperation, l'uticite n'est plus
 * garantie. Il faut donc surcharger readObject et writeObject pour conserver
 * le HashSet
 */
public class HashList<E> extends ArrayList<E> { // HashList

    /**  */
    private static final long serialVersionUID = -334941610313293930L;

    protected HashSet<E> set = new HashSet<E>();

    public HashList() {
    }

    public HashList(Collection<? extends E> c) {
        addAll(c);
    }

    public HashList(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public E set(int index, E element) {
        throw new UnsupportedOperationException("You can't use set method in HashList");
    }

    @Override
    public boolean add(E o) {
        boolean result = !contains(o);
        add(size(), o);
        return result;
    }

    @Override
    public void add(int index, E element) {
        if (set.add(element)) {
            super.add(index, element);
        }
    }

    /**
     * supprime l'element demandé. Si l'elment n'existe pas alors, null
     * est retrouné.
     */
    @Override
    public E remove(int index) {
        if (set.remove(get(index))) {
            return super.remove(index);
        }
        return null;
    }

    @Override
    public void clear() {
        set.clear();
        super.clear();
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean modified = false;
        for (E aC : c) {
            if (add(aC))
                modified = true;
        }
        return modified;

    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        boolean modified = false;
        for (E aC : c) {
            add(index++, aC);
            modified = true;
        }
        return modified;

    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        for (int i = toIndex - 1; i >= fromIndex; i--) {
            remove(i);
        }
    }

    @Override
    public boolean contains(Object elem) {
        return set.contains(elem);
    }

    @Override
    public Object clone() {
        HashList<E> result = new HashList<E>(this);
        return result;
    }

} // HashList

